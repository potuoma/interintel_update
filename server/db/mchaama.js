module.exports = {
	"HOME" : {"gateway_host": "mchaama.com", "response_status": "00", "CHID": "1", "lat": "1.0", "lng": "38.0", "ip_address": "41.212.114.98", "response": {"event_status": "System User", "get_interface": {"all_pages": {"1": {"Home": {"2": ["HOME", "home"], "5": ["Login", "icons:lock"], "4": ["Gallery", "image:collections"], "7": ["Register?", "social:person-add"]}}}, "this_page_inputs": {"1": {"section_var": ["Home", "home"], "5": {"Login": {"1": {"Login": {"1": ["USERNAME", "TEXT INPUT", "2", "45", "USERNAME", "", "icons:account-circle", "col-md-4", "", true], "3": ["FORGOT PASSWORD?", "HYPERLINK", "15", "15", "FORGOT PASSWORD", "/forgot_password/", "icons:help-outline", "col-md-2", "", true], "2": ["PASSWORD", "PASSWORD INPUT", "6", "30", "PASSWORD", "", "icons:lock", "col-md-3", "", true], "4": ["CONTINUE", "SUBMIT", "0", "0", "submit", "Submit", "icons:check", "col-md-1", "", true], "input_var": ["LOGIN", "FORM", "0", "0", "", "", "section", "col-md-7", "settings", false, false, 900]}}}, "page_var": ["Login", "icons:lock"]}, "7": {"Register?": {"1": {"Register?": {"input_var": ["MEMBERSHIP REGISTRATION REQUEST", "FORM", "0", "0", "", "MEMBERSHIP REGISTRATION REQUEST", "register", "col-md-4", "social:person-add", false, false, 900], "3": ["Membership Plan", "DROPDOWN SELECT", "1", "45", "item=Membership Plan", "DATA SOURCE", "icons:card-membership", "col-md-12", "", true], "2": ["Institution", "DROPDOWN SELECT", "1", "45", "institution_id", "DATA SOURCE", "icons:account-balance", "col-md-12", "", true], "5": ["Passport/ID Number", "TEXT INPUT", "4", "45", "passport_id_number", "", "icons:credit-card", "col-md-12", "", true], "4": ["Full Names", "TEXT INPUT", "10", "45", "full_names", "", "social:person", "col-md-12", "", true], "6": ["E-Mail", "EMAIL INPUT", "5", "45", "EMAIL", "", "icons:mail", "col-md-12", "", true], "8": ["Submit", "SUBMIT", "0", "0", "submit", "Submit", "check", "col-md-12", "", true]}}}, "page_var": ["Register?", "social:person-add"]}}}}, "get_gateway_details": {"background_image": "radial-gradient(ellipse farthest-corner at 50% 100%,#00a656 10%,#4afcff  60%,#00a656 90%)", "name": "MChaama", "tagline": "Invest, Borrow, Grow", "default_color": {"color": "#00a858", "theme": "mchaama_theme_1", "color_bk": "#E0EEE0"}, "host": "mchaama.com", "logo": "administration_gateway_logo/M-Chaama_icon.png"}}, "chid": "1"},
	"LOGIN" : {
		'sec_hash' : 'QsD3FzcxafPiaaIV/VuCzNw/FAZa7JoLYlQTtbFV8QM=',
		'chid' : '3',
		'username' : 'customercare',
		'password' : 'User@Company123',
		'ip_address' : '197.237.248.163',
		'lat' : '1.0',
		'gateway_host' : 'mchaama.com',
		'response' : {
			'login' : {
				'api_key' : 'PSx7OGdbWHFcaQ==\n',
				'access_level' : 'OPERATOR',
				'status' : 'ACTIVATED'
			},
			'session' : 'NmU3NDc5MzM3MDY5NzA3ODZlNjY2YjMxNzQzMjZlNjk2NDZkNzg2MTZhNzY2MjY5NzI2NTc5MzM3NTY1MzU2MTY2NjQ2ZjM3NmM2ZDM5Njk2ZTY1Mzk3NDZjNzc3OTczNzM2NTY2Nzk3Njc3NzQ3NTcxNjg2NDMwN2EzMzZhNjg2YzMxNjc3ODYzMzA2Yzc4NmU2ZDZkMzYwYTc0NjQzMTZmNzU3YTc0NmY3Nzc2NmM2ZDcyNmI2NjYxNjU2MzZjNjc3MDY5NzA3OTZkNjY2NDY5Nzc3NjY2MmI2MTY3NjE2ZjcxNzg2ZTcyNmQ2NDZhNzg3NDZlNmE2YTc4NmE3MjYzNzQ2ODY3MzY2ZjZiNjc3MjYxNmI2YTZhNzg2NTc2NzE2NjZjN2E3MDc5Njc2MjM0MGE2YjczMzQ2ZTcyMzI3MDY1NjY2MzM5Njk2Njc0Nzk2OTYzNmI3NDZmNzY3MzY3Mzg2MTMwNjQ2ZjY0MzA2OTJmNjU2ODcxNmQ2OTZjNjQ2YzczNjg2MzMzNzI2OTc0NjEwYQ==',
			'redirect' : '/'
		},
		'lng' : '38.0',
		'response_status' : '00'
	},
	"LOGIN_SUCCESS" : "",
	"HOME_LOGGED_IN" : {
		"gateway_host" : "mchaama.com",
		"response_status" : "00",
		"session_id" : "Njk2YTdhNmY3MTc0NzA3OTZjMzA3ODYxNjU2NDYyNzY3NDZkNzgzMTc1Njg2NjY0Nzc3OTMwMzM3ODc2MzAzNjc1NzQ2MjYzNmU2NTYyNmI2YzYzMzQzOTZlMzM2ZjY5NjQ2NTZlNmE2MTY4MzA3NTcwN2E2YzMxNmYzMTZhN2E2ZDMwNmU2ODZhNjc2ODZjNzQ2NjZkMmIwYTc2NjQ3MzY4NzY2Njc1Nzk2NjY1NmU2Nzc0NzMzMTczNzI3NzYyNjY3MDc0Mzk2MjZhNjk3YTMwNzI3ODMxNjE2Njc4MzAzNTc1NzQ3NjZmNzUzMjMxNzU2ZjYzMzkzNjcxNzQzNDMwNmQzMjZmNmY2NjZlNzA2NzZkNjk3YTM1NjU2ZDZlMzI3OTZiNzkzNzcyNjk3MTcyMGE3MjY3NjI3NTY1NjQ2NjcwNjU3OTM0Mzg2NDc1Njg2Zjc1NjQ3ODY4NjY2ODcwNmE2ZDMxNjczMzc4NjMzNDM3NzM2NjZjNmY2MTY1MzU3MzZmNzMzNDcyNzk3OTYzNjgwYQ==",
		"chid" : "1",
		"lat" : "1.0",
		"lng" : "38.0",
		"ip_address" : "197.237.248.163",
		"response" : {
			"event_status" : "Gateway Profile Exists",
			"get_interface" : {
				"all_pages" : {
					"1" : {
						"Home" : {
							"3" : ["Transactions", "payment"],
							"2" : ["HOME", "home"]
						}
					},
					"98" : {
						"Address Book" : {
							"2" : ["Contacts", "communication:contacts"]
						}
					},
					"100" : {
						"Account" : {
							"1" : ["Reports", "view-list"],
							"2" : ["Payment Details", "payment"]
						}
					},
					"99" : {
						"bizzcom" : {
							"1" : ["Inbox", "inbox"],
							"2" : ["Compose", "create"],
							"4" : ["Sent", "done-all"]
						}
					}
				},
				"this_page_inputs" : {
					"1" : {
						"section_var" : ["Home", "home"],
						"3" : {
							"Transactions" : {
								"F" : {
									"Transactions" : {
										/* "1" : ["Submit", "SUBMIT", "0", "0", "submit", "Submit", "icons:send", "col-md-10", ""], */
										//Added extra tenth attribute if false element is not shown
										"1" : ["Submit", "SUBMIT", "0", "0", "submit", "Submit", "icons:send", "col-md-10", "",false],
										"2" : ["Transactions", "LIST SELECT", "1", "45", "transaction_list", "DATA SOURCE", "icons:list", "col-md-14", ""],
										/* */
										//Added extra tenth attribute if false element title is not shown
										//Added extra eleventh attribute for size setting
										"input_var" : ["BOOTSTRAP", "FORM", "0", "0", "PLACEHOLDER", "", "REGISTRY TRANSACTION TRENDS", "col-md-12", "loyalty", false, false,900]
									}
								}
							},
							"page_var" : ["Transactions", "payment"]
						},
						"2" : {
							"HOME" : {
								/* "1" : {
									"Profile" : {
										"1" : ["Profile Photo", "IMAGE ELEMENT", "0", "0", "profile_photo", "", "image", "col-md-10", ""],
										"A" : ["PROFILE INFO", "PROFILE INFO", "0", "0", "PROFILE INFO", null, "icon", "col-md-12", ""],
										"3" : ["Last Name", "TEXT VIEW", "0", "0", "last_name", "Care", "icons:perm-identity", "col-md-10", ""],
										"2" : ["First Name", "TEXT VIEW", "0", "0", "first_name", "Customer", "icons:perm-identity", "col-md-10", ""],
										"input_var" : ["BOOTSTRAP", "FORM", "0", "0", "PLACEHOLDER", "", "PROFILE INFO", "col-md-3", "account-box", false]
									}
								},*/
								"3" : {
									"Summary" : {
										"1" : ["SUMMARY", "GROUPED SELECT", "1", "45", "summary", "DATA SOURCE", "shopping-basket", "col-md-11", ""],
										"A" : ["TOTAL REGISTRIES", "BOXES", "0", "0", "SUMMARY BOX", null, "check-box-outline-blank", "col-md-2", ""],
										"C" : ["REGISTRY PRODUCTS", "BOXES", "0", "0", "SUMMARY BOX", null, "work", "col-md-2", ""],
										"D" : ["TOTAL PRODUCTS", "BOXES", "0", "0", "SUMMARY BOX", null, "check-box-outline-blank", "col-md-2", ""],
										/* */
										//Added extra tenth attribute if false element title is not shown
										//Added extra eleventh attribute for size setting
										"input_var" : ["BOOTSTRAP", "FORM", "0", "0", "PLACEHOLDER", "", "SUMMARY BOXES", "col-md-4", "settings", false, false,465]
									}
								},
								"2" : {
									"Float Manager" : {
										"2" : ["Float Manager", "LIST SELECT", "1", "45", "float_manager_list", "DATA SOURCE", "icons:list", "col-md-14", ""],
										/* */
										//Added extra tenth attribute if false element title is not shown
										//Added extra eleventh attribute for size setting
										"input_var" : ["BOOTSTRAP", "FORM", "0", "0", "PLACEHOLDER", "", "credits", "col-md-5", "receipt", false, false,570]
									}
								}
							},
							"page_var" : ["HOME", "home"]
						}
					},
					"98" : {
						"section_var" : ["Address Book", "communication:contacts"],
						"2" : {
							"Contacts" : {
								"1" : {
									"Contacts" : {
										/* "1" : ["Submit", "SUBMIT", "0", "0", "submit", "Submit", "icons:send", "col-md-10", ""], */
										//Added extra tenth attribute if false element is not shown
										"1" : ["Submit", "SUBMIT", "0", "0", "submit", "Submit", "icons:send", "col-md-10", "",false],
										"3" : ["Notification Product(s)", "GROUPED SELECT", "1", "45", "notification_product", "DATA SOURCE", "icon", "col-md-14", ""],
										"2" : ["Action", "STATIC DROPDOWN SELECT", "1", "45", "trigger", "View Subscribers|Upload Subscriber List|View Upload Status", "icons:unfold-more", "col-md-11", ""],
										/* */
										//Added extra tenth attribute if false element title is not shown
										//Added extra eleventh attribute for size setting
										"input_var" : ["NOTIFICATION PRODUCT ITEM VIEW", "FORM", "0", "0", "", "", "contacts", "col-md-10", "communication:contacts", false, false, 600]
									}
								}
							},
							"page_var" : ["Contacts", "communication:contacts"]
						}
					},
					"100" : {
						"section_var" : ["Account", "info"],
						"1" : {
							"Reports" : {
								"1" : {
									"REPORTS" : {
										/* */
										//Added extra tenth attribute if false element title is not shown
										//Added extra eleventh attribute for size setting
										"input_var" : ["REPORT", "FRAME FORM", "0", "0", "GET", "/report/", "reports", "col-md-3", "settings", false, false, 600],
										"3" : ["Report Format", "STATIC DROPDOWN SELECT", "1", "20", "response_format", "HTML|PDF|CSV", "editor:insert-drive-file", "col-md-12", ""],
										"2" : ["Report Type", "DROPDOWN SELECT", "1", "45", "data_name", "DATA SOURCE", "icons:file-download", "col-md-12", ""],
										"5" : ["End Date", "DATE", "1", "12", "end_date", "", "icons:date-range", "col-md-12", ""],
										"4" : ["Start Date", "DATE", "1", "12", "start_date", "", "icons:date-range", "col-md-12", ""],
										/* "7" : ["Submit", "SUBMIT", "0", "0", "submit", "Submit", "check", "col-md-12", ""], */
										//Added extra tenth attribute if false element is not shown
										"7" : ["Submit", "SUBMIT", "0", "0", "submit", "Submit", "check", "col-md-12", "",true],
										"6" : ["Filter", "TEXT INPUT", "0", "30", "q", "", "icons:filter-list", "col-md-12", ""]
									}
								}
							},
							"page_var" : ["Reports", "view-list"]
						},
						"2" : {
							"Payment Details" : {
								"1" : {
									"Add Payment Method" : {
										/* "1" : ["CONTINUE", "SUBMIT", "0", "0", "submit", "Submit", "icon", "col-md-10", ""], */
										//Added extra tenth attribute if false element is not shown
										"1" : ["CONTINUE", "SUBMIT", "0", "0", "submit", "Submit", "icon", "col-md-10", "",true],
										/* */
										//Added extra tenth attribute if false element title is not shown
										//Added extra eleventh attribute for size setting
										"input_var" : ["CARD DETAIL", "FORM", "0", "0", "", "", "add_payment_method", "col-md-4", "payment", false, true, 245]
									}
								},
								"3" : {
									"Available Payment Methods" : {
										/* "5" : ["Submit", "SUBMIT", "0", "0", "submit", "Submit", "icon", "col-md-10", ""], */
										//Added extra tenth attribute if false element is not shown
										"5" : ["Submit", "SUBMIT", "0", "0", "submit", "Submit", "icon", "col-md-10", "",true],
										/* */
										//Added extra tenth attribute if false element title is not shown
										//Added extra eleventh attribute for size setting
										"input_var" : ["BOOTSTRAP", "FORM", "0", "0", "PLACEHOLDER", "", "available_payment_methods", "col-md-5", "attach-money", false, true, 295]
									}
								}
							},
							"page_var" : ["Payment Details", "payment"]
						}
					},
					"99" : {
						"section_var" : ["bizzcom", "perm-phone-msg"],
						"1" : {
							"Inbox" : {
								"1" : {
									"Inbox" : {
										"3" : ["Inbound Message(s)", "GROUPED SELECT", "1", "45", "inbound", "DATA SOURCE", "icon", "col-md-11", ""],
										"2" : ["Submit", "SUBMIT", "0", "0", "submit", "Submit", "add-circle-outline", "col-md-11", ""],
										/* "input_var" : ["INBOX REPLY", "FORM", "0", "0", null, null, "inbox", "col-md-12", "inbox", false] */
										//Added extra tenth attribute if false element title is not shown
										//Added extra eleventh attribute for size setting
										"input_var" : ["INBOX REPLY", "FORM", "0", "0", null, null, "inbox", "col-md-12", "inbox", false,false,600]
									}
								}
							},
							"page_var" : ["Inbox", "inbox"]
						},
						"2" : {
							"Compose" : {
								"1" : {
									"Quick Send" : {
										"1" : ["Notification Product", "DROPDOWN SELECT", "1", "45", "notification_product_id", "DATA SOURCE", "communication:message", "col-md-11", ""],
										"2" : ["Message", "TEXT AREA", "2", "3000", "message", "", "communication:textsms", "col-md-11", ""],
										"3" : ["Mobile Number", "MSISDN INPUT", "10", "16", "MSISDN", "DATA SOURCE", "communication:contact-phone", "col-md-11", ""],
										/* "4" : ["Submit", "SUBMIT", "0", "0", "submit", "Submit", "icons:check", "col-md-11", ""], */
										//Added extra tenth attribute if false element is not shown
										"4" : ["Submit", "SUBMIT", "0", "0", "submit", "Submit", "icons:check", "col-md-11", "",true],
										/* "input_var" : ["SENDSMS", "FORM", "0", "0", "", "", "new_contact", "col-md-5", "create", false] */
										//Added extra tenth attribute if false element title is not shown
										//Added extra eleventh attribute for size setting
										"input_var" : ["SEND SMS", "FORM", "0", "0", "", "", "new_contact", "col-md-5", "create", false,true,300]
									}
								},
								"2" : {
									"Compose" : {
										"1" : ["Scheduled Time", "TIME", "2", "45", "scheduled_time", null, "icon", "col-md-5", ""],
										"2" : ["Scheduled Date", "DATE", "2", "45", "scheduled _date", null, "icon", "col-md-5", ""],
										/* "4" : ["Notification Product(s)", "GROUPED MULTI SELECT", "1", "45", "notification_product", "DATA SOURCE", "send", "col-md-11", ""] */
										//Added extra ninth attribute for scroller id height
										"4" : ["Notification Product(s)", "GROUPED MULTI SELECT", "1", "45", "notification_product", "DATA SOURCE", "send", "col-md-11", "",240],
										"3" : ["Message", "TEXT AREA", "2", "3000", "message", "", "communication:textsms", "col-md-11", ""],
										/* "1" : ["Submit", "SUBMIT", "0", "0", "submit", "Submit", "add-circle-outline", "col-md-11", ""], */
										//Added extra tenth attribute if false element is not shown
										"5" : ["Submit", "SUBMIT", "0", "0", "submit", "Submit", "add-circle-outline", "col-md-11", "",true],
										/* "input_var" : ["NOTIFICATION PRODUCT SEND", "FORM", "0", "0", "", "", "compose", "col-md-6", "create", false] */
										//Added extra tenth attribute if false element title is not shown
										//Added extra eleventh attribute for size setting
										"input_var" : ["NOTIFICATION PRODUCT SEND", "FORM", "0", "0", "", "", "compose", "col-md-6", "create", false,true,600]
									}
								}
							},
							"page_var" : ["Compose", "create"]
						},
						"4" : {
							"Sent" : {
								"1" : {
									"Sent" : {
										/* "1" : ["Submit", "SUBMIT", "0", "0", "submit", "Submit", "add-circle-outline", "col-md-10", ""], */
										//Added extra tenth attribute if false element is not shown
										"1" : ["Submit", "SUBMIT", "0", "0", "submit", "Submit", "add-circle-outline", "col-md-10", "",false],
										"2" : ["Outbound Message(s)", "GROUPED SELECT", "1", "45", "outbound", "DATA SOURCE", "icon", "col-md-14", ""],
										/* "input_var" : ["BOOTSTRAP", "FORM", "0", "0", "PLACEHOLDER", "", "sent", "col-md-12", "done-all", false] */
										//Added extra tenth attribute if false element title is not shown
										//Added extra eleventh attribute for size setting
										"input_var" : ["BOOTSTRAP", "FORM", "0", "0", "PLACEHOLDER", "", "sent", "col-md-12", "done-all", false,false,600]
									}
								}
							},
							"page_var" : ["Sent", "done-all"]
						}
					}
				}
			},
			"get_gateway_details" : {
				"website" : "mchaama.com",
				"background_image" : "radial-gradient(ellipse farthest-corner at 50% 100%,#4afcff 10%,#346b01 60%,#4afcff 90%)",
				"name" : "Amka Group",
				"tagline" : "Slicing the Pie",
				"default_color" : "#00a858",
				"logo" : "upc_institution_logo/amka_group_logo.png"
			}
		},
		"CHID" : "1"
	},
	"DATA_SOURCE" : {
		"institution_id" : {
			"gateway_host" : "amka.nikobizz.com",
			"SERVICE" : "DATA SOURCE",
			"service" : "DATA SOURCE",
			"response_status" : "00",
			"data_name" : "institution_id",
			"chid" : "1",
			"lat" : "1.0",
			"lng" : "38.0",
			"ip_address" : "154.70.45.20",
			"response" : {
				"data_source" : {
					"rows" : [[38, "Amka Group"]],
					"data" : [],
					"cols" : [],
					"min_id" : 0,
					"groups" : [],
					"max_id" : 0
				}
			},
			"CHID" : "1"
		},
		"item=Membership Plan" : {
			"gateway_host" : "amka.nikobizz.com",
			"SERVICE" : "DATA SOURCE",
			"service" : "DATA SOURCE",
			"response_status" : "00",
			"data_name" : "item=Membership Plan",
			"chid" : "1",
			"lat" : "1.0",
			"lng" : "38.0",
			"ip_address" : "154.70.45.20",
			"response" : {
				"data_source" : {
					"rows" : [["AMKA M-Chaama Membership", "AMKA M-Chaama Membership"]],
					"data" : [],
					"cols" : [],
					"min_id" : 0,
					"groups" : [],
					"max_id" : 0
				}
			},
			"CHID" : "1"
		},
		"MSISDN" : {
			"gateway_host" : "amka.nikobizz.com",
			"SERVICE" : "DATA SOURCE",
			"service" : "DATA SOURCE",
			"response_status" : "00",
			"data_name" : "MSISDN",
			"chid" : "1",
			"lat" : "1.0",
			"lng" : "38.0",
			"ip_address" : "154.70.45.20",
			"response" : {
				"data_source" : {
					"rows" : [["213", "Algeria"], ["1", "Antigua and Barbuda"], ["994", "Azerbaijan"], ["685", "American Samoa"], ["500", "South Georgia South Sandwich Islands"], ["244", "Angola"], ["54", "Argentina"], ["61", "Australia"], ["245", "Guinea-Bissau"], ["675", "Papua New Guinea"], ["1", "Bahamas"], ["880", "Bangladesh"], ["1", "Barbados"], ["1", "Bermuda"], ["673", "Brunei Darussalam"], ["855", "Cambodia"], ["1", "Canada"], ["350", "Gibraltar"], ["358", "\u00c3\u0085land Islands"], ["57", "Colombia"], ["269", "Comoros"], ["44", "Isle of Man"], ["421", "Slovakia"], ["591", "Bolivia"], ["95", "Burma"], ["55", "Brazil"], ["359", "Bulgaria"], ["352", "Luxembourg"], ["677", "Solomon Islands"], ["509", "Haiti"], ["242", "Congo"], ["243", "Democratic Republic of the Congo"], ["86", "China"], ["93", "Afghanistan"], ["238", "Cape Verde"], ["377", "Monaco"], ["237", "Cameroon"], ["1", "Cayman Islands"], ["235", "Chad"], ["56", "Chile"], ["236", "Central African Republic"], ["53", "Cuba"], ["357", "Cyprus"], ["45", "Denmark"], ["1", "Dominica"], ["240", "Equatorial Guinea"], ["291", "Eritrea"], ["372", "Estonia"], ["593", "Ecuador"], ["20", "Egypt"], ["353", "Ireland"], ["43", "Austria"], ["420", "Czech Republic"], ["251", "Ethiopia"], ["962", "Jordan"], ["679", "Fiji"], ["358", "Finland"], ["686", "Kiribati"], ["1", "Trinidad and Tobago"], ["992", "Tajikistan"], ["66", "Thailand"], ["690", "Tokelau"], [null, "Micronesia, Federated States of"], ["856", "Lao People's Democratic Republic"], ["689", "French Polynesia"], ["7", "Kazakhstan"], ["961", "Lebanon"], ["49", "Germany"], ["233", "Ghana"], ["30", "Greece"], ["299", "Greenland"], ["1", "Grenada"], ["1", "Guam"], ["224", "Guinea"], ["375", "Belarus"], ["592", "Guyana"], ["371", "Latvia"], ["370", "Lithuania"], ["970", "Palestine"], ["385", "Croatia"], ["504", "Honduras"], ["36", "Hungary"], ["354", "Iceland"], ["91", "India"], ["98", "Iran (Islamic Republic of)"], ["976", "Mongolia"], [null, "Cote d'Ivoire"], ["39", "Italy"], ["1", "Jamaica"], ["81", "Japan"], ["996", "Kyrgyzstan"], ["1", "Montserrat"], ["261", "Madagascar"], ["596", "Martinique"], ["265", "Malawi"], ["60", "Malaysia"], ["960", "Maldives"], ["223", "Mali"], ["356", "Malta"], ["222", "Mauritania"], ["230", "Mauritius"], ["52", "Mexico"], ["212", "Morocco"], ["258", "Mozambique"], ["687", "New Caledonia"], ["227", "Niger"], ["683", "Niue"], ["968", "Oman"], ["852", "Hong Kong"], ["1", "United States Minor Outlying Islands"], ["674", "Nauru"], ["977", "Nepal"], ["31", "Netherlands"], ["234", "Nigeria"], ["47", "Norway"], ["678", "Vanuatu"], [null, "Bouvet Island"], [null, "French Southern and Antarctic Lands"], [null, "Heard Island and McDonald Islands"], ["672", "Antarctica"], ["61", "Christmas Island"], ["61", "Cocos (Keeling) Islands"], ["672", "Norfolk Island"], ["351", "Portugal"], ["505", "Nicaragua"], ["92", "Pakistan"], ["507", "Panama"], ["595", "Paraguay"], ["51", "Peru"], ["48", "Poland"], ["262", "Reunion"], ["7", "Russia"], ["974", "Qatar"], ["40", "Romania"], ["63", "Philippines"], ["1", "Saint Kitts and Nevis"], ["966", "Saudi Arabia"], ["248", "Seychelles"], ["27", "South Africa"], ["221", "Senegal"], ["65", "Singapore"], ["252", "Somalia"], ["34", "Spain"], ["249", "Sudan"], ["46", "Sweden"], ["41", "Switzerland"], ["255", "United Republic of Tanzania"], ["993", "Turkmenistan"], ["688", "Tuvalu"], ["239", "Sao Tome and Principe"], ["216", "Tunisia"], ["90", "Turkey"], ["84", "Vietnam"], ["44", "United Kingdom"], ["1", "United States"], ["380", "Ukraine"], ["998", "Uzbekistan"], ["58", "Venezuela"], ["685", "Samoa"], ["62", "Indonesia"], ["264", "Namibia"], ["268", "Swaziland"], ["681", "Wallis and Futuna Islands"], ["967", "Yemen"], ["260", "Zambia"], ["263", "Zimbabwe"], ["599", "Netherlands Antilles"], [null, "Timor-Leste"], ["64", "Pitcairn Islands"], ["680", "Palau"], ["290", "Saint Helena"], ["590", "Saint Barthelemy"], ["590", "Saint Martin"], ["44", "Jersey"], ["381", "Serbia"], ["47", "Svalbard"], ["39", "Holy See (Vatican City)"], ["850", "North Korea"], ["82", "South Korea"], ["355", "Albania"], ["376", "Andorra"], ["1", "Anguilla"], ["374", "Armenia"], ["297", "Aruba"], ["973", "Bahrain"], ["501", "Belize"], ["32", "Belgium"], ["229", "Benin"], ["975", "Bhutan"], ["387", "Bosnia and Herzegovina"], ["267", "Botswana"], ["246", "British Indian Ocean Territory"], ["226", "Burkina Faso"], ["257", "Burundi"], ["682", "Cook Islands"], ["253", "Djibouti"], ["506", "Costa Rica"], ["1", "Dominican Republic"], ["503", "El Salvador"], ["500", "Falkland Islands (Malvinas)"], ["298", "Faroe Islands"], ["33", "France"], ["965", "Kuwait"], ["594", "French Guiana"], ["241", "Gabon"], ["220", "Gambia"], ["266", "Lesotho"], ["995", "Georgia"], ["590", "Guadeloupe"], ["502", "Guatemala"], ["44", "Guernsey"], ["964", "Iraq"], ["262", "Mayotte"], ["972", "Israel"], ["378", "San Marino"], ["254", "Kenya"], ["231", "Liberia"], ["218", "Libyan Arab Jamahiriya"], ["423", "Liechtenstein"], ["853", "Macau"], ["389", "The former Yugoslav Republic of Macedonia"], ["692", "Marshall Islands"], ["373", "Republic of Moldova"], ["382", "Montenegro"], ["64", "New Zealand"], ["1", "United States Virgin Islands"], ["1", "Northern Mariana Islands"], ["1", "Puerto Rico"], ["250", "Rwanda"], ["1", "Saint Lucia"], ["508", "Saint Pierre and Miquelon"], ["1", "Saint Vincent and the Grenadines"], ["232", "Sierra Leone"], ["386", "Slovenia"], ["94", "Sri Lanka"], ["597", "Suriname"], ["963", "Syrian Arab Republic"], ["886", "Taiwan"], ["228", "Togo"], ["676", "Tonga"], ["1", "Turks and Caicos Islands"], ["256", "Uganda"], ["971", "United Arab Emirates"], ["598", "Uruguay"], ["1", "British Virgin Islands"], ["212", "Western Sahara"]],
					"data" : [],
					"cols" : [],
					"min_id" : 0,
					"groups" : [],
					"max_id" : 0
				}
			},
			"CHID" : "1"
		},
		"float_manager_list" : {
			"gateway_host" : "mchaama.com",
			"response_status" : "00",
			"data_name" : "float_manager_list",
			"session_id" : "Njk2YTdhNmY3MTc0NzA3OTZjMzA3ODYxNjU2NDYyNzY3NDZkNzgzMTc1Njg2NjY0Nzc3OTMwMzM3ODc2MzAzNjc1NzQ2MjYzNmU2NTYyNmI2YzYzMzQzOTZlMzM2ZjY5NjQ2NTZlNmE2MTY4MzA3NTcwN2E2YzMxNmYzMTZhN2E2ZDMwNmU2ODZhNjc2ODZjNzQ2NjZkMmIwYTc2NjQ3MzY4NzY2Njc1Nzk2NjY1NmU2Nzc0NzMzMTczNzI3NzYyNjY3MDc0Mzk2MjZhNjk3YTMwNzI3ODMxNjE2Njc4MzAzNTc1NzQ3NjZmNzUzMjMxNzU2ZjYzMzkzNjcxNzQzNDMwNmQzMjZmNmY2NjZlNzA2NzZkNjk3YTM1NjU2ZDZlMzI3OTZiNzkzNzcyNjk3MTcyMGE3MjY3NjI3NTY1NjQ2NjcwNjU3OTM0Mzg2NDc1Njg2Zjc1NjQ3ODY4NjY2ODcwNmE2ZDMxNjczMzc4NjMzNDM3NzM2NjZjNmY2MTY1MzU3MzZmNzMzNDcyNzk3OTYzNjgwYQ==",
			"chid" : "1",
			"lat" : "1.0",
			"lng" : "38.0",
			"ip_address" : "197.237.248.163",
			"response" : {
				"data_source" : {
					"rows" : [[{
								"index" : 5044,
								"date_time" : "14 Sep 2016 02:36:12 PM EAT +0300",
								"type" : "Bulk SMS (Kenya)",
								"description" : "ext: 320350 | Amount: 2,896.00 Charge: 0.00 Balance: 5,965.00",
								"name" : "Debit"
							}
						], [{
								"index" : 5043,
								"date_time" : "12 Sep 2016 07:36:14 PM EAT +0300",
								"type" : "Bulk SMS (Kenya)",
								"description" : "ext: 320338 | Amount: 1.00 Charge: 0.00 Balance: 8,861.00",
								"name" : "Debit"
							}
						], [{
								"index" : 5042,
								"date_time" : "12 Sep 2016 07:36:13 PM EAT +0300",
								"type" : "M-PESA",
								"description" : "ext: 320338 | Amount: 2,000.00 Charge: 43.00 Balance: 68,235.00",
								"name" : "Debit"
							}
						], [{
								"index" : 5041,
								"date_time" : "12 Sep 2016 07:30:28 PM EAT +0300",
								"type" : "Bulk SMS (Kenya)",
								"description" : "ext: 320336 | Amount: 1.00 Charge: 0.00 Balance: 8,862.00",
								"name" : "Debit"
							}
						], [{
								"index" : 5038,
								"date_time" : "12 Sep 2016 11:59:20 AM EAT +0300",
								"type" : "Bulk SMS (Kenya)",
								"description" : "ext: 320319 | Amount: 2,896.00 Charge: 0.00 Balance: 8,863.00",
								"name" : "Debit"
							}
						], [{
								"index" : 5037,
								"date_time" : "12 Sep 2016 07:29:13 AM EAT +0300",
								"type" : "Bulk SMS (Kenya)",
								"description" : "ext: 320317 | Amount: 1.00 Charge: 0.00 Balance: 11,759.00",
								"name" : "Debit"
							}
						], [{
								"index" : 5035,
								"date_time" : "11 Sep 2016 08:38:25 PM EAT +0300",
								"type" : "Bulk SMS (Kenya)",
								"description" : "ext: 320311 | Amount: 1.00 Charge: 0.00 Balance: 11,760.00",
								"name" : "Debit"
							}
						], [{
								"index" : 5032,
								"date_time" : "10 Sep 2016 08:07:52 PM EAT +0300",
								"type" : "Bulk SMS (Kenya)",
								"description" : "ext: 320282 | Amount: 1.00 Charge: 0.00 Balance: 11,761.00",
								"name" : "Debit"
							}
						], [{
								"index" : 5030,
								"date_time" : "10 Sep 2016 02:19:14 PM EAT +0300",
								"type" : "Bulk SMS (Kenya)",
								"description" : "ext: 320278 | Amount: 1.00 Charge: 0.00 Balance: 11,762.00",
								"name" : "Debit"
							}
						], [{
								"index" : 5029,
								"date_time" : "10 Sep 2016 02:19:13 PM EAT +0300",
								"type" : "M-PESA",
								"description" : "ext: 320278 | Amount: 1,400.00 Charge: 43.00 Balance: 70,278.00",
								"name" : "Debit"
							}
						], [{
								"index" : 5028,
								"date_time" : "09 Sep 2016 04:10:15 PM EAT +0300",
								"type" : "Bulk SMS (Kenya)",
								"description" : "ext: 320258 | Amount: 1.00 Charge: 0.00 Balance: 11,763.00",
								"name" : "Debit"
							}
						], [{
								"index" : 5027,
								"date_time" : "09 Sep 2016 02:50:20 PM EAT +0300",
								"type" : "Bulk SMS (Kenya)",
								"description" : "ext: 320246 | Amount: 1.00 Charge: 0.00 Balance: 11,764.00",
								"name" : "Debit"
							}
						], [{
								"index" : 5025,
								"date_time" : "09 Sep 2016 11:13:05 AM EAT +0300",
								"type" : "Bulk SMS (Kenya)",
								"description" : "ext: 320242 | Amount: 1.00 Charge: 0.00 Balance: 11,765.00",
								"name" : "Debit"
							}
						], [{
								"index" : 5023,
								"date_time" : "08 Sep 2016 02:40:00 PM EAT +0300",
								"type" : "Bulk SMS (Kenya)",
								"description" : "ext: 320198 | Amount: 1.00 Charge: 0.00 Balance: 11,766.00",
								"name" : "Debit"
							}
						], [{
								"index" : 5022,
								"date_time" : "08 Sep 2016 02:38:17 PM EAT +0300",
								"type" : "Bulk SMS (Kenya)",
								"description" : "ext: 320196 | Amount: 1.00 Charge: 0.00 Balance: 11,767.00",
								"name" : "Debit"
							}
						], [{
								"index" : 5021,
								"date_time" : "08 Sep 2016 02:37:47 PM EAT +0300",
								"type" : "Bulk SMS (Kenya)",
								"description" : "ext: 320195 | Amount: 1.00 Charge: 0.00 Balance: 11,768.00",
								"name" : "Debit"
							}
						], [{
								"index" : 5020,
								"date_time" : "08 Sep 2016 02:36:40 PM EAT +0300",
								"type" : "Bulk SMS (Kenya)",
								"description" : "ext: 320193 | Amount: 1.00 Charge: 0.00 Balance: 11,769.00",
								"name" : "Debit"
							}
						], [{
								"index" : 5019,
								"date_time" : "08 Sep 2016 01:32:08 PM EAT +0300",
								"type" : "Bulk SMS (Kenya)",
								"description" : "ext: 320192 | Amount: 1.00 Charge: 0.00 Balance: 11,770.00",
								"name" : "Debit"
							}
						], [{
								"index" : 5018,
								"date_time" : "08 Sep 2016 01:32:08 PM EAT +0300",
								"type" : "M-PESA",
								"description" : "ext: 320192 | Amount: 1,000.00 Charge: 43.00 Balance: 71,721.00",
								"name" : "Debit"
							}
						], [{
								"index" : 5017,
								"date_time" : "08 Sep 2016 09:36:37 AM EAT +0300",
								"type" : "Bulk SMS (Kenya)",
								"description" : "ext: 320187 | Amount: 1.00 Charge: 0.00 Balance: 11,771.00",
								"name" : "Debit"
							}
						], [{
								"index" : 5016,
								"date_time" : "07 Sep 2016 04:07:00 PM EAT +0300",
								"type" : "M-PESA",
								"description" : "ext: 320169 | Amount: 1,000.00 Charge: 43.00 Balance: 72,764.00",
								"name" : "Debit"
							}
						], [{
								"index" : 5015,
								"date_time" : "07 Sep 2016 04:06:16 PM EAT +0300",
								"type" : "M-PESA",
								"description" : "ext: 320168 | Amount: 1,000.00 Charge: 43.00 Balance: 73,807.00",
								"name" : "Debit"
							}
						], [{
								"index" : 5014,
								"date_time" : "07 Sep 2016 04:05:43 PM EAT +0300",
								"type" : "M-PESA",
								"description" : "ext: 320167 | Amount: 1,000.00 Charge: 43.00 Balance: 74,850.00",
								"name" : "Debit"
							}
						], [{
								"index" : 5013,
								"date_time" : "07 Sep 2016 04:05:41 PM EAT +0300",
								"type" : "M-PESA",
								"description" : "ext: 320166 | Amount: 1,000.00 Charge: 43.00 Balance: 75,893.00",
								"name" : "Debit"
							}
						], [{
								"index" : 5012,
								"date_time" : "07 Sep 2016 10:26:50 AM EAT +0300",
								"type" : "Bulk SMS (Kenya)",
								"description" : "ext: 320157 | Amount: 2,896.00 Charge: 0.00 Balance: 11,772.00",
								"name" : "Debit"
							}
						], [{
								"index" : 5011,
								"date_time" : "06 Sep 2016 02:50:01 PM EAT +0300",
								"type" : "Bulk SMS (Kenya)",
								"description" : "ext: 320154 | Amount: 1.00 Charge: 0.00 Balance: 14,668.00",
								"name" : "Debit"
							}
						], [{
								"index" : 5010,
								"date_time" : "06 Sep 2016 02:50:00 PM EAT +0300",
								"type" : "M-PESA",
								"description" : "ext: 320154 | Amount: 1,000.00 Charge: 43.00 Balance: 76,936.00",
								"name" : "Debit"
							}
						], [{
								"index" : 5009,
								"date_time" : "06 Sep 2016 01:47:43 PM EAT +0300",
								"type" : "Bulk SMS (Kenya)",
								"description" : "ext: 320152 | Amount: 1.00 Charge: 0.00 Balance: 14,669.00",
								"name" : "Debit"
							}
						], [{
								"index" : 5008,
								"date_time" : "06 Sep 2016 01:28:51 PM EAT +0300",
								"type" : "Bulk SMS (Kenya)",
								"description" : "ext: 320150 | Amount: 1.00 Charge: 0.00 Balance: 14,670.00",
								"name" : "Debit"
							}
						], [{
								"index" : 5007,
								"date_time" : "06 Sep 2016 01:22:11 PM EAT +0300",
								"type" : "Bulk SMS (Kenya)",
								"description" : "ext: 320147 | Amount: 1.00 Charge: 0.00 Balance: 14,671.00",
								"name" : "Debit"
							}
						], [{
								"index" : 5006,
								"date_time" : "06 Sep 2016 01:20:33 PM EAT +0300",
								"type" : "Bulk SMS (Kenya)",
								"description" : "ext: 320146 | Amount: 1.00 Charge: 0.00 Balance: 14,672.00",
								"name" : "Debit"
							}
						], [{
								"index" : 5002,
								"date_time" : "05 Sep 2016 03:13:32 PM EAT +0300",
								"type" : "Bulk SMS (Kenya)",
								"description" : "ext: 320104 | Amount: 1.00 Charge: 0.00 Balance: 14,673.00",
								"name" : "Debit"
							}
						], [{
								"index" : 5001,
								"date_time" : "05 Sep 2016 03:11:36 PM EAT +0300",
								"type" : "Bulk SMS (Kenya)",
								"description" : "ext: 320103 | Amount: 1.00 Charge: 0.00 Balance: 14,674.00",
								"name" : "Debit"
							}
						], [{
								"index" : 4997,
								"date_time" : "04 Sep 2016 05:21:03 PM EAT +0300",
								"type" : "Bulk SMS (Kenya)",
								"description" : "ext: 320075 | Amount: 1.00 Charge: 0.00 Balance: 14,675.00",
								"name" : "Debit"
							}
						], [{
								"index" : 4993,
								"date_time" : "03 Sep 2016 10:18:29 AM EAT +0300",
								"type" : "Bulk SMS (Kenya)",
								"description" : "ext: 320054 | Amount: 1.00 Charge: 0.00 Balance: 14,676.00",
								"name" : "Debit"
							}
						], [{
								"index" : 4992,
								"date_time" : "03 Sep 2016 10:18:28 AM EAT +0300",
								"type" : "M-PESA",
								"description" : "ext: 320054 | Amount: 1,000.00 Charge: 43.00 Balance: 77,979.00",
								"name" : "Debit"
							}
						], [{
								"index" : 4991,
								"date_time" : "03 Sep 2016 09:41:26 AM EAT +0300",
								"type" : "Bulk SMS (Kenya)",
								"description" : "ext: 320053 | Amount: 2,896.00 Charge: 0.00 Balance: 14,677.00",
								"name" : "Debit"
							}
						], [{
								"index" : 4990,
								"date_time" : "03 Sep 2016 08:40:35 AM EAT +0300",
								"type" : "Bulk SMS (Kenya)",
								"description" : "ext: 320052 | Amount: 1.00 Charge: 0.00 Balance: 17,573.00",
								"name" : "Debit"
							}
						], [{
								"index" : 4989,
								"date_time" : "03 Sep 2016 08:38:52 AM EAT +0300",
								"type" : "Bulk SMS (Kenya)",
								"description" : "ext: 320049 | Amount: 1.00 Charge: 0.00 Balance: 17,574.00",
								"name" : "Debit"
							}
						], [{
								"index" : 4988,
								"date_time" : "03 Sep 2016 08:32:48 AM EAT +0300",
								"type" : "Bulk SMS (Kenya)",
								"description" : "ext: 320048 | Amount: 1.00 Charge: 0.00 Balance: 17,575.00",
								"name" : "Debit"
							}
						], [{
								"index" : 4981,
								"date_time" : "01 Sep 2016 09:49:58 AM EAT +0300",
								"type" : "Bulk SMS (Kenya)",
								"description" : "ext: 319923 | Amount: 2,896.00 Charge: 0.00 Balance: 17,576.00",
								"name" : "Debit"
							}
						], [{
								"index" : 4980,
								"date_time" : "01 Sep 2016 09:31:37 AM EAT +0300",
								"type" : "Bulk SMS (Kenya)",
								"description" : "ext:  | Amount: 20,000.00 Charge: 0.00 Balance: 20,472.00",
								"name" : "Credit"
							}
						], [{
								"index" : 4979,
								"date_time" : "31 Aug 2016 09:02:59 AM EAT +0300",
								"type" : "Bulk SMS (Kenya)",
								"description" : "ext: 319907 | Amount: 1.00 Charge: 0.00 Balance: 472.00",
								"name" : "Debit"
							}
						], [{
								"index" : 4978,
								"date_time" : "30 Aug 2016 10:24:20 AM EAT +0300",
								"type" : "Bulk SMS (Kenya)",
								"description" : "ext: 319846 | Amount: 1.00 Charge: 0.00 Balance: 473.00",
								"name" : "Debit"
							}
						], [{
								"index" : 4977,
								"date_time" : "30 Aug 2016 10:19:36 AM EAT +0300",
								"type" : "Bulk SMS (Kenya)",
								"description" : "ext: 319845 | Amount: 1.00 Charge: 0.00 Balance: 474.00",
								"name" : "Debit"
							}
						], [{
								"index" : 4975,
								"date_time" : "29 Aug 2016 05:16:51 PM EAT +0300",
								"type" : "Bulk SMS (Kenya)",
								"description" : "ext: 319830 | Amount: 1.00 Charge: 0.00 Balance: 475.00",
								"name" : "Debit"
							}
						], [{
								"index" : 4974,
								"date_time" : "29 Aug 2016 05:16:51 PM EAT +0300",
								"type" : "M-PESA",
								"description" : "ext: 319830 | Amount: 2,000.00 Charge: 43.00 Balance: 79,022.00",
								"name" : "Debit"
							}
						], [{
								"index" : 4973,
								"date_time" : "29 Aug 2016 05:14:12 PM EAT +0300",
								"type" : "Bulk SMS (Kenya)",
								"description" : "ext: 319829 | Amount: 1.00 Charge: 0.00 Balance: 476.00",
								"name" : "Debit"
							}
						], [{
								"index" : 4971,
								"date_time" : "27 Aug 2016 10:42:53 PM EAT +0300",
								"type" : "Bulk SMS (Kenya)",
								"description" : "ext: 319792 | Amount: 1.00 Charge: 0.00 Balance: 477.00",
								"name" : "Debit"
							}
						], [{
								"index" : 4970,
								"date_time" : "27 Aug 2016 10:41:11 PM EAT +0300",
								"type" : "Bulk SMS (Kenya)",
								"description" : "ext: 319791 | Amount: 1.00 Charge: 0.00 Balance: 478.00",
								"name" : "Debit"
							}
						]],
					"data" : [[{
								"index" : 5044,
								"date_time" : "14 Sep 2016 02:36:12 PM EAT +0300",
								"type" : "Bulk SMS (Kenya)",
								"description" : "ext: 320350 | Amount: 2,896.00 Charge: 0.00 Balance: 5,965.00",
								"name" : "Debit"
							}
						], [{
								"index" : 5043,
								"date_time" : "12 Sep 2016 07:36:14 PM EAT +0300",
								"type" : "Bulk SMS (Kenya)",
								"description" : "ext: 320338 | Amount: 1.00 Charge: 0.00 Balance: 8,861.00",
								"name" : "Debit"
							}
						], [{
								"index" : 5042,
								"date_time" : "12 Sep 2016 07:36:13 PM EAT +0300",
								"type" : "M-PESA",
								"description" : "ext: 320338 | Amount: 2,000.00 Charge: 43.00 Balance: 68,235.00",
								"name" : "Debit"
							}
						], [{
								"index" : 5041,
								"date_time" : "12 Sep 2016 07:30:28 PM EAT +0300",
								"type" : "Bulk SMS (Kenya)",
								"description" : "ext: 320336 | Amount: 1.00 Charge: 0.00 Balance: 8,862.00",
								"name" : "Debit"
							}
						], [{
								"index" : 5038,
								"date_time" : "12 Sep 2016 11:59:20 AM EAT +0300",
								"type" : "Bulk SMS (Kenya)",
								"description" : "ext: 320319 | Amount: 2,896.00 Charge: 0.00 Balance: 8,863.00",
								"name" : "Debit"
							}
						], [{
								"index" : 5037,
								"date_time" : "12 Sep 2016 07:29:13 AM EAT +0300",
								"type" : "Bulk SMS (Kenya)",
								"description" : "ext: 320317 | Amount: 1.00 Charge: 0.00 Balance: 11,759.00",
								"name" : "Debit"
							}
						], [{
								"index" : 5035,
								"date_time" : "11 Sep 2016 08:38:25 PM EAT +0300",
								"type" : "Bulk SMS (Kenya)",
								"description" : "ext: 320311 | Amount: 1.00 Charge: 0.00 Balance: 11,760.00",
								"name" : "Debit"
							}
						], [{
								"index" : 5032,
								"date_time" : "10 Sep 2016 08:07:52 PM EAT +0300",
								"type" : "Bulk SMS (Kenya)",
								"description" : "ext: 320282 | Amount: 1.00 Charge: 0.00 Balance: 11,761.00",
								"name" : "Debit"
							}
						], [{
								"index" : 5030,
								"date_time" : "10 Sep 2016 02:19:14 PM EAT +0300",
								"type" : "Bulk SMS (Kenya)",
								"description" : "ext: 320278 | Amount: 1.00 Charge: 0.00 Balance: 11,762.00",
								"name" : "Debit"
							}
						], [{
								"index" : 5029,
								"date_time" : "10 Sep 2016 02:19:13 PM EAT +0300",
								"type" : "M-PESA",
								"description" : "ext: 320278 | Amount: 1,400.00 Charge: 43.00 Balance: 70,278.00",
								"name" : "Debit"
							}
						], [{
								"index" : 5028,
								"date_time" : "09 Sep 2016 04:10:15 PM EAT +0300",
								"type" : "Bulk SMS (Kenya)",
								"description" : "ext: 320258 | Amount: 1.00 Charge: 0.00 Balance: 11,763.00",
								"name" : "Debit"
							}
						], [{
								"index" : 5027,
								"date_time" : "09 Sep 2016 02:50:20 PM EAT +0300",
								"type" : "Bulk SMS (Kenya)",
								"description" : "ext: 320246 | Amount: 1.00 Charge: 0.00 Balance: 11,764.00",
								"name" : "Debit"
							}
						], [{
								"index" : 5025,
								"date_time" : "09 Sep 2016 11:13:05 AM EAT +0300",
								"type" : "Bulk SMS (Kenya)",
								"description" : "ext: 320242 | Amount: 1.00 Charge: 0.00 Balance: 11,765.00",
								"name" : "Debit"
							}
						], [{
								"index" : 5023,
								"date_time" : "08 Sep 2016 02:40:00 PM EAT +0300",
								"type" : "Bulk SMS (Kenya)",
								"description" : "ext: 320198 | Amount: 1.00 Charge: 0.00 Balance: 11,766.00",
								"name" : "Debit"
							}
						], [{
								"index" : 5022,
								"date_time" : "08 Sep 2016 02:38:17 PM EAT +0300",
								"type" : "Bulk SMS (Kenya)",
								"description" : "ext: 320196 | Amount: 1.00 Charge: 0.00 Balance: 11,767.00",
								"name" : "Debit"
							}
						], [{
								"index" : 5021,
								"date_time" : "08 Sep 2016 02:37:47 PM EAT +0300",
								"type" : "Bulk SMS (Kenya)",
								"description" : "ext: 320195 | Amount: 1.00 Charge: 0.00 Balance: 11,768.00",
								"name" : "Debit"
							}
						], [{
								"index" : 5020,
								"date_time" : "08 Sep 2016 02:36:40 PM EAT +0300",
								"type" : "Bulk SMS (Kenya)",
								"description" : "ext: 320193 | Amount: 1.00 Charge: 0.00 Balance: 11,769.00",
								"name" : "Debit"
							}
						], [{
								"index" : 5019,
								"date_time" : "08 Sep 2016 01:32:08 PM EAT +0300",
								"type" : "Bulk SMS (Kenya)",
								"description" : "ext: 320192 | Amount: 1.00 Charge: 0.00 Balance: 11,770.00",
								"name" : "Debit"
							}
						], [{
								"index" : 5018,
								"date_time" : "08 Sep 2016 01:32:08 PM EAT +0300",
								"type" : "M-PESA",
								"description" : "ext: 320192 | Amount: 1,000.00 Charge: 43.00 Balance: 71,721.00",
								"name" : "Debit"
							}
						], [{
								"index" : 5017,
								"date_time" : "08 Sep 2016 09:36:37 AM EAT +0300",
								"type" : "Bulk SMS (Kenya)",
								"description" : "ext: 320187 | Amount: 1.00 Charge: 0.00 Balance: 11,771.00",
								"name" : "Debit"
							}
						], [{
								"index" : 5016,
								"date_time" : "07 Sep 2016 04:07:00 PM EAT +0300",
								"type" : "M-PESA",
								"description" : "ext: 320169 | Amount: 1,000.00 Charge: 43.00 Balance: 72,764.00",
								"name" : "Debit"
							}
						], [{
								"index" : 5015,
								"date_time" : "07 Sep 2016 04:06:16 PM EAT +0300",
								"type" : "M-PESA",
								"description" : "ext: 320168 | Amount: 1,000.00 Charge: 43.00 Balance: 73,807.00",
								"name" : "Debit"
							}
						], [{
								"index" : 5014,
								"date_time" : "07 Sep 2016 04:05:43 PM EAT +0300",
								"type" : "M-PESA",
								"description" : "ext: 320167 | Amount: 1,000.00 Charge: 43.00 Balance: 74,850.00",
								"name" : "Debit"
							}
						], [{
								"index" : 5013,
								"date_time" : "07 Sep 2016 04:05:41 PM EAT +0300",
								"type" : "M-PESA",
								"description" : "ext: 320166 | Amount: 1,000.00 Charge: 43.00 Balance: 75,893.00",
								"name" : "Debit"
							}
						], [{
								"index" : 5012,
								"date_time" : "07 Sep 2016 10:26:50 AM EAT +0300",
								"type" : "Bulk SMS (Kenya)",
								"description" : "ext: 320157 | Amount: 2,896.00 Charge: 0.00 Balance: 11,772.00",
								"name" : "Debit"
							}
						], [{
								"index" : 5011,
								"date_time" : "06 Sep 2016 02:50:01 PM EAT +0300",
								"type" : "Bulk SMS (Kenya)",
								"description" : "ext: 320154 | Amount: 1.00 Charge: 0.00 Balance: 14,668.00",
								"name" : "Debit"
							}
						], [{
								"index" : 5010,
								"date_time" : "06 Sep 2016 02:50:00 PM EAT +0300",
								"type" : "M-PESA",
								"description" : "ext: 320154 | Amount: 1,000.00 Charge: 43.00 Balance: 76,936.00",
								"name" : "Debit"
							}
						], [{
								"index" : 5009,
								"date_time" : "06 Sep 2016 01:47:43 PM EAT +0300",
								"type" : "Bulk SMS (Kenya)",
								"description" : "ext: 320152 | Amount: 1.00 Charge: 0.00 Balance: 14,669.00",
								"name" : "Debit"
							}
						], [{
								"index" : 5008,
								"date_time" : "06 Sep 2016 01:28:51 PM EAT +0300",
								"type" : "Bulk SMS (Kenya)",
								"description" : "ext: 320150 | Amount: 1.00 Charge: 0.00 Balance: 14,670.00",
								"name" : "Debit"
							}
						], [{
								"index" : 5007,
								"date_time" : "06 Sep 2016 01:22:11 PM EAT +0300",
								"type" : "Bulk SMS (Kenya)",
								"description" : "ext: 320147 | Amount: 1.00 Charge: 0.00 Balance: 14,671.00",
								"name" : "Debit"
							}
						], [{
								"index" : 5006,
								"date_time" : "06 Sep 2016 01:20:33 PM EAT +0300",
								"type" : "Bulk SMS (Kenya)",
								"description" : "ext: 320146 | Amount: 1.00 Charge: 0.00 Balance: 14,672.00",
								"name" : "Debit"
							}
						], [{
								"index" : 5002,
								"date_time" : "05 Sep 2016 03:13:32 PM EAT +0300",
								"type" : "Bulk SMS (Kenya)",
								"description" : "ext: 320104 | Amount: 1.00 Charge: 0.00 Balance: 14,673.00",
								"name" : "Debit"
							}
						], [{
								"index" : 5001,
								"date_time" : "05 Sep 2016 03:11:36 PM EAT +0300",
								"type" : "Bulk SMS (Kenya)",
								"description" : "ext: 320103 | Amount: 1.00 Charge: 0.00 Balance: 14,674.00",
								"name" : "Debit"
							}
						], [{
								"index" : 4997,
								"date_time" : "04 Sep 2016 05:21:03 PM EAT +0300",
								"type" : "Bulk SMS (Kenya)",
								"description" : "ext: 320075 | Amount: 1.00 Charge: 0.00 Balance: 14,675.00",
								"name" : "Debit"
							}
						], [{
								"index" : 4993,
								"date_time" : "03 Sep 2016 10:18:29 AM EAT +0300",
								"type" : "Bulk SMS (Kenya)",
								"description" : "ext: 320054 | Amount: 1.00 Charge: 0.00 Balance: 14,676.00",
								"name" : "Debit"
							}
						], [{
								"index" : 4992,
								"date_time" : "03 Sep 2016 10:18:28 AM EAT +0300",
								"type" : "M-PESA",
								"description" : "ext: 320054 | Amount: 1,000.00 Charge: 43.00 Balance: 77,979.00",
								"name" : "Debit"
							}
						], [{
								"index" : 4991,
								"date_time" : "03 Sep 2016 09:41:26 AM EAT +0300",
								"type" : "Bulk SMS (Kenya)",
								"description" : "ext: 320053 | Amount: 2,896.00 Charge: 0.00 Balance: 14,677.00",
								"name" : "Debit"
							}
						], [{
								"index" : 4990,
								"date_time" : "03 Sep 2016 08:40:35 AM EAT +0300",
								"type" : "Bulk SMS (Kenya)",
								"description" : "ext: 320052 | Amount: 1.00 Charge: 0.00 Balance: 17,573.00",
								"name" : "Debit"
							}
						], [{
								"index" : 4989,
								"date_time" : "03 Sep 2016 08:38:52 AM EAT +0300",
								"type" : "Bulk SMS (Kenya)",
								"description" : "ext: 320049 | Amount: 1.00 Charge: 0.00 Balance: 17,574.00",
								"name" : "Debit"
							}
						], [{
								"index" : 4988,
								"date_time" : "03 Sep 2016 08:32:48 AM EAT +0300",
								"type" : "Bulk SMS (Kenya)",
								"description" : "ext: 320048 | Amount: 1.00 Charge: 0.00 Balance: 17,575.00",
								"name" : "Debit"
							}
						], [{
								"index" : 4981,
								"date_time" : "01 Sep 2016 09:49:58 AM EAT +0300",
								"type" : "Bulk SMS (Kenya)",
								"description" : "ext: 319923 | Amount: 2,896.00 Charge: 0.00 Balance: 17,576.00",
								"name" : "Debit"
							}
						], [{
								"index" : 4980,
								"date_time" : "01 Sep 2016 09:31:37 AM EAT +0300",
								"type" : "Bulk SMS (Kenya)",
								"description" : "ext:  | Amount: 20,000.00 Charge: 0.00 Balance: 20,472.00",
								"name" : "Credit"
							}
						], [{
								"index" : 4979,
								"date_time" : "31 Aug 2016 09:02:59 AM EAT +0300",
								"type" : "Bulk SMS (Kenya)",
								"description" : "ext: 319907 | Amount: 1.00 Charge: 0.00 Balance: 472.00",
								"name" : "Debit"
							}
						], [{
								"index" : 4978,
								"date_time" : "30 Aug 2016 10:24:20 AM EAT +0300",
								"type" : "Bulk SMS (Kenya)",
								"description" : "ext: 319846 | Amount: 1.00 Charge: 0.00 Balance: 473.00",
								"name" : "Debit"
							}
						], [{
								"index" : 4977,
								"date_time" : "30 Aug 2016 10:19:36 AM EAT +0300",
								"type" : "Bulk SMS (Kenya)",
								"description" : "ext: 319845 | Amount: 1.00 Charge: 0.00 Balance: 474.00",
								"name" : "Debit"
							}
						], [{
								"index" : 4975,
								"date_time" : "29 Aug 2016 05:16:51 PM EAT +0300",
								"type" : "Bulk SMS (Kenya)",
								"description" : "ext: 319830 | Amount: 1.00 Charge: 0.00 Balance: 475.00",
								"name" : "Debit"
							}
						], [{
								"index" : 4974,
								"date_time" : "29 Aug 2016 05:16:51 PM EAT +0300",
								"type" : "M-PESA",
								"description" : "ext: 319830 | Amount: 2,000.00 Charge: 43.00 Balance: 79,022.00",
								"name" : "Debit"
							}
						], [{
								"index" : 4973,
								"date_time" : "29 Aug 2016 05:14:12 PM EAT +0300",
								"type" : "Bulk SMS (Kenya)",
								"description" : "ext: 319829 | Amount: 1.00 Charge: 0.00 Balance: 476.00",
								"name" : "Debit"
							}
						], [{
								"index" : 4971,
								"date_time" : "27 Aug 2016 10:42:53 PM EAT +0300",
								"type" : "Bulk SMS (Kenya)",
								"description" : "ext: 319792 | Amount: 1.00 Charge: 0.00 Balance: 477.00",
								"name" : "Debit"
							}
						], [{
								"index" : 4970,
								"date_time" : "27 Aug 2016 10:41:11 PM EAT +0300",
								"type" : "Bulk SMS (Kenya)",
								"description" : "ext: 319791 | Amount: 1.00 Charge: 0.00 Balance: 478.00",
								"name" : "Debit"
							}
						]],
					"cols" : [],
					"min_id" : 3253,
					"groups" : ["Bulk SMS (Kenya)", "M-PESA", "MIPAY"],
					"max_id" : 5044
				}
			},
			"CHID" : "1"
		},
		"summary" : {
			"gateway_host" : "mchaama.com",
			"response_status" : "00",
			"data_name" : "summary",
			"session_id" : "Njk2YTdhNmY3MTc0NzA3OTZjMzA3ODYxNjU2NDYyNzY3NDZkNzgzMTc1Njg2NjY0Nzc3OTMwMzM3ODc2MzAzNjc1NzQ2MjYzNmU2NTYyNmI2YzYzMzQzOTZlMzM2ZjY5NjQ2NTZlNmE2MTY4MzA3NTcwN2E2YzMxNmYzMTZhN2E2ZDMwNmU2ODZhNjc2ODZjNzQ2NjZkMmIwYTc2NjQ3MzY4NzY2Njc1Nzk2NjY1NmU2Nzc0NzMzMTczNzI3NzYyNjY3MDc0Mzk2MjZhNjk3YTMwNzI3ODMxNjE2Njc4MzAzNTc1NzQ3NjZmNzUzMjMxNzU2ZjYzMzkzNjcxNzQzNDMwNmQzMjZmNmY2NjZlNzA2NzZkNjk3YTM1NjU2ZDZlMzI3OTZiNzkzNzcyNjk3MTcyMGE3MjY3NjI3NTY1NjQ2NjcwNjU3OTM0Mzg2NDc1Njg2Zjc1NjQ3ODY4NjY2ODcwNmE2ZDMxNjczMzc4NjMzNDM3NzM2NjZjNmY2MTY1MzU3MzZmNzMzNDcyNzk3OTYzNjgwYQ==",
			"chid" : "1",
			"lat" : "1.0",
			"lng" : "38.0",
			"ip_address" : "197.237.248.163",
			"response" : {
				"data_source" : {
					"rows" : [],
					"data" : [[{
								"count" : "1764 SENT",
								"date_time" : "May      , 2016",
								"type" : "40808_Bulk_AMKAGROUP",
								"name" : "40808_Bulk_AMKAGROUP_NEW"
							}, {
								"count" : "190 DELIVERED",
								"date_time" : "July     , 2016",
								"type" : "40808_Bulk_AMKAGROUP",
								"name" : "40808_Bulk_AMKAGROUP_PILOT"
							}, {
								"count" : "78 SENT",
								"date_time" : "August   , 2016",
								"type" : "AMKA BULK EMAIL",
								"name" : "AMKA BULK EMAIL"
							}, {
								"count" : "229 FAILED",
								"date_time" : "January  , 2016",
								"type" : "40808_Bulk_AMKAGROUP",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"count" : "10374 DELIVERED",
								"date_time" : "February , 2016",
								"type" : "40808_Bulk_AMKAGROUP",
								"name" : "40808_Bulk_AMKAGROUP_NEW"
							}, {
								"count" : "13 FAILED",
								"date_time" : "March    , 2016",
								"type" : "40808_Bulk_AMKAGROUP",
								"name" : "40808_Bulk_AMKAGROUP_NEW"
							}, {
								"count" : "1360 SENT",
								"date_time" : "August   , 2016",
								"type" : "40808_Bulk_AMKAGROUP",
								"name" : "40808_Bulk_AMKAGROUP_NEW"
							}, {
								"count" : "1549 SENT",
								"date_time" : "April    , 2016",
								"type" : "40808_Bulk_AMKAGROUP",
								"name" : "40808_Bulk_AMKAGROUP_NEW"
							}, {
								"count" : "98 FAILED",
								"date_time" : "September, 2016",
								"type" : "40808_Bulk_AMKAGROUP",
								"name" : "40808_Bulk_AMKAGROUP_NEW"
							}, {
								"count" : "138 SENT",
								"date_time" : "February , 2016",
								"type" : "40808_Bulk_AMKAGROUP",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"count" : "8 CREATED",
								"date_time" : "March    , 2016",
								"type" : "40808_Bulk_AMKAGROUP",
								"name" : "40808_Bulk_AMKAGROUP_NEW"
							}, {
								"count" : "36 FAILED",
								"date_time" : "June     , 2016",
								"type" : "40808_Bulk_AMKAGROUP",
								"name" : "40808_Bulk_AMKAGROUP_NEW"
							}, {
								"count" : "1960 DELIVERED",
								"date_time" : "February , 2016",
								"type" : "40808_Bulk_AMKAGROUP",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"count" : "13227 DELIVERED",
								"date_time" : "August   , 2016",
								"type" : "40808_Bulk_AMKAGROUP",
								"name" : "40808_Bulk_AMKAGROUP_NEW"
							}, {
								"count" : "19989 DELIVERED",
								"date_time" : "April    , 2016",
								"type" : "40808_Bulk_AMKAGROUP",
								"name" : "40808_Bulk_AMKAGROUP_NEW"
							}, {
								"count" : "1 CREATED",
								"date_time" : "July     , 2016",
								"type" : "40808_Bulk_AMKAGROUP",
								"name" : "40808_Bulk_AMKAGROUP_NEW"
							}, {
								"count" : "15 PROCESSING",
								"date_time" : "August   , 2016",
								"type" : "40808_Bulk_AMKAGROUP",
								"name" : "40808_Bulk_AMKAGROUP_NEW"
							}, {
								"count" : "2528 SENT",
								"date_time" : "February , 2016",
								"type" : "40808_Bulk_AMKAGROUP",
								"name" : "40808_Bulk_AMKAGROUP_NEW"
							}, {
								"count" : "8 SENT",
								"date_time" : "July     , 2016",
								"type" : "40808_Bulk_AMKAGROUP",
								"name" : "40808_Bulk_AMKAGROUP_PILOT"
							}, {
								"count" : "25442 DELIVERED",
								"date_time" : "May      , 2016",
								"type" : "40808_Bulk_AMKAGROUP",
								"name" : "40808_Bulk_AMKAGROUP_NEW"
							}, {
								"count" : "13359 DELIVERED",
								"date_time" : "September, 2016",
								"type" : "40808_Bulk_AMKAGROUP",
								"name" : "40808_Bulk_AMKAGROUP_NEW"
							}, {
								"count" : "1 CREATED",
								"date_time" : "May      , 2016",
								"type" : "40808_Bulk_AMKAGROUP",
								"name" : "40808_Bulk_AMKAGROUP_NEW"
							}, {
								"count" : "7 PROCESSING",
								"date_time" : "September, 2016",
								"type" : "AMKA BULK EMAIL",
								"name" : "AMKA BULK EMAIL"
							}, {
								"count" : "28051 DELIVERED",
								"date_time" : "July     , 2016",
								"type" : "40808_Bulk_AMKAGROUP",
								"name" : "40808_Bulk_AMKAGROUP_NEW"
							}, {
								"count" : "4134 SENT",
								"date_time" : "June     , 2016",
								"type" : "40808_Bulk_AMKAGROUP",
								"name" : "40808_Bulk_AMKAGROUP_NEW"
							}, {
								"count" : "12183 DELIVERED",
								"date_time" : "January  , 2016",
								"type" : "40808_Bulk_AMKAGROUP",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"count" : "1 CREATED",
								"date_time" : "April    , 2016",
								"type" : "40808_Bulk_AMKAGROUP",
								"name" : "40808_Bulk_AMKAGROUP_NEW"
							}, {
								"count" : "33 PROCESSING",
								"date_time" : "July     , 2016",
								"type" : "40808_Bulk_AMKAGROUP",
								"name" : "40808_Bulk_AMKAGROUP_NEW"
							}, {
								"count" : "25741 DELIVERED",
								"date_time" : "March    , 2016",
								"type" : "40808_Bulk_AMKAGROUP",
								"name" : "40808_Bulk_AMKAGROUP_NEW"
							}, {
								"count" : "41 FAILED",
								"date_time" : "February , 2016",
								"type" : "40808_Bulk_AMKAGROUP",
								"name" : "40808_Bulk_AMKAGROUP_NEW"
							}, {
								"count" : "15 PROCESSING",
								"date_time" : "September, 2016",
								"type" : "40808_Bulk_AMKAGROUP",
								"name" : "40808_Bulk_AMKAGROUP_NEW"
							}, {
								"count" : "1773 SENT",
								"date_time" : "March    , 2016",
								"type" : "40808_Bulk_AMKAGROUP",
								"name" : "40808_Bulk_AMKAGROUP_NEW"
							}, {
								"count" : "50 FAILED",
								"date_time" : "May      , 2016",
								"type" : "40808_Bulk_AMKAGROUP",
								"name" : "40808_Bulk_AMKAGROUP_NEW"
							}, {
								"count" : "8967 SENT",
								"date_time" : "January  , 2016",
								"type" : "40808_Bulk_AMKAGROUP",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"count" : "78 SENT",
								"date_time" : "September, 2016",
								"type" : "AMKA BULK EMAIL",
								"name" : "AMKA BULK EMAIL"
							}, {
								"count" : "13 FAILED",
								"date_time" : "February , 2016",
								"type" : "40808_Bulk_AMKAGROUP",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"count" : "2158 SENT",
								"date_time" : "July     , 2016",
								"type" : "40808_Bulk_AMKAGROUP",
								"name" : "40808_Bulk_AMKAGROUP_NEW"
							}, {
								"count" : "67 CREATED",
								"date_time" : "February , 2016",
								"type" : "40808_Bulk_AMKAGROUP",
								"name" : "40808_Bulk_AMKAGROUP_NEW"
							}, {
								"count" : "37734 DELIVERED",
								"date_time" : "June     , 2016",
								"type" : "40808_Bulk_AMKAGROUP",
								"name" : "40808_Bulk_AMKAGROUP_NEW"
							}, {
								"count" : "33 FAILED",
								"date_time" : "April    , 2016",
								"type" : "40808_Bulk_AMKAGROUP",
								"name" : "40808_Bulk_AMKAGROUP_NEW"
							}, {
								"count" : "1034 SENT",
								"date_time" : "September, 2016",
								"type" : "40808_Bulk_AMKAGROUP",
								"name" : "40808_Bulk_AMKAGROUP_NEW"
							}, {
								"count" : "120 FAILED",
								"date_time" : "August   , 2016",
								"type" : "40808_Bulk_AMKAGROUP",
								"name" : "40808_Bulk_AMKAGROUP_NEW"
							}
						], [{
								"count" : "3 UNPAID",
								"date_time" : "August   , 2016",
								"name" : "Credit Account",
								"description" : "KES 3740.00"
							}, {
								"count" : "11 SETTLED",
								"date_time" : "August   , 2016",
								"name" : "Credit Account",
								"description" : "KES 27995.00"
							}, {
								"count" : "11 UNPAID",
								"date_time" : "July     , 2016",
								"name" : "Investor Enrollment",
								"description" : "KES 22660.00"
							}, {
								"count" : "6 SETTLED",
								"date_time" : "July     , 2016",
								"name" : "Investor Enrollment",
								"description" : "KES 12360.00"
							}, {
								"count" : "21 PAID",
								"date_time" : "August   , 2016",
								"name" : "Membership Plan",
								"description" : "KES 2730.00"
							}, {
								"count" : "8 UNPAID",
								"date_time" : "September, 2016",
								"name" : "Investment",
								"description" : "KES 103865.00"
							}, {
								"count" : "1 PAID",
								"date_time" : "September, 2016",
								"name" : "Investor Enrollment",
								"description" : "KES 2060.00"
							}, {
								"count" : "10 PAID",
								"date_time" : "August   , 2016",
								"name" : "Credit Account",
								"description" : "KES 31405.00"
							}, {
								"count" : "54 UNPAID",
								"date_time" : "August   , 2016",
								"name" : "Investment",
								"description" : "KES 678510.00"
							}, {
								"count" : "1 SETTLED",
								"date_time" : "August   , 2016",
								"name" : "Investment",
								"description" : "KES 12565.00"
							}, {
								"count" : "3 PAID",
								"date_time" : "August   , 2016",
								"name" : "Investor Enrollment",
								"description" : "KES 6180.00"
							}, {
								"count" : "135 UNPAID",
								"date_time" : "July     , 2016",
								"name" : "Membership Plan",
								"description" : "KES 17550.00"
							}, {
								"count" : "46 SETTLED",
								"date_time" : "July     , 2016",
								"name" : "Membership Plan",
								"description" : "KES 5980.00"
							}, {
								"count" : "2 SETTLED",
								"date_time" : "June     , 2016",
								"name" : "Membership Plan",
								"description" : "KES 260.00"
							}, {
								"count" : "1 UNPAID",
								"date_time" : "September, 2016",
								"name" : "Investor Enrollment",
								"description" : "KES 2060.00"
							}, {
								"count" : "10 UNPAID",
								"date_time" : "July     , 2016",
								"name" : "Investment",
								"description" : "KES 125650.00"
							}, {
								"count" : "246 UNPAID",
								"date_time" : "August   , 2016",
								"name" : "Membership Plan",
								"description" : "KES 31980.00"
							}, {
								"count" : "18 SETTLED",
								"date_time" : "August   , 2016",
								"name" : "Membership Plan",
								"description" : "KES 2340.00"
							}, {
								"count" : "9 UNPAID",
								"date_time" : "September, 2016",
								"name" : "Credit Account",
								"description" : "KES 11440.00"
							}, {
								"count" : "27 UNPAID",
								"date_time" : "August   , 2016",
								"name" : "Investor Enrollment",
								"description" : "KES 55620.00"
							}, {
								"count" : "6 SETTLED",
								"date_time" : "August   , 2016",
								"name" : "Investor Enrollment",
								"description" : "KES 12360.00"
							}, {
								"count" : "82 UNPAID",
								"date_time" : "September, 2016",
								"name" : "Membership Plan",
								"description" : "KES 10660.00"
							}, {
								"count" : "32 SETTLED",
								"date_time" : "July     , 2016",
								"name" : "Credit Account",
								"description" : "KES 35640.00"
							}, {
								"count" : "13 UNPAID",
								"date_time" : "July     , 2016",
								"name" : "Credit Account",
								"description" : "KES 12760.00"
							}, {
								"count" : "3 PAID",
								"date_time" : "September, 2016",
								"name" : "Credit Account",
								"description" : "KES 4620.00"
							}, {
								"count" : "9 PAID",
								"date_time" : "September, 2016",
								"name" : "Membership Plan",
								"description" : "KES 1170.00"
							}
						]],
					"cols" : [],
					"min_id" : 0,
					"groups" : ["Notification Product", "Purchase Order"],
					"max_id" : 0
				}
			},
			"CHID" : "1"
		},
		"transaction_list" : {
			"gateway_host" : "mchaama.com",
			"response_status" : "00",
			"data_name" : "transaction_list",
			"session_id" : "Njk2YTdhNmY3MTc0NzA3OTZjMzA3ODYxNjU2NDYyNzY3NDZkNzgzMTc1Njg2NjY0Nzc3OTMwMzM3ODc2MzAzNjc1NzQ2MjYzNmU2NTYyNmI2YzYzMzQzOTZlMzM2ZjY5NjQ2NTZlNmE2MTY4MzA3NTcwN2E2YzMxNmYzMTZhN2E2ZDMwNmU2ODZhNjc2ODZjNzQ2NjZkMmIwYTc2NjQ3MzY4NzY2Njc1Nzk2NjY1NmU2Nzc0NzMzMTczNzI3NzYyNjY3MDc0Mzk2MjZhNjk3YTMwNzI3ODMxNjE2Njc4MzAzNTc1NzQ3NjZmNzUzMjMxNzU2ZjYzMzkzNjcxNzQzNDMwNmQzMjZmNmY2NjZlNzA2NzZkNjk3YTM1NjU2ZDZlMzI3OTZiNzkzNzcyNjk3MTcyMGE3MjY3NjI3NTY1NjQ2NjcwNjU3OTM0Mzg2NDc1Njg2Zjc1NjQ3ODY4NjY2ODcwNmE2ZDMxNjczMzc4NjMzNDM3NzM2NjZjNmY2MTY1MzU3MzZmNzMzNDcyNzk3OTYzNjgwYQ==",
			"chid" : "1",
			"lat" : "1.0",
			"lng" : "38.0",
			"ip_address" : "197.237.248.163",
			"response" : {
				"data_source" : {
					"rows" : [[{
								"index" : 320356,
								"name" : "JACINTA MUTISO",
								"date_time" : "15 Sep 2016 03:25:42 PM EAT +0300",
								"request" : {
									"full_names" : "Jacinta A. W. Mutiso",
									"accesspoint" : "808",
									"msisdn" : "+254713373524",
									"access_point" : "*808*77#",
									"passport_id_number" : "2974566",
									"item" : "AMKA M-Chaama Membership",
									"payment_method" : "M-PESA"
								},
								"type" : "MEMBERSHIP REGISTRATION REQUEST",
								"response" : {
									"get_profile" : "Session Profile Captured",
									"member_registration" : "Successful",
									"remit" : "Remit Successful",
									"sale_order" : "Sale Order"
								},
								"description" : "PROCESSED|Response Status: Approved or completed successfully|Overall Status: Approved or completed successfully"
							}
						], [{
								"index" : 320355,
								"name" : "charles ngamau",
								"date_time" : "15 Sep 2016 03:20:26 PM EAT +0300",
								"request" : {
									"full_names" : "charles ngamau",
									"accesspoint" : "808",
									"msisdn" : "+254718482176",
									"access_point" : "*808*77#",
									"passport_id_number" : "29116298",
									"item" : "AMKA M-Chaama Membership",
									"payment_method" : "M-PESA"
								},
								"type" : "MEMBERSHIP REGISTRATION REQUEST",
								"response" : {
									"get_profile" : "Session Profile Captured",
									"member_registration" : "Successful",
									"remit" : "Remit Successful",
									"sale_order" : "Sale Order"
								},
								"description" : "PROCESSED|Response Status: Approved or completed successfully|Overall Status: Approved or completed successfully"
							}
						], [{
								"index" : 320354,
								"name" : "charles ngamau",
								"date_time" : "15 Sep 2016 12:22:38 PM EAT +0300",
								"request" : {
									"full_names" : "charles ngamau",
									"accesspoint" : "808",
									"msisdn" : "+254718482176",
									"access_point" : "*808*77#",
									"passport_id_number" : "29116298",
									"item" : "AMKA M-Chaama Membership",
									"payment_method" : "M-PESA"
								},
								"type" : "MEMBERSHIP REGISTRATION REQUEST",
								"response" : {
									"get_profile" : "Session Profile Captured",
									"member_registration" : "Successful",
									"remit" : "Remit Successful",
									"sale_order" : "Sale Order"
								},
								"description" : "PROCESSED|Response Status: Approved or completed successfully|Overall Status: Approved or completed successfully"
							}
						], [{
								"index" : 320353,
								"name" : "charles ngamau",
								"date_time" : "15 Sep 2016 12:18:44 PM EAT +0300",
								"request" : {
									"full_names" : "charles ngamau",
									"accesspoint" : "808",
									"msisdn" : "+254718482176",
									"access_point" : "*808*77#",
									"passport_id_number" : "29116298",
									"item" : "AMKA M-Chaama Membership",
									"payment_method" : "M-PESA"
								},
								"type" : "MEMBERSHIP REGISTRATION REQUEST",
								"response" : {
									"get_profile" : "Session Profile Captured",
									"member_registration" : "Successful",
									"remit" : "Remit Successful",
									"sale_order" : "Sale Order"
								},
								"description" : "PROCESSED|Response Status: Approved or completed successfully|Overall Status: Approved or completed successfully"
							}
						], [{
								"index" : 320352,
								"name" : "System User",
								"date_time" : "15 Sep 2016 12:04:05 PM EAT +0300",
								"request" : {
									"full_names" : "charles ngamau",
									"accesspoint" : "808",
									"msisdn" : "+254718482176",
									"access_point" : "*808*77#",
									"passport_id_number" : "29116298",
									"item" : "AMKA M-Chaama Membership",
									"payment_method" : "M-PESA"
								},
								"type" : "MEMBERSHIP REGISTRATION REQUEST",
								"response" : {
									"get_profile" : "Session Profile Captured",
									"member_registration" : "Successful",
									"remit" : "Remit Successful",
									"sale_order" : "Sale Order"
								},
								"description" : "PROCESSED|Response Status: Approved or completed successfully|Overall Status: Approved or completed successfully"
							}
						], [{
								"index" : 320350,
								"name" : "Amka Group",
								"date_time" : "14 Sep 2016 02:36:20 PM EAT +0300",
								"request" : {
									"scheduled_time" : "2:21 pm",
									"message" : "Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
									"scheduled _date" : "14/09/2016",
									"notification_product" : "74"
								},
								"type" : "NOTIFICATION PRODUCT SEND",
								"response" : {
									"check_transaction_auth" : "Operator Transaction found",
									"log_outbound_message" : "Outbound Message Processed",
									"notification_debit_float" : " | Float Debited with: 2896.00 balance: 5965.00"
								},
								"description" : "PROCESSED|Response Status: Approved or completed successfully|Overall Status: Approved or completed successfully"
							}
						], [{
								"index" : 320349,
								"name" : "DANIEL KIMITI",
								"date_time" : "14 Sep 2016 01:56:46 PM EAT +0300",
								"request" : {
									"msisdn" : "+254722301125",
									"access_point" : "*808*77#",
									"account_type_id" : "3",
									"payment_method" : "M-PESA",
									"accesspoint" : "808"
								},
								"type" : "LOAN REQUEST",
								"response" : {
									"loan_status" : "Principal-Loan-Due Date\nKES 1,000.00-KES 1,100.00-08/Oct/2016\nKES 1,000.00-KES 1,100.00-06/Oct/2016\n",
									"get_account" : "Account Captured",
									"loan_limit" : "Credit Limit KES 7,200.00\nAvailable Limit KES 600.00",
									"get_profile" : "Session Profile Captured",
									"loan_item_details" : "Exceeds withdrawal limit. Credit Limit KES 7,200.00\nAvailable Limit KES 600.00"
								},
								"description" : "PROCESSED|Response Status: Exceeds withdrawal limit|Overall Status: Exceeds withdrawal limit"
							}
						], [{
								"index" : 320348,
								"name" : "DANIEL KIMITI",
								"date_time" : "14 Sep 2016 01:39:02 PM EAT +0300",
								"request" : {
									"msisdn" : "+254722301125",
									"access_point" : "*808*77#",
									"reference" : "1-AMKA-RUP53",
									"payment_method" : "MIPAY",
									"accesspoint" : "808"
								},
								"type" : "ORDER CHECKOUT",
								"response" : {
									"get_bill" : "Bill Balance: 1100.00",
									"pay_bill" : "Bill Payment Accepted. Balance: 695.00",
									"get_account" : "Account Captured",
									"account_balance" : "KES 405.00",
									"debit_account" : "Account Debited"
								},
								"description" : "PROCESSED|Response Status: Approved or completed successfully|Overall Status: Approved or completed successfully"
							}
						], [{
								"index" : 320347,
								"name" : "JACINTA MUTISO",
								"date_time" : "14 Sep 2016 09:17:12 AM EAT +0300",
								"request" : {
									"full_names" : "Jacinta Wanzila mutisa",
									"accesspoint" : "808",
									"msisdn" : "+254713373524",
									"access_point" : "*808*77#",
									"passport_id_number" : "2974566",
									"item" : "AMKA M-Chaama Membership",
									"payment_method" : "M-PESA"
								},
								"type" : "MEMBERSHIP REGISTRATION REQUEST",
								"response" : {
									"get_profile" : "Session Profile Captured",
									"member_registration" : "Successful",
									"remit" : "Remit Successful",
									"sale_order" : "Sale Order"
								},
								"description" : "PROCESSED|Response Status: Approved or completed successfully|Overall Status: Approved or completed successfully"
							}
						], [{
								"index" : 320341,
								"name" : "System User",
								"date_time" : "14 Sep 2016 01:43:27 AM EAT +0300",
								"request" : {
									"passport_id_number" : "29595134",
									"msisdn" : "+254701701916",
									"full_names" : "pamela mwende",
									"email" : "pamelamwende4@gmail.com",
									"item" : "AMKA M-Chaama Membership"
								},
								"type" : "MEMBERSHIP REGISTRATION REQUEST",
								"response" : {
									"get_profile" : "Session Profile Captured",
									"get_notification" : "Notification Captured",
									"debit_float" : "No float amount to debit",
									"member_registration" : "Successful",
									"sale_order" : "Sale Order",
									"send_notification" : "Notification Sent. Please check EMAIL"
								},
								"description" : "PROCESSED|Response Status: Approved or completed successfully|Overall Status: Approved or completed successfully"
							}
						], [{
								"index" : 320340,
								"name" : "System User",
								"date_time" : "14 Sep 2016 01:43:25 AM EAT +0300",
								"request" : {
									"passport_id_number" : "29595134",
									"msisdn" : "+254701701916",
									"full_names" : "pamela mwende",
									"email" : "pamelamwende4@gmail.com",
									"item" : "AMKA M-Chaama Membership"
								},
								"type" : "MEMBERSHIP REGISTRATION REQUEST",
								"response" : {
									"get_profile" : "Session Profile Captured",
									"get_notification" : "Notification Captured",
									"debit_float" : "No float amount to debit",
									"member_registration" : "Successful",
									"sale_order" : "Sale Order",
									"send_notification" : "Notification Sent. Please check EMAIL"
								},
								"description" : "PROCESSED|Response Status: Approved or completed successfully|Overall Status: Approved or completed successfully"
							}
						], [{
								"index" : 320339,
								"name" : "System User",
								"date_time" : "14 Sep 2016 12:21:11 AM EAT +0300",
								"request" : {
									"passport_id_number" : "31610811",
									"msisdn" : "+254722331493",
									"full_names" : "Emmanuel Kangere waruhiu",
									"email" : "kangere20@gmail.com",
									"item" : "AMKA M-Chaama Membership"
								},
								"type" : "MEMBERSHIP REGISTRATION REQUEST",
								"response" : {
									"get_profile" : "Session Profile Captured",
									"get_notification" : "Notification Captured",
									"debit_float" : "No float amount to debit",
									"member_registration" : "Successful",
									"sale_order" : "Sale Order",
									"send_notification" : "Notification Sent. Please check EMAIL"
								},
								"description" : "PROCESSED|Response Status: Approved or completed successfully|Overall Status: Approved or completed successfully"
							}
						], [{
								"index" : 320338,
								"name" : "GRACE KIIRI",
								"date_time" : "12 Sep 2016 07:36:14 PM EAT +0300",
								"request" : {
									"msisdn" : "+254721640597",
									"access_point" : "*808*77#",
									"account_type_id" : "3",
									"payment_method" : "M-PESA",
									"accesspoint" : "808"
								},
								"type" : "LOAN REQUEST",
								"response" : {
									"loan_limit" : "Credit Limit KES 2,000.00\nAvailable Limit KES 2,000.00",
									"get_profile" : "Session Profile Captured",
									"get_notification" : "Notification Captured",
									"remit" : "Remit Successful",
									"get_account" : "Account Captured",
									"debit_float" : "Float Debited with: 1.00 balance: 8861.00",
									"loan_item_details" : "Captured",
									"debit_account" : "Account Debited",
									"loan_status" : "No Loan Record Available",
									"sale_order" : "Sale Order",
									"send_notification" : "Notification Sent. Please check SMS"
								},
								"description" : "PROCESSED|Response Status: Approved or completed successfully|Overall Status: Approved or completed successfully"
							}
						], [{
								"index" : 320337,
								"name" : "GRACE KIIRI",
								"date_time" : "12 Sep 2016 07:33:30 PM EAT +0300",
								"request" : {
									"msisdn" : "+254721640597",
									"access_point" : "*808*77#",
									"account_type_id" : "3",
									"payment_method" : "M-PESA",
									"accesspoint" : "808"
								},
								"type" : "LOAN REQUEST",
								"response" : {
									"loan_status" : "No Loan Record Available",
									"get_account" : "Account Captured",
									"loan_limit" : "Credit Limit KES 2,000.00\nAvailable Limit KES 2,000.00",
									"get_profile" : "Session Profile Captured",
									"loan_item_details" : "Exceeds withdrawal limit. Credit Limit KES 2,000.00\nAvailable Limit KES 2,000.00"
								},
								"description" : "PROCESSED|Response Status: Exceeds withdrawal limit|Overall Status: Exceeds withdrawal limit"
							}
						], [{
								"index" : 320336,
								"name" : "GRACE KIIRI",
								"date_time" : "12 Sep 2016 07:30:28 PM EAT +0300",
								"request" : {
									"username" : "+254721640597",
									"last_name" : "KIIRI",
									"reference" : "1-AMKA-MZF00",
									"payment_method" : "M-PESA",
									"access_point" : "*808*77#",
									"credit_total" : "0",
									"first_name" : "GRACE",
									"institution_till_id" : "1",
									"accesspoint" : "808",
									"account_manager_id" : "4045",
									"product_item_id" : "99770",
									"due_date" : "28/Sep/2016",
									"overdue_credit" : "False",
									"balance_bf" : "-2200.00",
									"msisdn" : "+254721640597",
									"purchase_order_id" : "3495",
									"credit_limit" : "2000.00",
									"float_amount" : "2000",
									"available_limit" : "2000.00",
									"account_type_id" : "3",
									"available_credit" : "0",
									"quantity" : "2200.00"
								},
								"type" : "LOAN REPAYMENT",
								"response" : {
									"get_profile" : "Session Profile Captured",
									"get_notification" : "Notification Captured",
									"debit_float" : "Float Debited with: 1.00 balance: 8862.00",
									"loan_repayment" : "Loan Repaid",
									"get_account" : "Account Captured",
									"send_notification" : "Notification Sent. Please check SMS",
									"amka_credit_limit_review" : "Loan Limit Reviewed",
									"credit_account" : "Account Credited"
								},
								"description" : "PROCESSED|Response Status: Approved or completed successfully|Overall Status: Approved or completed successfully"
							}
						], [{
								"index" : 320334,
								"name" : "GRACE KIIRI",
								"date_time" : "12 Sep 2016 07:29:36 PM EAT +0300",
								"request" : {
									"msisdn" : "+254721640597",
									"access_point" : "*808*77#",
									"reference" : "1-AMKA-MZF00",
									"payment_method" : "M-PESA",
									"accesspoint" : "808"
								},
								"type" : "ORDER CHECKOUT",
								"response" : {
									"get_bill" : "Bill Balance: 2200.00",
									"remit" : "Remit Successful"
								},
								"description" : "PROCESSED|Response Status: Approved or completed successfully|Overall Status: Approved or completed successfully"
							}
						], [{
								"index" : 320333,
								"name" : "System User",
								"date_time" : "12 Sep 2016 01:30:34 PM EAT +0300",
								"request" : {
									"full_names" : "Mary njeri macharia",
									"accesspoint" : "*808*77#",
									"msisdn" : "+254732699033",
									"access_point" : "*808*77#",
									"passport_id_number" : "11588626",
									"item" : "AMKA M-Chaama Membership",
									"payment_method" : "M-PESA"
								},
								"type" : "MEMBERSHIP REGISTRATION REQUEST",
								"response" : {
									"get_profile" : "Session Profile Captured",
									"member_registration" : "Successful",
									"remit" : "Remit Successful",
									"sale_order" : "Sale Order"
								},
								"description" : "PROCESSED|Response Status: Approved or completed successfully|Overall Status: Approved or completed successfully"
							}
						], [{
								"index" : 320332,
								"name" : "System User",
								"date_time" : "12 Sep 2016 01:21:58 PM EAT +0300",
								"request" : {
									"passport_id_number" : "24379191",
									"msisdn" : "+254711850004",
									"full_names" : "Martin mbugua ",
									"email" : "martinmbg@yahoo.com",
									"item" : "AMKA M-Chaama Membership"
								},
								"type" : "MEMBERSHIP REGISTRATION REQUEST",
								"response" : {
									"get_profile" : "Session Profile Captured",
									"member_registration" : "Duplicate record. Record Exists"
								},
								"description" : "PROCESSED|Response Status: Duplicate record|Overall Status: Duplicate record"
							}
						], [{
								"index" : 320331,
								"name" : "System User",
								"date_time" : "12 Sep 2016 01:21:51 PM EAT +0300",
								"request" : {
									"passport_id_number" : "24379191",
									"msisdn" : "+254711850004",
									"full_names" : "Martin mbugua ",
									"email" : "martinmbg@yahoo.com",
									"item" : "AMKA M-Chaama Membership"
								},
								"type" : "MEMBERSHIP REGISTRATION REQUEST",
								"response" : {
									"get_profile" : "Session Profile Captured",
									"member_registration" : "Duplicate record. Record Exists"
								},
								"description" : "PROCESSED|Response Status: Duplicate record|Overall Status: Duplicate record"
							}
						], [{
								"index" : 320330,
								"name" : "System User",
								"date_time" : "12 Sep 2016 01:21:42 PM EAT +0300",
								"request" : {
									"passport_id_number" : "24379191",
									"msisdn" : "+254711850004",
									"full_names" : "Martin mbugua ",
									"email" : "martinmbg@yahoo.com",
									"item" : "AMKA M-Chaama Membership"
								},
								"type" : "MEMBERSHIP REGISTRATION REQUEST",
								"response" : {
									"get_profile" : "Session Profile Captured",
									"member_registration" : "Duplicate record. Record Exists"
								},
								"description" : "PROCESSED|Response Status: Duplicate record|Overall Status: Duplicate record"
							}
						], [{
								"index" : 320329,
								"name" : "System User",
								"date_time" : "12 Sep 2016 01:21:34 PM EAT +0300",
								"request" : {
									"passport_id_number" : "24379191",
									"msisdn" : "+254711850004",
									"full_names" : "Martin mbugua ",
									"email" : "martinmbg@yahoo.com",
									"item" : "AMKA M-Chaama Membership"
								},
								"type" : "MEMBERSHIP REGISTRATION REQUEST",
								"response" : {
									"get_profile" : "Session Profile Captured",
									"member_registration" : "Duplicate record. Record Exists"
								},
								"description" : "PROCESSED|Response Status: Duplicate record|Overall Status: Duplicate record"
							}
						], [{
								"index" : 320327,
								"name" : "Martin wachera",
								"date_time" : "12 Sep 2016 12:58:51 PM EAT +0300",
								"request" : {
									"product_item_id" : "99767",
									"username" : "martinmbg",
									"first_name" : "Martin",
									"last_name" : "wachera",
									"full_names" : "Martin mbugua wachera",
									"reference" : "1-AMKA-QFA24",
									"msisdn" : "+254711850004",
									"purchase_order_id" : "3659",
									"passport_id_number" : "24379191",
									"item" : "AMKA M-Chaama Membership",
									"institution_till_id" : "1",
									"middle_name" : "mbugua",
									"email" : "martinmbg@yahoo.com",
									"quantity" : "1.00"
								},
								"type" : "MEMBERSHIP REGISTRATION",
								"response" : {
									"one_time_pin" : "One Time Pin Set",
									"get_profile" : "Session Profile Captured",
									"debit_float" : "No float amount to debit",
									"send_notification" : "Notification Sent. Please check EMAIL",
									"get_email_notification" : "Notification Captured",
									"create_enrollment" : "Enrollment Captured"
								},
								"description" : "PROCESSED|Response Status: Approved or completed successfully|Overall Status: Approved or completed successfully"
							}
						], [{
								"index" : 320323,
								"name" : "System User",
								"date_time" : "12 Sep 2016 12:55:02 PM EAT +0300",
								"request" : {
									"passport_id_number" : "24379191",
									"msisdn" : "+254711850004",
									"full_names" : "Martin mbugua wachera",
									"email" : "martinmbg@yahoo.com",
									"item" : "AMKA M-Chaama Membership"
								},
								"type" : "MEMBERSHIP REGISTRATION REQUEST",
								"response" : {
									"get_profile" : "Session Profile Captured",
									"get_notification" : "Notification Captured",
									"debit_float" : "No float amount to debit",
									"member_registration" : "Successful",
									"sale_order" : "Sale Order",
									"send_notification" : "Notification Sent. Please check EMAIL"
								},
								"description" : "PROCESSED|Response Status: Approved or completed successfully|Overall Status: Approved or completed successfully"
							}
						], [{
								"index" : 320322,
								"name" : "System User",
								"date_time" : "12 Sep 2016 12:55:00 PM EAT +0300",
								"request" : {
									"passport_id_number" : "24379191",
									"msisdn" : "+254711850004",
									"full_names" : "Martin mbugua wachera",
									"email" : "martinmbg@yahoo.com",
									"item" : "AMKA M-Chaama Membership"
								},
								"type" : "MEMBERSHIP REGISTRATION REQUEST",
								"response" : {
									"get_profile" : "Session Profile Captured",
									"get_notification" : "Notification Captured",
									"debit_float" : "No float amount to debit",
									"member_registration" : "Successful",
									"sale_order" : "Sale Order",
									"send_notification" : "Notification Sent. Please check EMAIL"
								},
								"description" : "PROCESSED|Response Status: Approved or completed successfully|Overall Status: Approved or completed successfully"
							}
						], [{
								"index" : 320321,
								"name" : "GRACE KIIRI",
								"date_time" : "12 Sep 2016 12:29:58 PM EAT +0300",
								"request" : {
									"msisdn" : "+254721640597",
									"access_point" : "*808*77#",
									"reference" : "1-AMKA-MZF00",
									"payment_method" : "M-PESA",
									"accesspoint" : "808"
								},
								"type" : "ORDER CHECKOUT",
								"response" : {
									"get_bill" : "Bill Balance: 2200.00",
									"remit" : "Remit Successful"
								},
								"description" : "PROCESSED|Response Status: Approved or completed successfully|Overall Status: Approved or completed successfully"
							}
						], [{
								"index" : 320320,
								"name" : "GRACE KIIRI",
								"date_time" : "12 Sep 2016 12:27:02 PM EAT +0300",
								"request" : {
									"msisdn" : "+254721640597",
									"access_point" : "*808*77#",
									"reference" : "1-AMKA-MZF00",
									"payment_method" : "M-PESA",
									"accesspoint" : "808"
								},
								"type" : "ORDER CHECKOUT",
								"response" : {
									"get_bill" : "Bill Balance: 2200.00",
									"remit" : "Remit Successful"
								},
								"description" : "PROCESSED|Response Status: Approved or completed successfully|Overall Status: Approved or completed successfully"
							}
						], [{
								"index" : 320319,
								"name" : "Amka Group",
								"date_time" : "12 Sep 2016 11:59:20 AM EAT +0300",
								"request" : {
									"scheduled_time" : "11:51 am",
									"message" : "Now Invest,Borrow or Lend 24Hrs/7dys on M-Chaama & grow via High Dividends! Lenders Pie now@13,500 upto 10.10.16.Sacco Mradi-Loans Available",
									"scheduled _date" : "12/09/2016",
									"notification_product" : "74"
								},
								"type" : "NOTIFICATION PRODUCT SEND",
								"response" : {
									"check_transaction_auth" : "Operator Transaction found",
									"log_outbound_message" : "Outbound Message Processed",
									"notification_debit_float" : " | Float Debited with: 2896.00 balance: 8863.00"
								},
								"description" : "PROCESSED|Response Status: Approved or completed successfully|Overall Status: Approved or completed successfully"
							}
						], [{
								"index" : 320317,
								"name" : "PATRICK ADEGO",
								"date_time" : "12 Sep 2016 07:29:13 AM EAT +0300",
								"request" : {
									"username" : "+254720337751",
									"product_item_id" : "99767",
									"first_name" : "PATRICK",
									"last_name" : "ADEGO",
									"full_names" : "Patrick Odari Adego",
									"reference" : "1-AMKA-UYC61",
									"accesspoint" : "808",
									"payment_method" : "M-PESA",
									"access_point" : "*808*77#",
									"purchase_order_id" : "3657",
									"passport_id_number" : "12471079",
									"msisdn" : "+254720337751",
									"institution_till_id" : "1",
									"item" : "AMKA M-Chaama Membership",
									"middle_name" : "Odari",
									"quantity" : "1.00"
								},
								"type" : "MEMBERSHIP REGISTRATION",
								"response" : {
									"one_time_pin" : "One Time Pin Set",
									"get_profile" : "Session Profile Captured",
									"get_notification" : "Notification Captured",
									"debit_float" : "Float Debited with: 1.00 balance: 11759.00",
									"send_notification" : "Notification Sent. Please check SMS",
									"create_enrollment" : "Enrollment Captured"
								},
								"description" : "PROCESSED|Response Status: Approved or completed successfully|Overall Status: Approved or completed successfully"
							}
						], [{
								"index" : 320315,
								"name" : "PATRICK ADEGO",
								"date_time" : "12 Sep 2016 07:28:17 AM EAT +0300",
								"request" : {
									"full_names" : "Patrick Odari Adego",
									"accesspoint" : "808",
									"msisdn" : "+254720337751",
									"access_point" : "*808*77#",
									"passport_id_number" : "12471079",
									"item" : "AMKA M-Chaama Membership",
									"payment_method" : "M-PESA"
								},
								"type" : "MEMBERSHIP REGISTRATION REQUEST",
								"response" : {
									"get_profile" : "Session Profile Captured",
									"member_registration" : "Successful",
									"remit" : "Remit Successful",
									"sale_order" : "Sale Order"
								},
								"description" : "PROCESSED|Response Status: Approved or completed successfully|Overall Status: Approved or completed successfully"
							}
						], [{
								"index" : 320314,
								"name" : "PATRICK ADEGO",
								"date_time" : "12 Sep 2016 07:18:23 AM EAT +0300",
								"request" : {
									"full_names" : "Patrick Odari Adego",
									"accesspoint" : "808",
									"msisdn" : "+254720337751",
									"access_point" : "*808*77#",
									"passport_id_number" : "12471079",
									"item" : "AMKA M-Chaama Membership",
									"payment_method" : "M-PESA"
								},
								"type" : "MEMBERSHIP REGISTRATION REQUEST",
								"response" : {
									"get_profile" : "Session Profile Captured",
									"member_registration" : "Successful",
									"remit" : "Remit Successful",
									"sale_order" : "Sale Order"
								},
								"description" : "PROCESSED|Response Status: Approved or completed successfully|Overall Status: Approved or completed successfully"
							}
						], [{
								"index" : 320313,
								"name" : "PATRICK ADEGO",
								"date_time" : "12 Sep 2016 07:05:46 AM EAT +0300",
								"request" : {
									"full_names" : "Patrick Odari Adego",
									"accesspoint" : "808",
									"msisdn" : "+254720337751",
									"access_point" : "*808*77#",
									"passport_id_number" : "12471879",
									"item" : "AMKA M-Chaama Membership",
									"payment_method" : "M-PESA"
								},
								"type" : "MEMBERSHIP REGISTRATION REQUEST",
								"response" : {
									"get_profile" : "Session Profile Captured",
									"member_registration" : "Successful",
									"remit" : "Remit Successful",
									"sale_order" : "Sale Order"
								},
								"description" : "PROCESSED|Response Status: Approved or completed successfully|Overall Status: Approved or completed successfully"
							}
						], [{
								"index" : 320312,
								"name" : "PATRICK ADEGO",
								"date_time" : "12 Sep 2016 07:02:12 AM EAT +0300",
								"request" : {
									"full_names" : "Patrick Odari Adego",
									"accesspoint" : "808",
									"msisdn" : "+254720337751",
									"access_point" : "*808*77#",
									"passport_id_number" : "12471079",
									"item" : "AMKA M-Chaama Membership",
									"payment_method" : "M-PESA"
								},
								"type" : "MEMBERSHIP REGISTRATION REQUEST",
								"response" : {
									"get_profile" : "Session Profile Captured",
									"member_registration" : "Successful",
									"remit" : "Remit Successful",
									"sale_order" : "Sale Order"
								},
								"description" : "PROCESSED|Response Status: Approved or completed successfully|Overall Status: Approved or completed successfully"
							}
						], [{
								"index" : 320311,
								"name" : "HENRY KABURU",
								"date_time" : "11 Sep 2016 08:38:25 PM EAT +0300",
								"request" : {
									"product_item_id" : "99770",
									"due_date" : "09/Sep/2016",
									"overdue_credit" : "False",
									"credit_limit" : "1000.00",
									"balance_bf" : "-1100.00",
									"reference" : "1-AMKA-TLA24",
									"accesspoint" : "808",
									"payment_method" : "M-PESA",
									"access_point" : "*808*77#",
									"credit_total" : "0",
									"purchase_order_id" : "3092",
									"msisdn" : "+254724236531",
									"institution_till_id" : "1",
									"float_amount" : "1000",
									"available_credit" : "0",
									"available_limit" : "1000.00",
									"account_type_id" : "3",
									"account_manager_id" : "3923",
									"quantity" : "1100.00"
								},
								"type" : "LOAN REPAYMENT",
								"response" : {
									"get_profile" : "Session Profile Captured",
									"get_notification" : "Notification Captured",
									"debit_float" : "Float Debited with: 1.00 balance: 11760.00",
									"loan_repayment" : "Loan Repaid",
									"get_account" : "Account Captured",
									"send_notification" : "Notification Sent. Please check SMS",
									"amka_credit_limit_review" : "Loan Limit Reviewed",
									"credit_account" : "Account Credited"
								},
								"description" : "PROCESSED|Response Status: Approved or completed successfully|Overall Status: Approved or completed successfully"
							}
						], [{
								"index" : 320309,
								"name" : "HENRY KABURU",
								"date_time" : "11 Sep 2016 08:36:51 PM EAT +0300",
								"request" : {
									"msisdn" : "+254724236531",
									"access_point" : "*808*77#",
									"reference" : "1-AMKA-TLA24",
									"payment_method" : "M-PESA",
									"accesspoint" : "808"
								},
								"type" : "ORDER CHECKOUT",
								"response" : {
									"get_bill" : "Bill Balance: 1100.00",
									"remit" : "Remit Successful"
								},
								"description" : "PROCESSED|Response Status: Approved or completed successfully|Overall Status: Approved or completed successfully"
							}
						], [{
								"index" : 320308,
								"name" : "HENRY KABURU",
								"date_time" : "11 Sep 2016 08:34:04 PM EAT +0300",
								"request" : {
									"msisdn" : "+254724236531",
									"access_point" : "*808*77#",
									"reference" : "1-AMKA-TLA24",
									"payment_method" : "M-PESA",
									"accesspoint" : "808"
								},
								"type" : "ORDER CHECKOUT",
								"response" : {
									"get_bill" : "Bill Balance: 1100.00",
									"remit" : "Remit Successful"
								},
								"description" : "PROCESSED|Response Status: Approved or completed successfully|Overall Status: Approved or completed successfully"
							}
						], [{
								"index" : 320305,
								"name" : "wanzaa ndusya",
								"date_time" : "11 Sep 2016 12:15:20 PM EAT +0300",
								"request" : {
									"product_item_id" : "99767",
									"username" : "lucywanja.com",
									"first_name" : "wanzaa",
									"last_name" : "ndusya",
									"full_names" : "wanzaa mwende ndusya",
									"reference" : "1-AMKA-MEQ01",
									"msisdn" : "+254724618425",
									"purchase_order_id" : "3653",
									"passport_id_number" : "22734592",
									"item" : "AMKA M-Chaama Membership",
									"institution_till_id" : "1",
									"middle_name" : "mwende",
									"email" : "lucywanja.com@gmail.com",
									"quantity" : "1.00"
								},
								"type" : "MEMBERSHIP REGISTRATION",
								"response" : {
									"one_time_pin" : "One Time Pin Set",
									"get_profile" : "Session Profile Captured",
									"debit_float" : "No float amount to debit",
									"send_notification" : "Notification Sent. Please check EMAIL",
									"get_email_notification" : "Notification Captured",
									"create_enrollment" : "Enrollment Captured"
								},
								"description" : "PROCESSED|Response Status: Approved or completed successfully|Overall Status: Approved or completed successfully"
							}
						], [{
								"index" : 320301,
								"name" : "System User",
								"date_time" : "11 Sep 2016 12:13:42 PM EAT +0300",
								"request" : {
									"passport_id_number" : "22734592",
									"msisdn" : "+254724618425",
									"full_names" : "wanzaa mwende ndusya",
									"email" : "lucywanja.com@gmail.com",
									"item" : "AMKA M-Chaama Membership"
								},
								"type" : "MEMBERSHIP REGISTRATION REQUEST",
								"response" : {
									"get_profile" : "Session Profile Captured",
									"get_notification" : "Notification Captured",
									"debit_float" : "No float amount to debit",
									"member_registration" : "Successful",
									"sale_order" : "Sale Order",
									"send_notification" : "Notification Sent. Please check EMAIL"
								},
								"description" : "PROCESSED|Response Status: Approved or completed successfully|Overall Status: Approved or completed successfully"
							}
						], [{
								"index" : 320300,
								"name" : "System User",
								"date_time" : "11 Sep 2016 12:11:25 PM EAT +0300",
								"request" : {
									"passport_id_number" : "22734592",
									"msisdn" : "+25724618425",
									"full_names" : "wanzaa mwende ndusya",
									"email" : "lucywanja.com@gmail.com",
									"item" : "AMKA M-Chaama Membership"
								},
								"type" : "MEMBERSHIP REGISTRATION REQUEST",
								"response" : {
									"get_profile" : "Session Profile Captured",
									"get_notification" : "Notification Captured",
									"debit_float" : "No float amount to debit",
									"member_registration" : "Successful",
									"sale_order" : "Sale Order",
									"send_notification" : "Notification Sent. Please check EMAIL"
								},
								"description" : "PROCESSED|Response Status: Approved or completed successfully|Overall Status: Approved or completed successfully"
							}
						], [{
								"index" : 320299,
								"name" : "System User",
								"date_time" : "11 Sep 2016 12:11:24 PM EAT +0300",
								"request" : {
									"passport_id_number" : "22734592",
									"msisdn" : "+25724618425",
									"full_names" : "wanzaa mwende ndusya",
									"email" : "lucywanja.com@gmail.com",
									"item" : "AMKA M-Chaama Membership"
								},
								"type" : "MEMBERSHIP REGISTRATION REQUEST",
								"response" : {
									"get_profile" : "Session Profile Captured",
									"get_notification" : "Notification Captured",
									"debit_float" : "No float amount to debit",
									"member_registration" : "Successful",
									"sale_order" : "Sale Order",
									"send_notification" : "Notification Sent. Please check EMAIL"
								},
								"description" : "PROCESSED|Response Status: Approved or completed successfully|Overall Status: Approved or completed successfully"
							}
						], [{
								"index" : 320298,
								"name" : "System User",
								"date_time" : "11 Sep 2016 12:11:21 PM EAT +0300",
								"request" : {
									"passport_id_number" : "22734592",
									"msisdn" : "+25724618425",
									"full_names" : "wanzaa mwende ndusya",
									"email" : "lucywanja.com@gmail.com",
									"item" : "AMKA M-Chaama Membership"
								},
								"type" : "MEMBERSHIP REGISTRATION REQUEST",
								"response" : {
									"get_profile" : "Session Profile Captured",
									"get_notification" : "Notification Captured",
									"debit_float" : "No float amount to debit",
									"member_registration" : "Successful",
									"sale_order" : "Sale Order",
									"send_notification" : "Notification Sent. Please check EMAIL"
								},
								"description" : "PROCESSED|Response Status: Approved or completed successfully|Overall Status: Approved or completed successfully"
							}
						], [{
								"index" : 320297,
								"name" : "System User",
								"date_time" : "11 Sep 2016 12:09:28 PM EAT +0300",
								"request" : {
									"passport_id_number" : "22734592",
									"msisdn" : "+254724618425",
									"full_names" : "wanzaa mwende ndusya",
									"email" : "lucywanja.com@gmail.com",
									"item" : "AMKA M-Chaama Membership"
								},
								"type" : "MEMBERSHIP REGISTRATION REQUEST",
								"response" : {
									"get_profile" : "Session Profile Captured",
									"get_notification" : "Notification Captured",
									"debit_float" : "No float amount to debit",
									"member_registration" : "Successful",
									"sale_order" : "Sale Order",
									"send_notification" : "Notification Sent. Please check EMAIL"
								},
								"description" : "PROCESSED|Response Status: Approved or completed successfully|Overall Status: Approved or completed successfully"
							}
						], [{
								"index" : 320296,
								"name" : "System User",
								"date_time" : "11 Sep 2016 12:09:23 PM EAT +0300",
								"request" : {
									"passport_id_number" : "22734592",
									"msisdn" : "+254724618425",
									"full_names" : "wanzaa mwende ndusya",
									"email" : "lucywanja.com@gmail.com",
									"item" : "AMKA M-Chaama Membership"
								},
								"type" : "MEMBERSHIP REGISTRATION REQUEST",
								"response" : {
									"get_profile" : "Session Profile Captured",
									"get_notification" : "Notification Captured",
									"debit_float" : "No float amount to debit",
									"member_registration" : "Successful",
									"sale_order" : "Sale Order",
									"send_notification" : "Notification Sent. Please check EMAIL"
								},
								"description" : "PROCESSED|Response Status: Approved or completed successfully|Overall Status: Approved or completed successfully"
							}
						], [{
								"index" : 320293,
								"name" : "System User",
								"date_time" : "11 Sep 2016 08:43:27 AM EAT +0300",
								"request" : {
									"passport_id_number" : "12772843",
									"msisdn" : "+254722177609",
									"full_names" : "jossie billey musya ",
									"email" : "lamukyuso@yahoo.com",
									"item" : "AMKA M-Chaama Membership"
								},
								"type" : "MEMBERSHIP REGISTRATION REQUEST",
								"response" : {
									"get_profile" : "Session Profile Captured",
									"get_notification" : "Notification Captured",
									"debit_float" : "No float amount to debit",
									"member_registration" : "Successful",
									"sale_order" : "Sale Order",
									"send_notification" : "Notification Sent. Please check EMAIL"
								},
								"description" : "PROCESSED|Response Status: Approved or completed successfully|Overall Status: Approved or completed successfully"
							}
						], [{
								"index" : 320292,
								"name" : "System User",
								"date_time" : "11 Sep 2016 08:43:15 AM EAT +0300",
								"request" : {
									"passport_id_number" : "12772843",
									"msisdn" : "+254722177609",
									"full_names" : "jossie billey musya ",
									"email" : "lamukyuso@yahoo.com",
									"item" : "AMKA M-Chaama Membership"
								},
								"type" : "MEMBERSHIP REGISTRATION REQUEST",
								"response" : {
									"get_profile" : "Session Profile Captured",
									"get_notification" : "Notification Captured",
									"debit_float" : "No float amount to debit",
									"member_registration" : "Successful",
									"sale_order" : "Sale Order",
									"send_notification" : "Notification Sent. Please check EMAIL"
								},
								"description" : "PROCESSED|Response Status: Approved or completed successfully|Overall Status: Approved or completed successfully"
							}
						], [{
								"index" : 320290,
								"name" : "System User",
								"date_time" : "11 Sep 2016 08:43:11 AM EAT +0300",
								"request" : {
									"passport_id_number" : "12772843",
									"msisdn" : "+254722177609",
									"full_names" : "jossie billey musya ",
									"email" : "lamukyuso@yahoo.com",
									"item" : "AMKA M-Chaama Membership"
								},
								"type" : "MEMBERSHIP REGISTRATION REQUEST",
								"response" : {
									"get_profile" : "Session Profile Captured",
									"get_notification" : "Notification Captured",
									"debit_float" : "No float amount to debit",
									"member_registration" : "Successful",
									"sale_order" : "Sale Order",
									"send_notification" : "Notification Sent. Please check EMAIL"
								},
								"description" : "PROCESSED|Response Status: Approved or completed successfully|Overall Status: Approved or completed successfully"
							}
						], [{
								"index" : 320291,
								"name" : "System User",
								"date_time" : "11 Sep 2016 08:43:10 AM EAT +0300",
								"request" : {
									"passport_id_number" : "12772843",
									"msisdn" : "+254722177609",
									"full_names" : "jossie billey musya ",
									"email" : "lamukyuso@yahoo.com",
									"item" : "AMKA M-Chaama Membership"
								},
								"type" : "MEMBERSHIP REGISTRATION REQUEST",
								"response" : {
									"get_profile" : "Session Profile Captured",
									"get_notification" : "Notification Captured",
									"debit_float" : "No float amount to debit",
									"member_registration" : "Successful",
									"sale_order" : "Sale Order",
									"send_notification" : "Notification Sent. Please check EMAIL"
								},
								"description" : "PROCESSED|Response Status: Approved or completed successfully|Overall Status: Approved or completed successfully"
							}
						], [{
								"index" : 320285,
								"name" : "System User",
								"date_time" : "11 Sep 2016 08:11:44 AM EAT +0300",
								"request" : {
									"passport_id_number" : "22734592",
									"msisdn" : "+254724618425",
									"full_names" : "wanzaa mwende ndusya",
									"email" : "lucywanja.com@gmail.com",
									"item" : "AMKA M-Chaama Membership"
								},
								"type" : "MEMBERSHIP REGISTRATION REQUEST",
								"response" : {
									"get_profile" : "Session Profile Captured",
									"get_notification" : "Notification Captured",
									"debit_float" : "No float amount to debit",
									"member_registration" : "Successful",
									"sale_order" : "Sale Order",
									"send_notification" : "Notification Sent. Please check EMAIL"
								},
								"description" : "PROCESSED|Response Status: Approved or completed successfully|Overall Status: Approved or completed successfully"
							}
						], [{
								"index" : 320286,
								"name" : "System User",
								"date_time" : "11 Sep 2016 08:11:38 AM EAT +0300",
								"request" : {
									"passport_id_number" : "22734592",
									"msisdn" : "+254724618425",
									"full_names" : "wanzaa mwende ndusya",
									"email" : "lucywanja.com@gmail.com",
									"item" : "AMKA M-Chaama Membership"
								},
								"type" : "MEMBERSHIP REGISTRATION REQUEST",
								"response" : {
									"get_profile" : "Session Profile Captured",
									"get_notification" : "Notification Captured",
									"debit_float" : "No float amount to debit",
									"member_registration" : "Successful",
									"sale_order" : "Sale Order",
									"send_notification" : "Notification Sent. Please check EMAIL"
								},
								"description" : "PROCESSED|Response Status: Approved or completed successfully|Overall Status: Approved or completed successfully"
							}
						], [{
								"index" : 320284,
								"name" : "System User",
								"date_time" : "10 Sep 2016 08:58:04 PM EAT +0300",
								"request" : {
									"full_names" : "Wilson Mwangi maina",
									"accesspoint" : "808",
									"msisdn" : "+254726996551",
									"access_point" : "*808*77#",
									"passport_id_number" : "22103031",
									"item" : "AMKA M-Chaama Membership",
									"payment_method" : "M-PESA"
								},
								"type" : "MEMBERSHIP REGISTRATION REQUEST",
								"response" : {
									"get_profile" : "Session Profile Captured",
									"member_registration" : "Successful",
									"remit" : "Remit Successful",
									"sale_order" : "Sale Order"
								},
								"description" : "PROCESSED|Response Status: Approved or completed successfully|Overall Status: Approved or completed successfully"
							}
						], [{
								"index" : 320283,
								"name" : "SAMSON MBURU",
								"date_time" : "10 Sep 2016 08:12:13 PM EAT +0300",
								"request" : {
									"msisdn" : "+254722491320",
									"access_point" : "*808*77#",
									"accesspoint" : "808"
								},
								"type" : "CONFIRM ONE TIME PIN",
								"response" : {
									"set_profile_pin" : "New PIN isSet",
									"validate_one_time_pin" : "Valid One Time PIN",
									"get_profile" : "Session Profile Captured"
								},
								"description" : "PROCESSED|Response Status: Approved or completed successfully|Overall Status: Approved or completed successfully"
							}
						]],
					"data" : [[{
								"index" : 320356,
								"name" : "JACINTA MUTISO",
								"date_time" : "15 Sep 2016 03:25:42 PM EAT +0300",
								"request" : {
									"full_names" : "Jacinta A. W. Mutiso",
									"accesspoint" : "808",
									"msisdn" : "+254713373524",
									"access_point" : "*808*77#",
									"passport_id_number" : "2974566",
									"item" : "AMKA M-Chaama Membership",
									"payment_method" : "M-PESA"
								},
								"type" : "MEMBERSHIP REGISTRATION REQUEST",
								"response" : {
									"get_profile" : "Session Profile Captured",
									"member_registration" : "Successful",
									"remit" : "Remit Successful",
									"sale_order" : "Sale Order"
								},
								"description" : "PROCESSED|Response Status: Approved or completed successfully|Overall Status: Approved or completed successfully"
							}
						], [{
								"index" : 320355,
								"name" : "charles ngamau",
								"date_time" : "15 Sep 2016 03:20:26 PM EAT +0300",
								"request" : {
									"full_names" : "charles ngamau",
									"accesspoint" : "808",
									"msisdn" : "+254718482176",
									"access_point" : "*808*77#",
									"passport_id_number" : "29116298",
									"item" : "AMKA M-Chaama Membership",
									"payment_method" : "M-PESA"
								},
								"type" : "MEMBERSHIP REGISTRATION REQUEST",
								"response" : {
									"get_profile" : "Session Profile Captured",
									"member_registration" : "Successful",
									"remit" : "Remit Successful",
									"sale_order" : "Sale Order"
								},
								"description" : "PROCESSED|Response Status: Approved or completed successfully|Overall Status: Approved or completed successfully"
							}
						], [{
								"index" : 320354,
								"name" : "charles ngamau",
								"date_time" : "15 Sep 2016 12:22:38 PM EAT +0300",
								"request" : {
									"full_names" : "charles ngamau",
									"accesspoint" : "808",
									"msisdn" : "+254718482176",
									"access_point" : "*808*77#",
									"passport_id_number" : "29116298",
									"item" : "AMKA M-Chaama Membership",
									"payment_method" : "M-PESA"
								},
								"type" : "MEMBERSHIP REGISTRATION REQUEST",
								"response" : {
									"get_profile" : "Session Profile Captured",
									"member_registration" : "Successful",
									"remit" : "Remit Successful",
									"sale_order" : "Sale Order"
								},
								"description" : "PROCESSED|Response Status: Approved or completed successfully|Overall Status: Approved or completed successfully"
							}
						], [{
								"index" : 320353,
								"name" : "charles ngamau",
								"date_time" : "15 Sep 2016 12:18:44 PM EAT +0300",
								"request" : {
									"full_names" : "charles ngamau",
									"accesspoint" : "808",
									"msisdn" : "+254718482176",
									"access_point" : "*808*77#",
									"passport_id_number" : "29116298",
									"item" : "AMKA M-Chaama Membership",
									"payment_method" : "M-PESA"
								},
								"type" : "MEMBERSHIP REGISTRATION REQUEST",
								"response" : {
									"get_profile" : "Session Profile Captured",
									"member_registration" : "Successful",
									"remit" : "Remit Successful",
									"sale_order" : "Sale Order"
								},
								"description" : "PROCESSED|Response Status: Approved or completed successfully|Overall Status: Approved or completed successfully"
							}
						], [{
								"index" : 320352,
								"name" : "System User",
								"date_time" : "15 Sep 2016 12:04:05 PM EAT +0300",
								"request" : {
									"full_names" : "charles ngamau",
									"accesspoint" : "808",
									"msisdn" : "+254718482176",
									"access_point" : "*808*77#",
									"passport_id_number" : "29116298",
									"item" : "AMKA M-Chaama Membership",
									"payment_method" : "M-PESA"
								},
								"type" : "MEMBERSHIP REGISTRATION REQUEST",
								"response" : {
									"get_profile" : "Session Profile Captured",
									"member_registration" : "Successful",
									"remit" : "Remit Successful",
									"sale_order" : "Sale Order"
								},
								"description" : "PROCESSED|Response Status: Approved or completed successfully|Overall Status: Approved or completed successfully"
							}
						], [{
								"index" : 320350,
								"name" : "Amka Group",
								"date_time" : "14 Sep 2016 02:36:20 PM EAT +0300",
								"request" : {
									"scheduled_time" : "2:21 pm",
									"message" : "Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
									"scheduled _date" : "14/09/2016",
									"notification_product" : "74"
								},
								"type" : "NOTIFICATION PRODUCT SEND",
								"response" : {
									"check_transaction_auth" : "Operator Transaction found",
									"log_outbound_message" : "Outbound Message Processed",
									"notification_debit_float" : " | Float Debited with: 2896.00 balance: 5965.00"
								},
								"description" : "PROCESSED|Response Status: Approved or completed successfully|Overall Status: Approved or completed successfully"
							}
						], [{
								"index" : 320349,
								"name" : "DANIEL KIMITI",
								"date_time" : "14 Sep 2016 01:56:46 PM EAT +0300",
								"request" : {
									"msisdn" : "+254722301125",
									"access_point" : "*808*77#",
									"account_type_id" : "3",
									"payment_method" : "M-PESA",
									"accesspoint" : "808"
								},
								"type" : "LOAN REQUEST",
								"response" : {
									"loan_status" : "Principal-Loan-Due Date\nKES 1,000.00-KES 1,100.00-08/Oct/2016\nKES 1,000.00-KES 1,100.00-06/Oct/2016\n",
									"get_account" : "Account Captured",
									"loan_limit" : "Credit Limit KES 7,200.00\nAvailable Limit KES 600.00",
									"get_profile" : "Session Profile Captured",
									"loan_item_details" : "Exceeds withdrawal limit. Credit Limit KES 7,200.00\nAvailable Limit KES 600.00"
								},
								"description" : "PROCESSED|Response Status: Exceeds withdrawal limit|Overall Status: Exceeds withdrawal limit"
							}
						], [{
								"index" : 320348,
								"name" : "DANIEL KIMITI",
								"date_time" : "14 Sep 2016 01:39:02 PM EAT +0300",
								"request" : {
									"msisdn" : "+254722301125",
									"access_point" : "*808*77#",
									"reference" : "1-AMKA-RUP53",
									"payment_method" : "MIPAY",
									"accesspoint" : "808"
								},
								"type" : "ORDER CHECKOUT",
								"response" : {
									"get_bill" : "Bill Balance: 1100.00",
									"pay_bill" : "Bill Payment Accepted. Balance: 695.00",
									"get_account" : "Account Captured",
									"account_balance" : "KES 405.00",
									"debit_account" : "Account Debited"
								},
								"description" : "PROCESSED|Response Status: Approved or completed successfully|Overall Status: Approved or completed successfully"
							}
						], [{
								"index" : 320347,
								"name" : "JACINTA MUTISO",
								"date_time" : "14 Sep 2016 09:17:12 AM EAT +0300",
								"request" : {
									"full_names" : "Jacinta Wanzila mutisa",
									"accesspoint" : "808",
									"msisdn" : "+254713373524",
									"access_point" : "*808*77#",
									"passport_id_number" : "2974566",
									"item" : "AMKA M-Chaama Membership",
									"payment_method" : "M-PESA"
								},
								"type" : "MEMBERSHIP REGISTRATION REQUEST",
								"response" : {
									"get_profile" : "Session Profile Captured",
									"member_registration" : "Successful",
									"remit" : "Remit Successful",
									"sale_order" : "Sale Order"
								},
								"description" : "PROCESSED|Response Status: Approved or completed successfully|Overall Status: Approved or completed successfully"
							}
						], [{
								"index" : 320341,
								"name" : "System User",
								"date_time" : "14 Sep 2016 01:43:27 AM EAT +0300",
								"request" : {
									"passport_id_number" : "29595134",
									"msisdn" : "+254701701916",
									"full_names" : "pamela mwende",
									"email" : "pamelamwende4@gmail.com",
									"item" : "AMKA M-Chaama Membership"
								},
								"type" : "MEMBERSHIP REGISTRATION REQUEST",
								"response" : {
									"get_profile" : "Session Profile Captured",
									"get_notification" : "Notification Captured",
									"debit_float" : "No float amount to debit",
									"member_registration" : "Successful",
									"sale_order" : "Sale Order",
									"send_notification" : "Notification Sent. Please check EMAIL"
								},
								"description" : "PROCESSED|Response Status: Approved or completed successfully|Overall Status: Approved or completed successfully"
							}
						], [{
								"index" : 320340,
								"name" : "System User",
								"date_time" : "14 Sep 2016 01:43:25 AM EAT +0300",
								"request" : {
									"passport_id_number" : "29595134",
									"msisdn" : "+254701701916",
									"full_names" : "pamela mwende",
									"email" : "pamelamwende4@gmail.com",
									"item" : "AMKA M-Chaama Membership"
								},
								"type" : "MEMBERSHIP REGISTRATION REQUEST",
								"response" : {
									"get_profile" : "Session Profile Captured",
									"get_notification" : "Notification Captured",
									"debit_float" : "No float amount to debit",
									"member_registration" : "Successful",
									"sale_order" : "Sale Order",
									"send_notification" : "Notification Sent. Please check EMAIL"
								},
								"description" : "PROCESSED|Response Status: Approved or completed successfully|Overall Status: Approved or completed successfully"
							}
						], [{
								"index" : 320339,
								"name" : "System User",
								"date_time" : "14 Sep 2016 12:21:11 AM EAT +0300",
								"request" : {
									"passport_id_number" : "31610811",
									"msisdn" : "+254722331493",
									"full_names" : "Emmanuel Kangere waruhiu",
									"email" : "kangere20@gmail.com",
									"item" : "AMKA M-Chaama Membership"
								},
								"type" : "MEMBERSHIP REGISTRATION REQUEST",
								"response" : {
									"get_profile" : "Session Profile Captured",
									"get_notification" : "Notification Captured",
									"debit_float" : "No float amount to debit",
									"member_registration" : "Successful",
									"sale_order" : "Sale Order",
									"send_notification" : "Notification Sent. Please check EMAIL"
								},
								"description" : "PROCESSED|Response Status: Approved or completed successfully|Overall Status: Approved or completed successfully"
							}
						], [{
								"index" : 320338,
								"name" : "GRACE KIIRI",
								"date_time" : "12 Sep 2016 07:36:14 PM EAT +0300",
								"request" : {
									"msisdn" : "+254721640597",
									"access_point" : "*808*77#",
									"account_type_id" : "3",
									"payment_method" : "M-PESA",
									"accesspoint" : "808"
								},
								"type" : "LOAN REQUEST",
								"response" : {
									"loan_limit" : "Credit Limit KES 2,000.00\nAvailable Limit KES 2,000.00",
									"get_profile" : "Session Profile Captured",
									"get_notification" : "Notification Captured",
									"remit" : "Remit Successful",
									"get_account" : "Account Captured",
									"debit_float" : "Float Debited with: 1.00 balance: 8861.00",
									"loan_item_details" : "Captured",
									"debit_account" : "Account Debited",
									"loan_status" : "No Loan Record Available",
									"sale_order" : "Sale Order",
									"send_notification" : "Notification Sent. Please check SMS"
								},
								"description" : "PROCESSED|Response Status: Approved or completed successfully|Overall Status: Approved or completed successfully"
							}
						], [{
								"index" : 320337,
								"name" : "GRACE KIIRI",
								"date_time" : "12 Sep 2016 07:33:30 PM EAT +0300",
								"request" : {
									"msisdn" : "+254721640597",
									"access_point" : "*808*77#",
									"account_type_id" : "3",
									"payment_method" : "M-PESA",
									"accesspoint" : "808"
								},
								"type" : "LOAN REQUEST",
								"response" : {
									"loan_status" : "No Loan Record Available",
									"get_account" : "Account Captured",
									"loan_limit" : "Credit Limit KES 2,000.00\nAvailable Limit KES 2,000.00",
									"get_profile" : "Session Profile Captured",
									"loan_item_details" : "Exceeds withdrawal limit. Credit Limit KES 2,000.00\nAvailable Limit KES 2,000.00"
								},
								"description" : "PROCESSED|Response Status: Exceeds withdrawal limit|Overall Status: Exceeds withdrawal limit"
							}
						], [{
								"index" : 320336,
								"name" : "GRACE KIIRI",
								"date_time" : "12 Sep 2016 07:30:28 PM EAT +0300",
								"request" : {
									"username" : "+254721640597",
									"last_name" : "KIIRI",
									"reference" : "1-AMKA-MZF00",
									"payment_method" : "M-PESA",
									"access_point" : "*808*77#",
									"credit_total" : "0",
									"first_name" : "GRACE",
									"institution_till_id" : "1",
									"accesspoint" : "808",
									"account_manager_id" : "4045",
									"product_item_id" : "99770",
									"due_date" : "28/Sep/2016",
									"overdue_credit" : "False",
									"balance_bf" : "-2200.00",
									"msisdn" : "+254721640597",
									"purchase_order_id" : "3495",
									"credit_limit" : "2000.00",
									"float_amount" : "2000",
									"available_limit" : "2000.00",
									"account_type_id" : "3",
									"available_credit" : "0",
									"quantity" : "2200.00"
								},
								"type" : "LOAN REPAYMENT",
								"response" : {
									"get_profile" : "Session Profile Captured",
									"get_notification" : "Notification Captured",
									"debit_float" : "Float Debited with: 1.00 balance: 8862.00",
									"loan_repayment" : "Loan Repaid",
									"get_account" : "Account Captured",
									"send_notification" : "Notification Sent. Please check SMS",
									"amka_credit_limit_review" : "Loan Limit Reviewed",
									"credit_account" : "Account Credited"
								},
								"description" : "PROCESSED|Response Status: Approved or completed successfully|Overall Status: Approved or completed successfully"
							}
						], [{
								"index" : 320334,
								"name" : "GRACE KIIRI",
								"date_time" : "12 Sep 2016 07:29:36 PM EAT +0300",
								"request" : {
									"msisdn" : "+254721640597",
									"access_point" : "*808*77#",
									"reference" : "1-AMKA-MZF00",
									"payment_method" : "M-PESA",
									"accesspoint" : "808"
								},
								"type" : "ORDER CHECKOUT",
								"response" : {
									"get_bill" : "Bill Balance: 2200.00",
									"remit" : "Remit Successful"
								},
								"description" : "PROCESSED|Response Status: Approved or completed successfully|Overall Status: Approved or completed successfully"
							}
						], [{
								"index" : 320333,
								"name" : "System User",
								"date_time" : "12 Sep 2016 01:30:34 PM EAT +0300",
								"request" : {
									"full_names" : "Mary njeri macharia",
									"accesspoint" : "*808*77#",
									"msisdn" : "+254732699033",
									"access_point" : "*808*77#",
									"passport_id_number" : "11588626",
									"item" : "AMKA M-Chaama Membership",
									"payment_method" : "M-PESA"
								},
								"type" : "MEMBERSHIP REGISTRATION REQUEST",
								"response" : {
									"get_profile" : "Session Profile Captured",
									"member_registration" : "Successful",
									"remit" : "Remit Successful",
									"sale_order" : "Sale Order"
								},
								"description" : "PROCESSED|Response Status: Approved or completed successfully|Overall Status: Approved or completed successfully"
							}
						], [{
								"index" : 320332,
								"name" : "System User",
								"date_time" : "12 Sep 2016 01:21:58 PM EAT +0300",
								"request" : {
									"passport_id_number" : "24379191",
									"msisdn" : "+254711850004",
									"full_names" : "Martin mbugua ",
									"email" : "martinmbg@yahoo.com",
									"item" : "AMKA M-Chaama Membership"
								},
								"type" : "MEMBERSHIP REGISTRATION REQUEST",
								"response" : {
									"get_profile" : "Session Profile Captured",
									"member_registration" : "Duplicate record. Record Exists"
								},
								"description" : "PROCESSED|Response Status: Duplicate record|Overall Status: Duplicate record"
							}
						], [{
								"index" : 320331,
								"name" : "System User",
								"date_time" : "12 Sep 2016 01:21:51 PM EAT +0300",
								"request" : {
									"passport_id_number" : "24379191",
									"msisdn" : "+254711850004",
									"full_names" : "Martin mbugua ",
									"email" : "martinmbg@yahoo.com",
									"item" : "AMKA M-Chaama Membership"
								},
								"type" : "MEMBERSHIP REGISTRATION REQUEST",
								"response" : {
									"get_profile" : "Session Profile Captured",
									"member_registration" : "Duplicate record. Record Exists"
								},
								"description" : "PROCESSED|Response Status: Duplicate record|Overall Status: Duplicate record"
							}
						], [{
								"index" : 320330,
								"name" : "System User",
								"date_time" : "12 Sep 2016 01:21:42 PM EAT +0300",
								"request" : {
									"passport_id_number" : "24379191",
									"msisdn" : "+254711850004",
									"full_names" : "Martin mbugua ",
									"email" : "martinmbg@yahoo.com",
									"item" : "AMKA M-Chaama Membership"
								},
								"type" : "MEMBERSHIP REGISTRATION REQUEST",
								"response" : {
									"get_profile" : "Session Profile Captured",
									"member_registration" : "Duplicate record. Record Exists"
								},
								"description" : "PROCESSED|Response Status: Duplicate record|Overall Status: Duplicate record"
							}
						], [{
								"index" : 320329,
								"name" : "System User",
								"date_time" : "12 Sep 2016 01:21:34 PM EAT +0300",
								"request" : {
									"passport_id_number" : "24379191",
									"msisdn" : "+254711850004",
									"full_names" : "Martin mbugua ",
									"email" : "martinmbg@yahoo.com",
									"item" : "AMKA M-Chaama Membership"
								},
								"type" : "MEMBERSHIP REGISTRATION REQUEST",
								"response" : {
									"get_profile" : "Session Profile Captured",
									"member_registration" : "Duplicate record. Record Exists"
								},
								"description" : "PROCESSED|Response Status: Duplicate record|Overall Status: Duplicate record"
							}
						], [{
								"index" : 320327,
								"name" : "Martin wachera",
								"date_time" : "12 Sep 2016 12:58:51 PM EAT +0300",
								"request" : {
									"product_item_id" : "99767",
									"username" : "martinmbg",
									"first_name" : "Martin",
									"last_name" : "wachera",
									"full_names" : "Martin mbugua wachera",
									"reference" : "1-AMKA-QFA24",
									"msisdn" : "+254711850004",
									"purchase_order_id" : "3659",
									"passport_id_number" : "24379191",
									"item" : "AMKA M-Chaama Membership",
									"institution_till_id" : "1",
									"middle_name" : "mbugua",
									"email" : "martinmbg@yahoo.com",
									"quantity" : "1.00"
								},
								"type" : "MEMBERSHIP REGISTRATION",
								"response" : {
									"one_time_pin" : "One Time Pin Set",
									"get_profile" : "Session Profile Captured",
									"debit_float" : "No float amount to debit",
									"send_notification" : "Notification Sent. Please check EMAIL",
									"get_email_notification" : "Notification Captured",
									"create_enrollment" : "Enrollment Captured"
								},
								"description" : "PROCESSED|Response Status: Approved or completed successfully|Overall Status: Approved or completed successfully"
							}
						], [{
								"index" : 320323,
								"name" : "System User",
								"date_time" : "12 Sep 2016 12:55:02 PM EAT +0300",
								"request" : {
									"passport_id_number" : "24379191",
									"msisdn" : "+254711850004",
									"full_names" : "Martin mbugua wachera",
									"email" : "martinmbg@yahoo.com",
									"item" : "AMKA M-Chaama Membership"
								},
								"type" : "MEMBERSHIP REGISTRATION REQUEST",
								"response" : {
									"get_profile" : "Session Profile Captured",
									"get_notification" : "Notification Captured",
									"debit_float" : "No float amount to debit",
									"member_registration" : "Successful",
									"sale_order" : "Sale Order",
									"send_notification" : "Notification Sent. Please check EMAIL"
								},
								"description" : "PROCESSED|Response Status: Approved or completed successfully|Overall Status: Approved or completed successfully"
							}
						], [{
								"index" : 320322,
								"name" : "System User",
								"date_time" : "12 Sep 2016 12:55:00 PM EAT +0300",
								"request" : {
									"passport_id_number" : "24379191",
									"msisdn" : "+254711850004",
									"full_names" : "Martin mbugua wachera",
									"email" : "martinmbg@yahoo.com",
									"item" : "AMKA M-Chaama Membership"
								},
								"type" : "MEMBERSHIP REGISTRATION REQUEST",
								"response" : {
									"get_profile" : "Session Profile Captured",
									"get_notification" : "Notification Captured",
									"debit_float" : "No float amount to debit",
									"member_registration" : "Successful",
									"sale_order" : "Sale Order",
									"send_notification" : "Notification Sent. Please check EMAIL"
								},
								"description" : "PROCESSED|Response Status: Approved or completed successfully|Overall Status: Approved or completed successfully"
							}
						], [{
								"index" : 320321,
								"name" : "GRACE KIIRI",
								"date_time" : "12 Sep 2016 12:29:58 PM EAT +0300",
								"request" : {
									"msisdn" : "+254721640597",
									"access_point" : "*808*77#",
									"reference" : "1-AMKA-MZF00",
									"payment_method" : "M-PESA",
									"accesspoint" : "808"
								},
								"type" : "ORDER CHECKOUT",
								"response" : {
									"get_bill" : "Bill Balance: 2200.00",
									"remit" : "Remit Successful"
								},
								"description" : "PROCESSED|Response Status: Approved or completed successfully|Overall Status: Approved or completed successfully"
							}
						], [{
								"index" : 320320,
								"name" : "GRACE KIIRI",
								"date_time" : "12 Sep 2016 12:27:02 PM EAT +0300",
								"request" : {
									"msisdn" : "+254721640597",
									"access_point" : "*808*77#",
									"reference" : "1-AMKA-MZF00",
									"payment_method" : "M-PESA",
									"accesspoint" : "808"
								},
								"type" : "ORDER CHECKOUT",
								"response" : {
									"get_bill" : "Bill Balance: 2200.00",
									"remit" : "Remit Successful"
								},
								"description" : "PROCESSED|Response Status: Approved or completed successfully|Overall Status: Approved or completed successfully"
							}
						], [{
								"index" : 320319,
								"name" : "Amka Group",
								"date_time" : "12 Sep 2016 11:59:20 AM EAT +0300",
								"request" : {
									"scheduled_time" : "11:51 am",
									"message" : "Now Invest,Borrow or Lend 24Hrs/7dys on M-Chaama & grow via High Dividends! Lenders Pie now@13,500 upto 10.10.16.Sacco Mradi-Loans Available",
									"scheduled _date" : "12/09/2016",
									"notification_product" : "74"
								},
								"type" : "NOTIFICATION PRODUCT SEND",
								"response" : {
									"check_transaction_auth" : "Operator Transaction found",
									"log_outbound_message" : "Outbound Message Processed",
									"notification_debit_float" : " | Float Debited with: 2896.00 balance: 8863.00"
								},
								"description" : "PROCESSED|Response Status: Approved or completed successfully|Overall Status: Approved or completed successfully"
							}
						], [{
								"index" : 320317,
								"name" : "PATRICK ADEGO",
								"date_time" : "12 Sep 2016 07:29:13 AM EAT +0300",
								"request" : {
									"username" : "+254720337751",
									"product_item_id" : "99767",
									"first_name" : "PATRICK",
									"last_name" : "ADEGO",
									"full_names" : "Patrick Odari Adego",
									"reference" : "1-AMKA-UYC61",
									"accesspoint" : "808",
									"payment_method" : "M-PESA",
									"access_point" : "*808*77#",
									"purchase_order_id" : "3657",
									"passport_id_number" : "12471079",
									"msisdn" : "+254720337751",
									"institution_till_id" : "1",
									"item" : "AMKA M-Chaama Membership",
									"middle_name" : "Odari",
									"quantity" : "1.00"
								},
								"type" : "MEMBERSHIP REGISTRATION",
								"response" : {
									"one_time_pin" : "One Time Pin Set",
									"get_profile" : "Session Profile Captured",
									"get_notification" : "Notification Captured",
									"debit_float" : "Float Debited with: 1.00 balance: 11759.00",
									"send_notification" : "Notification Sent. Please check SMS",
									"create_enrollment" : "Enrollment Captured"
								},
								"description" : "PROCESSED|Response Status: Approved or completed successfully|Overall Status: Approved or completed successfully"
							}
						], [{
								"index" : 320315,
								"name" : "PATRICK ADEGO",
								"date_time" : "12 Sep 2016 07:28:17 AM EAT +0300",
								"request" : {
									"full_names" : "Patrick Odari Adego",
									"accesspoint" : "808",
									"msisdn" : "+254720337751",
									"access_point" : "*808*77#",
									"passport_id_number" : "12471079",
									"item" : "AMKA M-Chaama Membership",
									"payment_method" : "M-PESA"
								},
								"type" : "MEMBERSHIP REGISTRATION REQUEST",
								"response" : {
									"get_profile" : "Session Profile Captured",
									"member_registration" : "Successful",
									"remit" : "Remit Successful",
									"sale_order" : "Sale Order"
								},
								"description" : "PROCESSED|Response Status: Approved or completed successfully|Overall Status: Approved or completed successfully"
							}
						], [{
								"index" : 320314,
								"name" : "PATRICK ADEGO",
								"date_time" : "12 Sep 2016 07:18:23 AM EAT +0300",
								"request" : {
									"full_names" : "Patrick Odari Adego",
									"accesspoint" : "808",
									"msisdn" : "+254720337751",
									"access_point" : "*808*77#",
									"passport_id_number" : "12471079",
									"item" : "AMKA M-Chaama Membership",
									"payment_method" : "M-PESA"
								},
								"type" : "MEMBERSHIP REGISTRATION REQUEST",
								"response" : {
									"get_profile" : "Session Profile Captured",
									"member_registration" : "Successful",
									"remit" : "Remit Successful",
									"sale_order" : "Sale Order"
								},
								"description" : "PROCESSED|Response Status: Approved or completed successfully|Overall Status: Approved or completed successfully"
							}
						], [{
								"index" : 320313,
								"name" : "PATRICK ADEGO",
								"date_time" : "12 Sep 2016 07:05:46 AM EAT +0300",
								"request" : {
									"full_names" : "Patrick Odari Adego",
									"accesspoint" : "808",
									"msisdn" : "+254720337751",
									"access_point" : "*808*77#",
									"passport_id_number" : "12471879",
									"item" : "AMKA M-Chaama Membership",
									"payment_method" : "M-PESA"
								},
								"type" : "MEMBERSHIP REGISTRATION REQUEST",
								"response" : {
									"get_profile" : "Session Profile Captured",
									"member_registration" : "Successful",
									"remit" : "Remit Successful",
									"sale_order" : "Sale Order"
								},
								"description" : "PROCESSED|Response Status: Approved or completed successfully|Overall Status: Approved or completed successfully"
							}
						], [{
								"index" : 320312,
								"name" : "PATRICK ADEGO",
								"date_time" : "12 Sep 2016 07:02:12 AM EAT +0300",
								"request" : {
									"full_names" : "Patrick Odari Adego",
									"accesspoint" : "808",
									"msisdn" : "+254720337751",
									"access_point" : "*808*77#",
									"passport_id_number" : "12471079",
									"item" : "AMKA M-Chaama Membership",
									"payment_method" : "M-PESA"
								},
								"type" : "MEMBERSHIP REGISTRATION REQUEST",
								"response" : {
									"get_profile" : "Session Profile Captured",
									"member_registration" : "Successful",
									"remit" : "Remit Successful",
									"sale_order" : "Sale Order"
								},
								"description" : "PROCESSED|Response Status: Approved or completed successfully|Overall Status: Approved or completed successfully"
							}
						], [{
								"index" : 320311,
								"name" : "HENRY KABURU",
								"date_time" : "11 Sep 2016 08:38:25 PM EAT +0300",
								"request" : {
									"product_item_id" : "99770",
									"due_date" : "09/Sep/2016",
									"overdue_credit" : "False",
									"credit_limit" : "1000.00",
									"balance_bf" : "-1100.00",
									"reference" : "1-AMKA-TLA24",
									"accesspoint" : "808",
									"payment_method" : "M-PESA",
									"access_point" : "*808*77#",
									"credit_total" : "0",
									"purchase_order_id" : "3092",
									"msisdn" : "+254724236531",
									"institution_till_id" : "1",
									"float_amount" : "1000",
									"available_credit" : "0",
									"available_limit" : "1000.00",
									"account_type_id" : "3",
									"account_manager_id" : "3923",
									"quantity" : "1100.00"
								},
								"type" : "LOAN REPAYMENT",
								"response" : {
									"get_profile" : "Session Profile Captured",
									"get_notification" : "Notification Captured",
									"debit_float" : "Float Debited with: 1.00 balance: 11760.00",
									"loan_repayment" : "Loan Repaid",
									"get_account" : "Account Captured",
									"send_notification" : "Notification Sent. Please check SMS",
									"amka_credit_limit_review" : "Loan Limit Reviewed",
									"credit_account" : "Account Credited"
								},
								"description" : "PROCESSED|Response Status: Approved or completed successfully|Overall Status: Approved or completed successfully"
							}
						], [{
								"index" : 320309,
								"name" : "HENRY KABURU",
								"date_time" : "11 Sep 2016 08:36:51 PM EAT +0300",
								"request" : {
									"msisdn" : "+254724236531",
									"access_point" : "*808*77#",
									"reference" : "1-AMKA-TLA24",
									"payment_method" : "M-PESA",
									"accesspoint" : "808"
								},
								"type" : "ORDER CHECKOUT",
								"response" : {
									"get_bill" : "Bill Balance: 1100.00",
									"remit" : "Remit Successful"
								},
								"description" : "PROCESSED|Response Status: Approved or completed successfully|Overall Status: Approved or completed successfully"
							}
						], [{
								"index" : 320308,
								"name" : "HENRY KABURU",
								"date_time" : "11 Sep 2016 08:34:04 PM EAT +0300",
								"request" : {
									"msisdn" : "+254724236531",
									"access_point" : "*808*77#",
									"reference" : "1-AMKA-TLA24",
									"payment_method" : "M-PESA",
									"accesspoint" : "808"
								},
								"type" : "ORDER CHECKOUT",
								"response" : {
									"get_bill" : "Bill Balance: 1100.00",
									"remit" : "Remit Successful"
								},
								"description" : "PROCESSED|Response Status: Approved or completed successfully|Overall Status: Approved or completed successfully"
							}
						], [{
								"index" : 320305,
								"name" : "wanzaa ndusya",
								"date_time" : "11 Sep 2016 12:15:20 PM EAT +0300",
								"request" : {
									"product_item_id" : "99767",
									"username" : "lucywanja.com",
									"first_name" : "wanzaa",
									"last_name" : "ndusya",
									"full_names" : "wanzaa mwende ndusya",
									"reference" : "1-AMKA-MEQ01",
									"msisdn" : "+254724618425",
									"purchase_order_id" : "3653",
									"passport_id_number" : "22734592",
									"item" : "AMKA M-Chaama Membership",
									"institution_till_id" : "1",
									"middle_name" : "mwende",
									"email" : "lucywanja.com@gmail.com",
									"quantity" : "1.00"
								},
								"type" : "MEMBERSHIP REGISTRATION",
								"response" : {
									"one_time_pin" : "One Time Pin Set",
									"get_profile" : "Session Profile Captured",
									"debit_float" : "No float amount to debit",
									"send_notification" : "Notification Sent. Please check EMAIL",
									"get_email_notification" : "Notification Captured",
									"create_enrollment" : "Enrollment Captured"
								},
								"description" : "PROCESSED|Response Status: Approved or completed successfully|Overall Status: Approved or completed successfully"
							}
						], [{
								"index" : 320301,
								"name" : "System User",
								"date_time" : "11 Sep 2016 12:13:42 PM EAT +0300",
								"request" : {
									"passport_id_number" : "22734592",
									"msisdn" : "+254724618425",
									"full_names" : "wanzaa mwende ndusya",
									"email" : "lucywanja.com@gmail.com",
									"item" : "AMKA M-Chaama Membership"
								},
								"type" : "MEMBERSHIP REGISTRATION REQUEST",
								"response" : {
									"get_profile" : "Session Profile Captured",
									"get_notification" : "Notification Captured",
									"debit_float" : "No float amount to debit",
									"member_registration" : "Successful",
									"sale_order" : "Sale Order",
									"send_notification" : "Notification Sent. Please check EMAIL"
								},
								"description" : "PROCESSED|Response Status: Approved or completed successfully|Overall Status: Approved or completed successfully"
							}
						], [{
								"index" : 320300,
								"name" : "System User",
								"date_time" : "11 Sep 2016 12:11:25 PM EAT +0300",
								"request" : {
									"passport_id_number" : "22734592",
									"msisdn" : "+25724618425",
									"full_names" : "wanzaa mwende ndusya",
									"email" : "lucywanja.com@gmail.com",
									"item" : "AMKA M-Chaama Membership"
								},
								"type" : "MEMBERSHIP REGISTRATION REQUEST",
								"response" : {
									"get_profile" : "Session Profile Captured",
									"get_notification" : "Notification Captured",
									"debit_float" : "No float amount to debit",
									"member_registration" : "Successful",
									"sale_order" : "Sale Order",
									"send_notification" : "Notification Sent. Please check EMAIL"
								},
								"description" : "PROCESSED|Response Status: Approved or completed successfully|Overall Status: Approved or completed successfully"
							}
						], [{
								"index" : 320299,
								"name" : "System User",
								"date_time" : "11 Sep 2016 12:11:24 PM EAT +0300",
								"request" : {
									"passport_id_number" : "22734592",
									"msisdn" : "+25724618425",
									"full_names" : "wanzaa mwende ndusya",
									"email" : "lucywanja.com@gmail.com",
									"item" : "AMKA M-Chaama Membership"
								},
								"type" : "MEMBERSHIP REGISTRATION REQUEST",
								"response" : {
									"get_profile" : "Session Profile Captured",
									"get_notification" : "Notification Captured",
									"debit_float" : "No float amount to debit",
									"member_registration" : "Successful",
									"sale_order" : "Sale Order",
									"send_notification" : "Notification Sent. Please check EMAIL"
								},
								"description" : "PROCESSED|Response Status: Approved or completed successfully|Overall Status: Approved or completed successfully"
							}
						], [{
								"index" : 320298,
								"name" : "System User",
								"date_time" : "11 Sep 2016 12:11:21 PM EAT +0300",
								"request" : {
									"passport_id_number" : "22734592",
									"msisdn" : "+25724618425",
									"full_names" : "wanzaa mwende ndusya",
									"email" : "lucywanja.com@gmail.com",
									"item" : "AMKA M-Chaama Membership"
								},
								"type" : "MEMBERSHIP REGISTRATION REQUEST",
								"response" : {
									"get_profile" : "Session Profile Captured",
									"get_notification" : "Notification Captured",
									"debit_float" : "No float amount to debit",
									"member_registration" : "Successful",
									"sale_order" : "Sale Order",
									"send_notification" : "Notification Sent. Please check EMAIL"
								},
								"description" : "PROCESSED|Response Status: Approved or completed successfully|Overall Status: Approved or completed successfully"
							}
						], [{
								"index" : 320297,
								"name" : "System User",
								"date_time" : "11 Sep 2016 12:09:28 PM EAT +0300",
								"request" : {
									"passport_id_number" : "22734592",
									"msisdn" : "+254724618425",
									"full_names" : "wanzaa mwende ndusya",
									"email" : "lucywanja.com@gmail.com",
									"item" : "AMKA M-Chaama Membership"
								},
								"type" : "MEMBERSHIP REGISTRATION REQUEST",
								"response" : {
									"get_profile" : "Session Profile Captured",
									"get_notification" : "Notification Captured",
									"debit_float" : "No float amount to debit",
									"member_registration" : "Successful",
									"sale_order" : "Sale Order",
									"send_notification" : "Notification Sent. Please check EMAIL"
								},
								"description" : "PROCESSED|Response Status: Approved or completed successfully|Overall Status: Approved or completed successfully"
							}
						], [{
								"index" : 320296,
								"name" : "System User",
								"date_time" : "11 Sep 2016 12:09:23 PM EAT +0300",
								"request" : {
									"passport_id_number" : "22734592",
									"msisdn" : "+254724618425",
									"full_names" : "wanzaa mwende ndusya",
									"email" : "lucywanja.com@gmail.com",
									"item" : "AMKA M-Chaama Membership"
								},
								"type" : "MEMBERSHIP REGISTRATION REQUEST",
								"response" : {
									"get_profile" : "Session Profile Captured",
									"get_notification" : "Notification Captured",
									"debit_float" : "No float amount to debit",
									"member_registration" : "Successful",
									"sale_order" : "Sale Order",
									"send_notification" : "Notification Sent. Please check EMAIL"
								},
								"description" : "PROCESSED|Response Status: Approved or completed successfully|Overall Status: Approved or completed successfully"
							}
						], [{
								"index" : 320293,
								"name" : "System User",
								"date_time" : "11 Sep 2016 08:43:27 AM EAT +0300",
								"request" : {
									"passport_id_number" : "12772843",
									"msisdn" : "+254722177609",
									"full_names" : "jossie billey musya ",
									"email" : "lamukyuso@yahoo.com",
									"item" : "AMKA M-Chaama Membership"
								},
								"type" : "MEMBERSHIP REGISTRATION REQUEST",
								"response" : {
									"get_profile" : "Session Profile Captured",
									"get_notification" : "Notification Captured",
									"debit_float" : "No float amount to debit",
									"member_registration" : "Successful",
									"sale_order" : "Sale Order",
									"send_notification" : "Notification Sent. Please check EMAIL"
								},
								"description" : "PROCESSED|Response Status: Approved or completed successfully|Overall Status: Approved or completed successfully"
							}
						], [{
								"index" : 320292,
								"name" : "System User",
								"date_time" : "11 Sep 2016 08:43:15 AM EAT +0300",
								"request" : {
									"passport_id_number" : "12772843",
									"msisdn" : "+254722177609",
									"full_names" : "jossie billey musya ",
									"email" : "lamukyuso@yahoo.com",
									"item" : "AMKA M-Chaama Membership"
								},
								"type" : "MEMBERSHIP REGISTRATION REQUEST",
								"response" : {
									"get_profile" : "Session Profile Captured",
									"get_notification" : "Notification Captured",
									"debit_float" : "No float amount to debit",
									"member_registration" : "Successful",
									"sale_order" : "Sale Order",
									"send_notification" : "Notification Sent. Please check EMAIL"
								},
								"description" : "PROCESSED|Response Status: Approved or completed successfully|Overall Status: Approved or completed successfully"
							}
						], [{
								"index" : 320290,
								"name" : "System User",
								"date_time" : "11 Sep 2016 08:43:11 AM EAT +0300",
								"request" : {
									"passport_id_number" : "12772843",
									"msisdn" : "+254722177609",
									"full_names" : "jossie billey musya ",
									"email" : "lamukyuso@yahoo.com",
									"item" : "AMKA M-Chaama Membership"
								},
								"type" : "MEMBERSHIP REGISTRATION REQUEST",
								"response" : {
									"get_profile" : "Session Profile Captured",
									"get_notification" : "Notification Captured",
									"debit_float" : "No float amount to debit",
									"member_registration" : "Successful",
									"sale_order" : "Sale Order",
									"send_notification" : "Notification Sent. Please check EMAIL"
								},
								"description" : "PROCESSED|Response Status: Approved or completed successfully|Overall Status: Approved or completed successfully"
							}
						], [{
								"index" : 320291,
								"name" : "System User",
								"date_time" : "11 Sep 2016 08:43:10 AM EAT +0300",
								"request" : {
									"passport_id_number" : "12772843",
									"msisdn" : "+254722177609",
									"full_names" : "jossie billey musya ",
									"email" : "lamukyuso@yahoo.com",
									"item" : "AMKA M-Chaama Membership"
								},
								"type" : "MEMBERSHIP REGISTRATION REQUEST",
								"response" : {
									"get_profile" : "Session Profile Captured",
									"get_notification" : "Notification Captured",
									"debit_float" : "No float amount to debit",
									"member_registration" : "Successful",
									"sale_order" : "Sale Order",
									"send_notification" : "Notification Sent. Please check EMAIL"
								},
								"description" : "PROCESSED|Response Status: Approved or completed successfully|Overall Status: Approved or completed successfully"
							}
						], [{
								"index" : 320285,
								"name" : "System User",
								"date_time" : "11 Sep 2016 08:11:44 AM EAT +0300",
								"request" : {
									"passport_id_number" : "22734592",
									"msisdn" : "+254724618425",
									"full_names" : "wanzaa mwende ndusya",
									"email" : "lucywanja.com@gmail.com",
									"item" : "AMKA M-Chaama Membership"
								},
								"type" : "MEMBERSHIP REGISTRATION REQUEST",
								"response" : {
									"get_profile" : "Session Profile Captured",
									"get_notification" : "Notification Captured",
									"debit_float" : "No float amount to debit",
									"member_registration" : "Successful",
									"sale_order" : "Sale Order",
									"send_notification" : "Notification Sent. Please check EMAIL"
								},
								"description" : "PROCESSED|Response Status: Approved or completed successfully|Overall Status: Approved or completed successfully"
							}
						], [{
								"index" : 320286,
								"name" : "System User",
								"date_time" : "11 Sep 2016 08:11:38 AM EAT +0300",
								"request" : {
									"passport_id_number" : "22734592",
									"msisdn" : "+254724618425",
									"full_names" : "wanzaa mwende ndusya",
									"email" : "lucywanja.com@gmail.com",
									"item" : "AMKA M-Chaama Membership"
								},
								"type" : "MEMBERSHIP REGISTRATION REQUEST",
								"response" : {
									"get_profile" : "Session Profile Captured",
									"get_notification" : "Notification Captured",
									"debit_float" : "No float amount to debit",
									"member_registration" : "Successful",
									"sale_order" : "Sale Order",
									"send_notification" : "Notification Sent. Please check EMAIL"
								},
								"description" : "PROCESSED|Response Status: Approved or completed successfully|Overall Status: Approved or completed successfully"
							}
						], [{
								"index" : 320284,
								"name" : "System User",
								"date_time" : "10 Sep 2016 08:58:04 PM EAT +0300",
								"request" : {
									"full_names" : "Wilson Mwangi maina",
									"accesspoint" : "808",
									"msisdn" : "+254726996551",
									"access_point" : "*808*77#",
									"passport_id_number" : "22103031",
									"item" : "AMKA M-Chaama Membership",
									"payment_method" : "M-PESA"
								},
								"type" : "MEMBERSHIP REGISTRATION REQUEST",
								"response" : {
									"get_profile" : "Session Profile Captured",
									"member_registration" : "Successful",
									"remit" : "Remit Successful",
									"sale_order" : "Sale Order"
								},
								"description" : "PROCESSED|Response Status: Approved or completed successfully|Overall Status: Approved or completed successfully"
							}
						], [{
								"index" : 320283,
								"name" : "SAMSON MBURU",
								"date_time" : "10 Sep 2016 08:12:13 PM EAT +0300",
								"request" : {
									"msisdn" : "+254722491320",
									"access_point" : "*808*77#",
									"accesspoint" : "808"
								},
								"type" : "CONFIRM ONE TIME PIN",
								"response" : {
									"set_profile_pin" : "New PIN isSet",
									"validate_one_time_pin" : "Valid One Time PIN",
									"get_profile" : "Session Profile Captured"
								},
								"description" : "PROCESSED|Response Status: Approved or completed successfully|Overall Status: Approved or completed successfully"
							}
						]],
					"cols" : [],
					"min_id" : 320283,
					"groups" : ["ADD NOTIFICATION CONTACT", "CONFIRM ONE TIME PIN", "INBOX REPLY", "INVESTMENT", "INVESTOR ENROLLMENT", "LOAN LIMIT", "LOAN REPAYMENT", "LOAN REQUEST", "MEMBERSHIP REGISTRATION", "MEMBERSHIP REGISTRATION REQUEST", "NOTIFICATION PRODUCT SEND", "ORDER CHECKOUT", "RESEND ONE TIME PIN", "SALE", "UPLOAD FILE"],
					"max_id" : 320356
				}
			},
			"CHID" : "1"
		},
		"notification_product" : {
			"gateway_host" : "mchaama.com",
			"response_status" : "00",
			"data_name" : "notification_product",
			"session_id" : "Njk2YTdhNmY3MTc0NzA3OTZjMzA3ODYxNjU2NDYyNzY3NDZkNzgzMTc1Njg2NjY0Nzc3OTMwMzM3ODc2MzAzNjc1NzQ2MjYzNmU2NTYyNmI2YzYzMzQzOTZlMzM2ZjY5NjQ2NTZlNmE2MTY4MzA3NTcwN2E2YzMxNmYzMTZhN2E2ZDMwNmU2ODZhNjc2ODZjNzQ2NjZkMmIwYTc2NjQ3MzY4NzY2Njc1Nzk2NjY1NmU2Nzc0NzMzMTczNzI3NzYyNjY3MDc0Mzk2MjZhNjk3YTMwNzI3ODMxNjE2Njc4MzAzNTc1NzQ3NjZmNzUzMjMxNzU2ZjYzMzkzNjcxNzQzNDMwNmQzMjZmNmY2NjZlNzA2NzZkNjk3YTM1NjU2ZDZlMzI3OTZiNzkzNzcyNjk3MTcyMGE3MjY3NjI3NTY1NjQ2NjcwNjU3OTM0Mzg2NDc1Njg2Zjc1NjQ3ODY4NjY2ODcwNmE2ZDMxNjczMzc4NjMzNDM3NzM2NjZjNmY2MTY1MzU3MzZmNzMzNDcyNzk3OTYzNjgwYQ==",
			"chid" : "1",
			"lat" : "1.0",
			"lng" : "38.0",
			"ip_address" : "197.237.248.163",
			"response" : {
				"data_source" : {
					"rows" : [{
							"count" : "32 Subscriber(s)",
							"index" : 85,
							"name" : "AMKA BULK EMAIL",
							"description" : "AMKA BULK EMAIL (): AMKA BULK EMAIL"
						}, {
							"count" : "99 Subscriber(s)",
							"index" : 84,
							"name" : "40808_Bulk_AMKAGROUP",
							"description" : "40808_Bulk_AMKAGROUP_PILOT (): 40808_Bulk_AMKAGROUP_PILOT"
						}, {
							"count" : "2130 Subscriber(s)",
							"index" : 64,
							"name" : "40808_Bulk_AMKAGROUP",
							"description" : "40808_Bulk_AMKAGROUP (): 40808_Bulk_AMKAGROUP"
						}, {
							"count" : "2896 Subscriber(s)",
							"index" : 74,
							"name" : "40808_Bulk_AMKAGROUP",
							"description" : "40808_Bulk_AMKAGROUP_NEW (): 40808_Bulk_AMKAGROUP_NEW"
						}
					],
					"data" : [[{
								"count" : "99 Subscriber(s)",
								"index" : 84,
								"name" : "40808_Bulk_AMKAGROUP",
								"description" : "40808_Bulk_AMKAGROUP_PILOT (): 40808_Bulk_AMKAGROUP_PILOT"
							}, {
								"count" : "2130 Subscriber(s)",
								"index" : 64,
								"name" : "40808_Bulk_AMKAGROUP",
								"description" : "40808_Bulk_AMKAGROUP (): 40808_Bulk_AMKAGROUP"
							}, {
								"count" : "2896 Subscriber(s)",
								"index" : 74,
								"name" : "40808_Bulk_AMKAGROUP",
								"description" : "40808_Bulk_AMKAGROUP_NEW (): 40808_Bulk_AMKAGROUP_NEW"
							}
						], [{
								"count" : "32 Subscriber(s)",
								"index" : 85,
								"name" : "AMKA BULK EMAIL",
								"description" : "AMKA BULK EMAIL (): AMKA BULK EMAIL"
							}
						]],
					"cols" : [],
					"min_id" : 0,
					"groups" : ["40808_Bulk_AMKAGROUP", "AMKA BULK EMAIL"],
					"max_id" : 0
				}
			},
			"CHID" : "1"
		},
		"inbound" : {
			"gateway_host" : "mchaama.com",
			"response_status" : "00",
			"data_name" : "inbound",
			"session_id" : "Njk2YTdhNmY3MTc0NzA3OTZjMzA3ODYxNjU2NDYyNzY3NDZkNzgzMTc1Njg2NjY0Nzc3OTMwMzM3ODc2MzAzNjc1NzQ2MjYzNmU2NTYyNmI2YzYzMzQzOTZlMzM2ZjY5NjQ2NTZlNmE2MTY4MzA3NTcwN2E2YzMxNmYzMTZhN2E2ZDMwNmU2ODZhNjc2ODZjNzQ2NjZkMmIwYTc2NjQ3MzY4NzY2Njc1Nzk2NjY1NmU2Nzc0NzMzMTczNzI3NzYyNjY3MDc0Mzk2MjZhNjk3YTMwNzI3ODMxNjE2Njc4MzAzNTc1NzQ3NjZmNzUzMjMxNzU2ZjYzMzkzNjcxNzQzNDMwNmQzMjZmNmY2NjZlNzA2NzZkNjk3YTM1NjU2ZDZlMzI3OTZiNzkzNzcyNjk3MTcyMGE3MjY3NjI3NTY1NjQ2NjcwNjU3OTM0Mzg2NDc1Njg2Zjc1NjQ3ODY4NjY2ODcwNmE2ZDMxNjczMzc4NjMzNDM3NzM2NjZjNmY2MTY1MzU3MzZmNzMzNDcyNzk3OTYzNjgwYQ==",
			"chid" : "1",
			"lat" : "1.0",
			"lng" : "38.0",
			"ip_address" : "197.237.248.163",
			"response" : {
				"data_source" : {
					"rows" : [],
					"data" : [],
					"cols" : [],
					"min_id" : 0,
					"groups" : [],
					"max_id" : 0
				}
			},
			"CHID" : "1"
		},
		"notification_product_id" : {
			"gateway_host" : "mchaama.com",
			"response_status" : "00",
			"data_name" : "notification_product_id",
			"session_id" : "Njk2YTdhNmY3MTc0NzA3OTZjMzA3ODYxNjU2NDYyNzY3NDZkNzgzMTc1Njg2NjY0Nzc3OTMwMzM3ODc2MzAzNjc1NzQ2MjYzNmU2NTYyNmI2YzYzMzQzOTZlMzM2ZjY5NjQ2NTZlNmE2MTY4MzA3NTcwN2E2YzMxNmYzMTZhN2E2ZDMwNmU2ODZhNjc2ODZjNzQ2NjZkMmIwYTc2NjQ3MzY4NzY2Njc1Nzk2NjY1NmU2Nzc0NzMzMTczNzI3NzYyNjY3MDc0Mzk2MjZhNjk3YTMwNzI3ODMxNjE2Njc4MzAzNTc1NzQ3NjZmNzUzMjMxNzU2ZjYzMzkzNjcxNzQzNDMwNmQzMjZmNmY2NjZlNzA2NzZkNjk3YTM1NjU2ZDZlMzI3OTZiNzkzNzcyNjk3MTcyMGE3MjY3NjI3NTY1NjQ2NjcwNjU3OTM0Mzg2NDc1Njg2Zjc1NjQ3ODY4NjY2ODcwNmE2ZDMxNjczMzc4NjMzNDM3NzM2NjZjNmY2MTY1MzU3MzZmNzMzNDcyNzk3OTYzNjgwYQ==",
			"chid" : "1",
			"lat" : "1.0",
			"lng" : "38.0",
			"ip_address" : "197.237.248.163",
			"response" : {
				"data_source" : {
					"rows" : [[85, "AMKA BULK EMAIL"], [84, "40808_Bulk_AMKAGROUP_PILOT"], [64, "40808_Bulk_AMKAGROUP"], [74, "40808_Bulk_AMKAGROUP_NEW"]],
					"data" : [],
					"cols" : [],
					"min_id" : 0,
					"groups" : [],
					"max_id" : 0
				}
			},
			"CHID" : "1"
		},
		"MSISDN" : {
			"gateway_host" : "mchaama.com",
			"response_status" : "00",
			"data_name" : "MSISDN",
			"session_id" : "Njk2YTdhNmY3MTc0NzA3OTZjMzA3ODYxNjU2NDYyNzY3NDZkNzgzMTc1Njg2NjY0Nzc3OTMwMzM3ODc2MzAzNjc1NzQ2MjYzNmU2NTYyNmI2YzYzMzQzOTZlMzM2ZjY5NjQ2NTZlNmE2MTY4MzA3NTcwN2E2YzMxNmYzMTZhN2E2ZDMwNmU2ODZhNjc2ODZjNzQ2NjZkMmIwYTc2NjQ3MzY4NzY2Njc1Nzk2NjY1NmU2Nzc0NzMzMTczNzI3NzYyNjY3MDc0Mzk2MjZhNjk3YTMwNzI3ODMxNjE2Njc4MzAzNTc1NzQ3NjZmNzUzMjMxNzU2ZjYzMzkzNjcxNzQzNDMwNmQzMjZmNmY2NjZlNzA2NzZkNjk3YTM1NjU2ZDZlMzI3OTZiNzkzNzcyNjk3MTcyMGE3MjY3NjI3NTY1NjQ2NjcwNjU3OTM0Mzg2NDc1Njg2Zjc1NjQ3ODY4NjY2ODcwNmE2ZDMxNjczMzc4NjMzNDM3NzM2NjZjNmY2MTY1MzU3MzZmNzMzNDcyNzk3OTYzNjgwYQ==",
			"chid" : "1",
			"lat" : "1.0",
			"lng" : "38.0",
			"ip_address" : "197.237.248.163",
			"response" : {
				"data_source" : {
					"rows" : [["213", "Algeria"], ["1", "Antigua and Barbuda"], ["994", "Azerbaijan"], ["685", "American Samoa"], ["500", "South Georgia South Sandwich Islands"], ["244", "Angola"], ["54", "Argentina"], ["61", "Australia"], ["245", "Guinea-Bissau"], ["675", "Papua New Guinea"], ["1", "Bahamas"], ["880", "Bangladesh"], ["1", "Barbados"], ["1", "Bermuda"], ["673", "Brunei Darussalam"], ["855", "Cambodia"], ["1", "Canada"], ["350", "Gibraltar"], ["358", "\u00c3\u0085land Islands"], ["57", "Colombia"], ["269", "Comoros"], ["44", "Isle of Man"], ["421", "Slovakia"], ["591", "Bolivia"], ["95", "Burma"], ["55", "Brazil"], ["359", "Bulgaria"], ["352", "Luxembourg"], ["677", "Solomon Islands"], ["509", "Haiti"], ["242", "Congo"], ["243", "Democratic Republic of the Congo"], ["86", "China"], ["93", "Afghanistan"], ["238", "Cape Verde"], ["377", "Monaco"], ["237", "Cameroon"], ["1", "Cayman Islands"], ["235", "Chad"], ["56", "Chile"], ["236", "Central African Republic"], ["53", "Cuba"], ["357", "Cyprus"], ["45", "Denmark"], ["1", "Dominica"], ["240", "Equatorial Guinea"], ["291", "Eritrea"], ["372", "Estonia"], ["593", "Ecuador"], ["20", "Egypt"], ["353", "Ireland"], ["43", "Austria"], ["420", "Czech Republic"], ["251", "Ethiopia"], ["962", "Jordan"], ["679", "Fiji"], ["358", "Finland"], ["686", "Kiribati"], ["1", "Trinidad and Tobago"], ["992", "Tajikistan"], ["66", "Thailand"], ["690", "Tokelau"], [null, "Micronesia, Federated States of"], ["856", "Lao People's Democratic Republic"], ["689", "French Polynesia"], ["7", "Kazakhstan"], ["961", "Lebanon"], ["49", "Germany"], ["233", "Ghana"], ["30", "Greece"], ["299", "Greenland"], ["1", "Grenada"], ["1", "Guam"], ["224", "Guinea"], ["375", "Belarus"], ["592", "Guyana"], ["371", "Latvia"], ["370", "Lithuania"], ["970", "Palestine"], ["385", "Croatia"], ["504", "Honduras"], ["36", "Hungary"], ["354", "Iceland"], ["91", "India"], ["98", "Iran (Islamic Republic of)"], ["976", "Mongolia"], [null, "Cote d'Ivoire"], ["39", "Italy"], ["1", "Jamaica"], ["81", "Japan"], ["996", "Kyrgyzstan"], ["1", "Montserrat"], ["261", "Madagascar"], ["596", "Martinique"], ["265", "Malawi"], ["60", "Malaysia"], ["960", "Maldives"], ["223", "Mali"], ["356", "Malta"], ["222", "Mauritania"], ["230", "Mauritius"], ["52", "Mexico"], ["212", "Morocco"], ["258", "Mozambique"], ["687", "New Caledonia"], ["227", "Niger"], ["683", "Niue"], ["968", "Oman"], ["852", "Hong Kong"], ["1", "United States Minor Outlying Islands"], ["674", "Nauru"], ["977", "Nepal"], ["31", "Netherlands"], ["234", "Nigeria"], ["47", "Norway"], ["678", "Vanuatu"], [null, "Bouvet Island"], [null, "French Southern and Antarctic Lands"], [null, "Heard Island and McDonald Islands"], ["672", "Antarctica"], ["61", "Christmas Island"], ["61", "Cocos (Keeling) Islands"], ["672", "Norfolk Island"], ["351", "Portugal"], ["505", "Nicaragua"], ["92", "Pakistan"], ["507", "Panama"], ["595", "Paraguay"], ["51", "Peru"], ["48", "Poland"], ["262", "Reunion"], ["7", "Russia"], ["974", "Qatar"], ["40", "Romania"], ["63", "Philippines"], ["1", "Saint Kitts and Nevis"], ["966", "Saudi Arabia"], ["248", "Seychelles"], ["27", "South Africa"], ["221", "Senegal"], ["65", "Singapore"], ["252", "Somalia"], ["34", "Spain"], ["249", "Sudan"], ["46", "Sweden"], ["41", "Switzerland"], ["255", "United Republic of Tanzania"], ["993", "Turkmenistan"], ["688", "Tuvalu"], ["239", "Sao Tome and Principe"], ["216", "Tunisia"], ["90", "Turkey"], ["84", "Vietnam"], ["44", "United Kingdom"], ["1", "United States"], ["380", "Ukraine"], ["998", "Uzbekistan"], ["58", "Venezuela"], ["685", "Samoa"], ["62", "Indonesia"], ["264", "Namibia"], ["268", "Swaziland"], ["681", "Wallis and Futuna Islands"], ["967", "Yemen"], ["260", "Zambia"], ["263", "Zimbabwe"], ["599", "Netherlands Antilles"], [null, "Timor-Leste"], ["64", "Pitcairn Islands"], ["680", "Palau"], ["290", "Saint Helena"], ["590", "Saint Barthelemy"], ["590", "Saint Martin"], ["44", "Jersey"], ["381", "Serbia"], ["47", "Svalbard"], ["39", "Holy See (Vatican City)"], ["850", "North Korea"], ["82", "South Korea"], ["355", "Albania"], ["376", "Andorra"], ["1", "Anguilla"], ["374", "Armenia"], ["297", "Aruba"], ["973", "Bahrain"], ["501", "Belize"], ["32", "Belgium"], ["229", "Benin"], ["975", "Bhutan"], ["387", "Bosnia and Herzegovina"], ["267", "Botswana"], ["246", "British Indian Ocean Territory"], ["226", "Burkina Faso"], ["257", "Burundi"], ["682", "Cook Islands"], ["253", "Djibouti"], ["506", "Costa Rica"], ["1", "Dominican Republic"], ["503", "El Salvador"], ["500", "Falkland Islands (Malvinas)"], ["298", "Faroe Islands"], ["33", "France"], ["965", "Kuwait"], ["594", "French Guiana"], ["241", "Gabon"], ["220", "Gambia"], ["266", "Lesotho"], ["995", "Georgia"], ["590", "Guadeloupe"], ["502", "Guatemala"], ["44", "Guernsey"], ["964", "Iraq"], ["262", "Mayotte"], ["972", "Israel"], ["378", "San Marino"], ["254", "Kenya"], ["231", "Liberia"], ["218", "Libyan Arab Jamahiriya"], ["423", "Liechtenstein"], ["853", "Macau"], ["389", "The former Yugoslav Republic of Macedonia"], ["692", "Marshall Islands"], ["373", "Republic of Moldova"], ["382", "Montenegro"], ["64", "New Zealand"], ["1", "United States Virgin Islands"], ["1", "Northern Mariana Islands"], ["1", "Puerto Rico"], ["250", "Rwanda"], ["1", "Saint Lucia"], ["508", "Saint Pierre and Miquelon"], ["1", "Saint Vincent and the Grenadines"], ["232", "Sierra Leone"], ["386", "Slovenia"], ["94", "Sri Lanka"], ["597", "Suriname"], ["963", "Syrian Arab Republic"], ["886", "Taiwan"], ["228", "Togo"], ["676", "Tonga"], ["1", "Turks and Caicos Islands"], ["256", "Uganda"], ["971", "United Arab Emirates"], ["598", "Uruguay"], ["1", "British Virgin Islands"], ["212", "Western Sahara"]],
					"data" : [],
					"cols" : [],
					"min_id" : 0,
					"groups" : [],
					"max_id" : 0
				}
			},
			"CHID" : "1"
		},
		"outbound" : {
			"gateway_host" : "mchaama.com",
			"response_status" : "00",
			"data_name" : "outbound",
			"session_id" : "Njk2YTdhNmY3MTc0NzA3OTZjMzA3ODYxNjU2NDYyNzY3NDZkNzgzMTc1Njg2NjY0Nzc3OTMwMzM3ODc2MzAzNjc1NzQ2MjYzNmU2NTYyNmI2YzYzMzQzOTZlMzM2ZjY5NjQ2NTZlNmE2MTY4MzA3NTcwN2E2YzMxNmYzMTZhN2E2ZDMwNmU2ODZhNjc2ODZjNzQ2NjZkMmIwYTc2NjQ3MzY4NzY2Njc1Nzk2NjY1NmU2Nzc0NzMzMTczNzI3NzYyNjY3MDc0Mzk2MjZhNjk3YTMwNzI3ODMxNjE2Njc4MzAzNTc1NzQ3NjZmNzUzMjMxNzU2ZjYzMzkzNjcxNzQzNDMwNmQzMjZmNmY2NjZlNzA2NzZkNjk3YTM1NjU2ZDZlMzI3OTZiNzkzNzcyNjk3MTcyMGE3MjY3NjI3NTY1NjQ2NjcwNjU3OTM0Mzg2NDc1Njg2Zjc1NjQ3ODY4NjY2ODcwNmE2ZDMxNjczMzc4NjMzNDM3NzM2NjZjNmY2MTY1MzU3MzZmNzMzNDcyNzk3OTYzNjgwYQ==",
			"chid" : "1",
			"lat" : "1.0",
			"lng" : "38.0",
			"ip_address" : "197.237.248.163",
			"response" : {
				"data_source" : {
					"rows" : [{
							"index" : 454522,
							"state" : "PROCESSING",
							"date_time" : "15 Sep 2016 02:38:03 PM EAT +0300",
							"description" : "N... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 454077,
							"state" : "PROCESSING",
							"date_time" : "15 Sep 2016 02:37:01 PM EAT +0300",
							"description" : "N... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 454076,
							"state" : "PROCESSING",
							"date_time" : "15 Sep 2016 02:36:59 PM EAT +0300",
							"description" : "N... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 450611,
							"state" : "PROCESSING",
							"date_time" : "15 Sep 2016 12:00:50 PM EAT +0300",
							"description" : "N... : Now Invest,Borrow or Lend 24Hrs/7dys on M-Chaama & grow via High Dividends! Lenders Pie now@13,500 upto 10.10.16.Sacco Mradi-Loans Available",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 450166,
							"state" : "PROCESSING",
							"date_time" : "15 Sep 2016 12:00:02 PM EAT +0300",
							"description" : "N... : Now Invest,Borrow or Lend 24Hrs/7dys on M-Chaama & grow via High Dividends! Lenders Pie now@13,500 upto 10.10.16.Sacco Mradi-Loans Available",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 450165,
							"state" : "PROCESSING",
							"date_time" : "15 Sep 2016 12:00:00 PM EAT +0300",
							"description" : "N... : Now Invest,Borrow or Lend 24Hrs/7dys on M-Chaama & grow via High Dividends! Lenders Pie now@13,500 upto 10.10.16.Sacco Mradi-Loans Available",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 455879,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 11:34:11 PM EAT +0300",
							"description" : "+254717203... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 454236,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 11:12:22 PM EAT +0300",
							"description" : "+254725274... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 454953,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 10:07:37 PM EAT +0300",
							"description" : "+254723535... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 455997,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 10:02:48 PM EAT +0300",
							"description" : "+254711909... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 455033,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 09:55:37 PM EAT +0300",
							"description" : "+254728776... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 454880,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 09:41:17 PM EAT +0300",
							"description" : "+254726634... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 454829,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 09:28:01 PM EAT +0300",
							"description" : "+254727062... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 456350,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 08:48:31 PM EAT +0300",
							"description" : "+254707309... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 455734,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 08:44:22 PM EAT +0300",
							"description" : "+254722269... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 456595,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 08:43:26 PM EAT +0300",
							"description" : "+254713675... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 454340,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 08:42:41 PM EAT +0300",
							"description" : "+254727478... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 454087,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 08:36:15 PM EAT +0300",
							"description" : "+254720349... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 454205,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 08:25:48 PM EAT +0300",
							"description" : "+254717311... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 454870,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 08:17:38 PM EAT +0300",
							"description" : "+254726704... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 456495,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 08:06:22 PM EAT +0300",
							"description" : "+254726073... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 454237,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 07:57:52 PM EAT +0300",
							"description" : "+254720408... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 454603,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 07:57:29 PM EAT +0300",
							"description" : "+254790115... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 456685,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 07:56:51 PM EAT +0300",
							"description" : "+254712195... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 455939,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 07:52:18 PM EAT +0300",
							"description" : "+254715715... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 455924,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 07:51:36 PM EAT +0300",
							"description" : "+254716118... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 456011,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 07:47:14 PM EAT +0300",
							"description" : "+254711642... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 454924,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 07:44:22 PM EAT +0300",
							"description" : "+254726210... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 455909,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 07:44:02 PM EAT +0300",
							"description" : "+254716467... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 455822,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 07:43:36 PM EAT +0300",
							"description" : "+254721735... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 453862,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 07:43:19 PM EAT +0300",
							"description" : "+254716269... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 455148,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 07:39:10 PM EAT +0300",
							"description" : "+254722754... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 456531,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 07:38:11 PM EAT +0300",
							"description" : "+254725673... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 455604,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 07:26:52 PM EAT +0300",
							"description" : "+254720122... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 454220,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 07:20:18 PM EAT +0300",
							"description" : "+254720432... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 453893,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 07:16:38 PM EAT +0300",
							"description" : "+254727244... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 454547,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 07:16:27 PM EAT +0300",
							"description" : "+254713683... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 456538,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 07:10:30 PM EAT +0300",
							"description" : "+254725640... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 456210,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 07:07:46 PM EAT +0300",
							"description" : "+254722448... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 455067,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 07:06:04 PM EAT +0300",
							"description" : "+254728338... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 454110,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 06:55:29 PM EAT +0300",
							"description" : "+254715308... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 456705,
							"state" : "FAILED",
							"date_time" : "14 Sep 2016 06:47:11 PM EAT +0300",
							"description" : "+254711989... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 456571,
							"state" : "FAILED",
							"date_time" : "14 Sep 2016 06:46:37 PM EAT +0300",
							"description" : "+254714356... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 456510,
							"state" : "FAILED",
							"date_time" : "14 Sep 2016 06:46:35 PM EAT +0300",
							"description" : "+254725855... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 456323,
							"state" : "FAILED",
							"date_time" : "14 Sep 2016 06:45:54 PM EAT +0300",
							"description" : "+254708208... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 456122,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 06:45:44 PM EAT +0300",
							"description" : "+254725301... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 455628,
							"state" : "FAILED",
							"date_time" : "14 Sep 2016 06:43:46 PM EAT +0300",
							"description" : "+254719391... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 455481,
							"state" : "FAILED",
							"date_time" : "14 Sep 2016 06:43:46 PM EAT +0300",
							"description" : "+254720949... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 455654,
							"state" : "FAILED",
							"date_time" : "14 Sep 2016 06:43:42 PM EAT +0300",
							"description" : "+254718561... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 456043,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 06:40:54 PM EAT +0300",
							"description" : "+254711108... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 454681,
							"state" : "FAILED",
							"date_time" : "14 Sep 2016 06:40:01 PM EAT +0300",
							"description" : "+254729522... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 454605,
							"state" : "SENT",
							"date_time" : "14 Sep 2016 06:39:57 PM EAT +0300",
							"description" : "+254775818... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 454638,
							"state" : "FAILED",
							"date_time" : "14 Sep 2016 06:39:56 PM EAT +0300",
							"description" : "+254736871... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 454300,
							"state" : "FAILED",
							"date_time" : "14 Sep 2016 06:39:53 PM EAT +0300",
							"description" : "+254734735... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 453908,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 06:39:40 PM EAT +0300",
							"description" : "+254722992... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 454371,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 06:39:33 PM EAT +0300",
							"description" : "+254724815... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 454517,
							"state" : "FAILED",
							"date_time" : "14 Sep 2016 06:39:26 PM EAT +0300",
							"description" : "+254719754... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 454114,
							"state" : "FAILED",
							"date_time" : "14 Sep 2016 06:38:22 PM EAT +0300",
							"description" : "+254725095... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 454160,
							"state" : "FAILED",
							"date_time" : "14 Sep 2016 06:37:51 PM EAT +0300",
							"description" : "+254733770... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 454036,
							"state" : "FAILED",
							"date_time" : "14 Sep 2016 06:37:24 PM EAT +0300",
							"description" : "+2547362634... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 453980,
							"state" : "FAILED",
							"date_time" : "14 Sep 2016 06:37:11 PM EAT +0300",
							"description" : "+254729988... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 456321,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 06:36:08 PM EAT +0300",
							"description" : "+254708252... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 454204,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 06:35:13 PM EAT +0300",
							"description" : "+254717432... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 454226,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 06:34:58 PM EAT +0300",
							"description" : "+254713215... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 455638,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 06:33:46 PM EAT +0300",
							"description" : "+254719118... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 456026,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 06:33:42 PM EAT +0300",
							"description" : "+254711427... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 454628,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 06:28:57 PM EAT +0300",
							"description" : "+254728943... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 454577,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 06:25:17 PM EAT +0300",
							"description" : "+254706365... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 456628,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 06:23:13 PM EAT +0300",
							"description" : "+254712990... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 455201,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 06:11:25 PM EAT +0300",
							"description" : "+254721510... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 456046,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 06:11:06 PM EAT +0300",
							"description" : "+254710981... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 456394,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 06:10:02 PM EAT +0300",
							"description" : "+254705516... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 455125,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 06:09:55 PM EAT +0300",
							"description" : "+254722843... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 456618,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 06:09:46 PM EAT +0300",
							"description" : "+254713267... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 454343,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 06:09:22 PM EAT +0300",
							"description" : "+254727236... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 455060,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 06:03:06 PM EAT +0300",
							"description" : "+254728494... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 454303,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 05:52:36 PM EAT +0300",
							"description" : "+254724655... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 454109,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 05:46:40 PM EAT +0300",
							"description" : "+254714684... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 455331,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 05:45:46 PM EAT +0300",
							"description" : "+254724526... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 456723,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 05:44:17 PM EAT +0300",
							"description" : "+254700556... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 454472,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 05:41:21 PM EAT +0300",
							"description" : "+254723468... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 456356,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 05:37:18 PM EAT +0300",
							"description" : "+254707024... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 455673,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 05:35:27 PM EAT +0300",
							"description" : "+254717756... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 456711,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 05:34:43 PM EAT +0300",
							"description" : "+254701232... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 454570,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 05:24:30 PM EAT +0300",
							"description" : "+254708819... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 456479,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 05:22:18 PM EAT +0300",
							"description" : "+254701512... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 455232,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 05:18:11 PM EAT +0300",
							"description" : "+254721340... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 456500,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 05:14:13 PM EAT +0300",
							"description" : "+254725982... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 456473,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 05:11:10 PM EAT +0300",
							"description" : "+254701700... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 455557,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 05:09:26 PM EAT +0300",
							"description" : "+254720528... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 456584,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 05:06:32 PM EAT +0300",
							"description" : "+254714073... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 456045,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 05:03:45 PM EAT +0300",
							"description" : "+254711107... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 455190,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 04:57:42 PM EAT +0300",
							"description" : "+254722587... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 456254,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 04:57:41 PM EAT +0300",
							"description" : "+254720259... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 456461,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 04:52:32 PM EAT +0300",
							"description" : "+254702412... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 453940,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 04:52:26 PM EAT +0300",
							"description" : "+254713904... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 454660,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 04:49:42 PM EAT +0300",
							"description" : "+254729747... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 455062,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 04:48:47 PM EAT +0300",
							"description" : "+254728471... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 454481,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 04:48:44 PM EAT +0300",
							"description" : "+254722726... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 454339,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 04:48:44 PM EAT +0300",
							"description" : "+254727599... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 456638,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 04:48:29 PM EAT +0300",
							"description" : "+254712796... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 454239,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 04:47:46 PM EAT +0300",
							"description" : "+254720376... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 455304,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 04:45:31 PM EAT +0300",
							"description" : "+254724680... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 455413,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 04:44:35 PM EAT +0300",
							"description" : "+254723885... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 453934,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 04:44:15 PM EAT +0300",
							"description" : "+254737486... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 454567,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 04:41:48 PM EAT +0300",
							"description" : "+254711101... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 454630,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 04:40:38 PM EAT +0300",
							"description" : "+254728392... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 455783,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 04:40:29 PM EAT +0300",
							"description" : "+254721947... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 454271,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 04:40:16 PM EAT +0300",
							"description" : "+254720243... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 455922,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 04:39:50 PM EAT +0300",
							"description" : "+254716128... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 454424,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 04:39:03 PM EAT +0300",
							"description" : "+254787776... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 454421,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 04:38:46 PM EAT +0300",
							"description" : "+254733404... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 456001,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 04:38:30 PM EAT +0300",
							"description" : "+254711855... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 454143,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 04:37:47 PM EAT +0300",
							"description" : "+254733925... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 455597,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 04:37:12 PM EAT +0300",
							"description" : "+254720145... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 456342,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 04:36:47 PM EAT +0300",
							"description" : "+254707680... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 454291,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 04:34:15 PM EAT +0300",
							"description" : "+254724793... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 455100,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 04:31:37 PM EAT +0300",
							"description" : "+254722938... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 455129,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 04:29:43 PM EAT +0300",
							"description" : "+254722839... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 454345,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 04:29:35 PM EAT +0300",
							"description" : "+254727135... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 454589,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 04:29:14 PM EAT +0300",
							"description" : "+254702468... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 454378,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 04:24:11 PM EAT +0300",
							"description" : "+254724093... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 454217,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 04:18:16 PM EAT +0300",
							"description" : "+254707214... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 455524,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 04:18:11 PM EAT +0300",
							"description" : "+254720754... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 456587,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 04:16:43 PM EAT +0300",
							"description" : "+254714009... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 456003,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 04:16:27 PM EAT +0300",
							"description" : "+254711842... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 456403,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 04:16:19 PM EAT +0300",
							"description" : "+254705122... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 454833,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 04:14:21 PM EAT +0300",
							"description" : "+254726988... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 456195,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 04:09:39 PM EAT +0300",
							"description" : "+254722501... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 456022,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 04:02:08 PM EAT +0300",
							"description" : "+254711456... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 456444,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 04:01:10 PM EAT +0300",
							"description" : "+254702861... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 455899,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 04:00:11 PM EAT +0300",
							"description" : "+254716718... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 454069,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 03:53:37 PM EAT +0300",
							"description" : "+254721946... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 455643,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 03:52:36 PM EAT +0300",
							"description" : "+254718887... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 454683,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 03:49:46 PM EAT +0300",
							"description" : "+254729498... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 455917,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 03:49:42 PM EAT +0300",
							"description" : "+254716285... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 454346,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 03:48:41 PM EAT +0300",
							"description" : "+254727051... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 454046,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 03:47:27 PM EAT +0300",
							"description" : "+254726143... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 455187,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 03:45:48 PM EAT +0300",
							"description" : "+254722598... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 456065,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 03:44:58 PM EAT +0300",
							"description" : "+254710747... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 454990,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 03:43:46 PM EAT +0300",
							"description" : "+254723336... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 455231,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 03:42:10 PM EAT +0300",
							"description" : "+254721345... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 456051,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 03:41:34 PM EAT +0300",
							"description" : "+254710914... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 454387,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 03:40:57 PM EAT +0300",
							"description" : "+254723173... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 455412,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 03:40:07 PM EAT +0300",
							"description" : "+254723896... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 454151,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 03:39:52 PM EAT +0300",
							"description" : "+254728752... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 454473,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 03:39:33 PM EAT +0300",
							"description" : "+254723367... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 454449,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 03:38:03 PM EAT +0300",
							"description" : "+254724619... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 453853,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 03:37:58 PM EAT +0300",
							"description" : "+254721596... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 453927,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 03:37:27 PM EAT +0300",
							"description" : "+254707362... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 456629,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 03:36:42 PM EAT +0300",
							"description" : "+254712987... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 455234,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 03:36:22 PM EAT +0300",
							"description" : "+254721333... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 456423,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 03:36:07 PM EAT +0300",
							"description" : "+254704061... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 456100,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 03:35:33 PM EAT +0300",
							"description" : "+254725433... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 453870,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 03:35:29 PM EAT +0300",
							"description" : "+254713904... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 455230,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 03:35:09 PM EAT +0300",
							"description" : "+254721352... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 455729,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 03:34:51 PM EAT +0300",
							"description" : "+254722284... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 455972,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 03:34:42 PM EAT +0300",
							"description" : "+254714818... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 455951,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 03:34:37 PM EAT +0300",
							"description" : "+254715378... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 455890,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 03:34:35 PM EAT +0300",
							"description" : "+254716955... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 455740,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 03:33:57 PM EAT +0300",
							"description" : "+254722241... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 455499,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 03:32:56 PM EAT +0300",
							"description" : "+254720844... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 456198,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 03:32:32 PM EAT +0300",
							"description" : "+254722497... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 454027,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 03:32:12 PM EAT +0300",
							"description" : "+254720378... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 455001,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 03:32:04 PM EAT +0300",
							"description" : "+254723250... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 455158,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 03:31:56 PM EAT +0300",
							"description" : "+254722691... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 455028,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 03:31:27 PM EAT +0300",
							"description" : "+254728851... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 454869,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 03:30:48 PM EAT +0300",
							"description" : "+254726705... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 456126,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 03:29:37 PM EAT +0300",
							"description" : "+254725286... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 455163,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 03:29:21 PM EAT +0300",
							"description" : "+254722678... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 454430,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 03:29:18 PM EAT +0300",
							"description" : "+254706704... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 454562,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 03:29:17 PM EAT +0300",
							"description" : "+254711431... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 454428,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 03:29:17 PM EAT +0300",
							"description" : "+254729674... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 454139,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 03:28:52 PM EAT +0300",
							"description" : "+254724204... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 454581,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 03:28:31 PM EAT +0300",
							"description" : "+254704520... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 454195,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 03:28:01 PM EAT +0300",
							"description" : "+254727491... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 455896,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 03:27:39 PM EAT +0300",
							"description" : "+254716800... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 454056,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 03:27:17 PM EAT +0300",
							"description" : "+254721765... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 453867,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 03:26:53 PM EAT +0300",
							"description" : "+254722896... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 454796,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 03:26:47 PM EAT +0300",
							"description" : "+254727434... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 456153,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 03:26:09 PM EAT +0300",
							"description" : "+254725069... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 456107,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 03:25:14 PM EAT +0300",
							"description" : "+254725384... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 454126,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 03:24:30 PM EAT +0300",
							"description" : "+254724236... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 456583,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 03:24:13 PM EAT +0300",
							"description" : "+254714133... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 456389,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 03:23:40 PM EAT +0300",
							"description" : "+254705833... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 455573,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 03:23:11 PM EAT +0300",
							"description" : "+254720473... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 456662,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 03:22:15 PM EAT +0300",
							"description" : "+254712475... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 456724,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 03:21:37 PM EAT +0300",
							"description" : "+254700546... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 454624,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 03:21:21 PM EAT +0300",
							"description" : "+254729010... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 454519,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 03:21:15 PM EAT +0300",
							"description" : "+254718502... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 456404,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 03:21:05 PM EAT +0300",
							"description" : "+254704728... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 455925,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 03:20:46 PM EAT +0300",
							"description" : "+254716111... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 455865,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 03:20:32 PM EAT +0300",
							"description" : "+254721575... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 455944,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 03:19:56 PM EAT +0300",
							"description" : "+254715601... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 455544,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 03:19:45 PM EAT +0300",
							"description" : "+254720615... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 456415,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 03:19:42 PM EAT +0300",
							"description" : "+254704355... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 456247,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 03:19:12 PM EAT +0300",
							"description" : "+254720290... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 455895,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 03:18:49 PM EAT +0300",
							"description" : "+254716812... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 454801,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 03:17:17 PM EAT +0300",
							"description" : "+254727326... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 455203,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 03:17:05 PM EAT +0300",
							"description" : "+254721491... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 453869,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 03:16:55 PM EAT +0300",
							"description" : "+254725739... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 455030,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 03:16:36 PM EAT +0300",
							"description" : "+254728807... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 454691,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 03:15:44 PM EAT +0300",
							"description" : "+254729370... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 456657,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 03:15:23 PM EAT +0300",
							"description" : "+254712519... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 454921,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 03:15:01 PM EAT +0300",
							"description" : "+254726261... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 455099,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 03:14:56 PM EAT +0300",
							"description" : "+254722941... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 454432,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 03:14:50 PM EAT +0300",
							"description" : "+254720972... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 454725,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 03:14:46 PM EAT +0300",
							"description" : "+254728221... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 455325,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 03:14:38 PM EAT +0300",
							"description" : "+254724547... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 455678,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 03:14:13 PM EAT +0300",
							"description" : "+254717641... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 455034,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 03:13:29 PM EAT +0300",
							"description" : "+254728759... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 456340,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 03:13:14 PM EAT +0300",
							"description" : "+254707740... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 455571,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 03:13:08 PM EAT +0300",
							"description" : "+254720477... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 454429,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 03:13:00 PM EAT +0300",
							"description" : "+254711184... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 454178,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 03:12:51 PM EAT +0300",
							"description" : "+254719464... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 455134,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 03:12:51 PM EAT +0300",
							"description" : "+254722817... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 454741,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 03:12:42 PM EAT +0300",
							"description" : "+254727987... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 455566,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 03:12:41 PM EAT +0300",
							"description" : "+254720488... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 455235,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 03:12:26 PM EAT +0300",
							"description" : "+254721332... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 455684,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 03:12:11 PM EAT +0300",
							"description" : "+254722400... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 456509,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 03:12:02 PM EAT +0300",
							"description" : "+254725855... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 454427,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 03:12:01 PM EAT +0300",
							"description" : "+254710941... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 453998,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 03:11:56 PM EAT +0300",
							"description" : "+254722779... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 456133,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 03:11:51 PM EAT +0300",
							"description" : "+254725239... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 455258,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 03:11:49 PM EAT +0300",
							"description" : "+254721232... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 456671,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 03:11:48 PM EAT +0300",
							"description" : "+254712360... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 455900,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 03:11:31 PM EAT +0300",
							"description" : "+254716691... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 454815,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 03:11:16 PM EAT +0300",
							"description" : "+254727212... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 455247,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 03:10:21 PM EAT +0300",
							"description" : "+254721291... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 454104,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 03:10:17 PM EAT +0300",
							"description" : "+254718477... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 456211,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 03:10:10 PM EAT +0300",
							"description" : "+254722447... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 456729,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 03:10:06 PM EAT +0300",
							"description" : "+254700481... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 456716,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 03:09:50 PM EAT +0300",
							"description" : "+254700868... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 454426,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 03:09:49 PM EAT +0300",
							"description" : "+254724071... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 455619,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 03:09:40 PM EAT +0300",
							"description" : "+254719792... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 455799,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 03:09:39 PM EAT +0300",
							"description" : "+254721862... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 455589,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 03:09:31 PM EAT +0300",
							"description" : "+254720386... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 454166,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 03:09:21 PM EAT +0300",
							"description" : "+254705575... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 455255,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 03:09:13 PM EAT +0300",
							"description" : "+254721252... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 455694,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 03:09:09 PM EAT +0300",
							"description" : "+254722376... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 455093,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 03:09:08 PM EAT +0300",
							"description" : "+254722975... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 455251,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 03:09:07 PM EAT +0300",
							"description" : "+254721275... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 456382,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 03:09:06 PM EAT +0300",
							"description" : "+254706118... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 455026,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 03:09:01 PM EAT +0300",
							"description" : "+254728872... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 456092,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 03:09:01 PM EAT +0300",
							"description" : "+254725478... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 456593,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 03:08:56 PM EAT +0300",
							"description" : "+254713711... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 455323,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 03:08:50 PM EAT +0300",
							"description" : "+254724558... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 454233,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 03:08:45 PM EAT +0300",
							"description" : "+254722233... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 454052,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 03:08:37 PM EAT +0300",
							"description" : "+254726826... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 456062,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 03:08:34 PM EAT +0300",
							"description" : "+254710767... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 456735,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 03:08:17 PM EAT +0300",
							"description" : "+254700303... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 454262,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 03:08:10 PM EAT +0300",
							"description" : "+254724766... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 455477,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 03:08:09 PM EAT +0300",
							"description" : "+254720970... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 454070,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 03:08:06 PM EAT +0300",
							"description" : "+254722946... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 455963,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 03:08:06 PM EAT +0300",
							"description" : "+254715056... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 456096,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 03:08:04 PM EAT +0300",
							"description" : "+254725460... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 454433,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 03:08:00 PM EAT +0300",
							"description" : "+254700002... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 453960,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 03:07:59 PM EAT +0300",
							"description" : "+254720831... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 454083,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 03:07:57 PM EAT +0300",
							"description" : "+254726943... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 454120,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 03:07:48 PM EAT +0300",
							"description" : "+254718243... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 455574,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 03:07:48 PM EAT +0300",
							"description" : "+254720473... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 456414,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 03:07:48 PM EAT +0300",
							"description" : "+254704368... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 455386,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 03:07:46 PM EAT +0300",
							"description" : "+254724092... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 455893,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 03:07:16 PM EAT +0300",
							"description" : "+254716834... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 456661,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 03:07:04 PM EAT +0300",
							"description" : "+254712481... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 454534,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 03:06:28 PM EAT +0300",
							"description" : "+254715677... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 454882,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 03:05:17 PM EAT +0300",
							"description" : "+254726610... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 454168,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 03:05:13 PM EAT +0300",
							"description" : "+254702810... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 456470,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 03:05:10 PM EAT +0300",
							"description" : "+254702014... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 455559,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 03:04:38 PM EAT +0300",
							"description" : "+254720520... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 454520,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 03:04:26 PM EAT +0300",
							"description" : "+254718372... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 455327,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 03:03:49 PM EAT +0300",
							"description" : "+254724541... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 454853,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 03:02:52 PM EAT +0300",
							"description" : "+254726860... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 456121,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 03:02:36 PM EAT +0300",
							"description" : "+254725301... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 456734,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 03:01:59 PM EAT +0300",
							"description" : "+254700390... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 454986,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 03:01:40 PM EAT +0300",
							"description" : "+254723370... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 456560,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 03:01:18 PM EAT +0300",
							"description" : "+254714439... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 455914,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 03:01:02 PM EAT +0300",
							"description" : "+254716330... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 456438,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 03:01:01 PM EAT +0300",
							"description" : "+254703168... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 456372,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 03:00:50 PM EAT +0300",
							"description" : "+254706331... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 456706,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 03:00:04 PM EAT +0300",
							"description" : "+254711989... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 456060,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 02:59:56 PM EAT +0300",
							"description" : "+254710778... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 456042,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 02:59:56 PM EAT +0300",
							"description" : "+254711126... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 455959,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 02:59:47 PM EAT +0300",
							"description" : "+254715156... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 455949,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 02:59:45 PM EAT +0300",
							"description" : "+254715426... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 455744,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 02:59:38 PM EAT +0300",
							"description" : "+254722223... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 455803,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 02:59:21 PM EAT +0300",
							"description" : "+254721828... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 455835,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 02:59:11 PM EAT +0300",
							"description" : "+254721683... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 455737,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 02:58:58 PM EAT +0300",
							"description" : "+254722255... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 456419,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 02:58:57 PM EAT +0300",
							"description" : "+254704278... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 456141,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 02:58:44 PM EAT +0300",
							"description" : "+254725206... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 454187,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 02:58:37 PM EAT +0300",
							"description" : "+254726832... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 455377,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 02:57:36 PM EAT +0300",
							"description" : "+254724144... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 455289,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 02:57:28 PM EAT +0300",
							"description" : "+254724786... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 454705,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 02:57:26 PM EAT +0300",
							"description" : "+254729079... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 455415,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 02:57:25 PM EAT +0300",
							"description" : "+254723879... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 455345,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 02:57:21 PM EAT +0300",
							"description" : "+254724441... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 455249,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 02:57:02 PM EAT +0300",
							"description" : "+254721283... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 454402,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 02:57:00 PM EAT +0300",
							"description" : "+254710956... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 455020,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 02:56:43 PM EAT +0300",
							"description" : "+254723165... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 455164,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 02:56:35 PM EAT +0300",
							"description" : "+254722676... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 454837,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 02:56:26 PM EAT +0300",
							"description" : "+254726943... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 454938,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 02:56:20 PM EAT +0300",
							"description" : "+254723591... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 454099,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 02:56:15 PM EAT +0300",
							"description" : "+254713310... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 455610,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 02:55:34 PM EAT +0300",
							"description" : "+254720060... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 454816,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 02:55:33 PM EAT +0300",
							"description" : "+254727177... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 454373,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 02:55:32 PM EAT +0300",
							"description" : "+254724628... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 454885,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 02:55:32 PM EAT +0300",
							"description" : "+254726559... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 454854,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 02:55:31 PM EAT +0300",
							"description" : "+254726841... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 454840,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 02:55:30 PM EAT +0300",
							"description" : "+254726934... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 456564,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 02:55:24 PM EAT +0300",
							"description" : "+254714419... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 454664,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 02:54:59 PM EAT +0300",
							"description" : "+254729722... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 454650,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 02:54:55 PM EAT +0300",
							"description" : "+254729898... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 456652,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 02:54:30 PM EAT +0300",
							"description" : "+254712592... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 454505,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 02:54:22 PM EAT +0300",
							"description" : "+254721209... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 454068,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 02:54:18 PM EAT +0300",
							"description" : "+254704735... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 454487,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 02:54:12 PM EAT +0300",
							"description" : "+254722276... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 456255,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 02:53:24 PM EAT +0300",
							"description" : "+254720258... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 454199,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 02:53:21 PM EAT +0300",
							"description" : "+254716414... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 455383,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 02:53:04 PM EAT +0300",
							"description" : "+254724102... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 454038,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 02:52:30 PM EAT +0300",
							"description" : "+254722828... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 453971,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 02:52:18 PM EAT +0300",
							"description" : "+254726037... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 453954,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 02:52:13 PM EAT +0300",
							"description" : "+254722349... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 454518,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 02:52:03 PM EAT +0300",
							"description" : "+254719389... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 454476,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 02:50:51 PM EAT +0300",
							"description" : "+254723094... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 454231,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 02:50:13 PM EAT +0300",
							"description" : "+254722630... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 454943,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 02:49:53 PM EAT +0300",
							"description" : "+254723574... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 456388,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 02:49:42 PM EAT +0300",
							"description" : "+254705855... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 455155,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 02:48:55 PM EAT +0300",
							"description" : "+254722694... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 455563,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 02:48:50 PM EAT +0300",
							"description" : "+254720495... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 456196,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 02:48:15 PM EAT +0300",
							"description" : "+254722498... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 454366,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 02:48:14 PM EAT +0300",
							"description" : "+254725050... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 456401,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 02:48:01 PM EAT +0300",
							"description" : "+254705199... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 456653,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 02:47:59 PM EAT +0300",
							"description" : "+254712559... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 454913,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 02:47:46 PM EAT +0300",
							"description" : "+254726316... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 454615,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 02:47:18 PM EAT +0300",
							"description" : "+254729980... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 455958,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 02:47:14 PM EAT +0300",
							"description" : "+254715181... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 454587,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 02:47:07 PM EAT +0300",
							"description" : "+254703816... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 456559,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 02:47:07 PM EAT +0300",
							"description" : "+254714447... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 456741,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 02:47:07 PM EAT +0300",
							"description" : "+254700166... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 456726,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 02:47:06 PM EAT +0300",
							"description" : "+254700498... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 456742,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 02:47:06 PM EAT +0300",
							"description" : "+254700148... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 456667,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 02:47:06 PM EAT +0300",
							"description" : "+254712461... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 456733,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 02:47:05 PM EAT +0300",
							"description" : "+254700432... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 456745,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 02:47:05 PM EAT +0300",
							"description" : "+254700082... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 455644,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 02:47:05 PM EAT +0300",
							"description" : "+254718879... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 456715,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 02:47:04 PM EAT +0300",
							"description" : "+254700903... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 456737,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 02:47:04 PM EAT +0300",
							"description" : "+254700227... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 456748,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 02:47:04 PM EAT +0300",
							"description" : "+254700018... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 456698,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 02:47:04 PM EAT +0300",
							"description" : "+254712085... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 456739,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 02:47:04 PM EAT +0300",
							"description" : "+254700207... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 456747,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 02:47:04 PM EAT +0300",
							"description" : "+254700020... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 456728,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 02:47:03 PM EAT +0300",
							"description" : "+254700491... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 456744,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 02:47:03 PM EAT +0300",
							"description" : "+254700083... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 456736,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 02:47:03 PM EAT +0300",
							"description" : "+254700278... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 456725,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 02:47:03 PM EAT +0300",
							"description" : "+254700534... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 456746,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 02:47:03 PM EAT +0300",
							"description" : "+254700032... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 456722,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 02:47:03 PM EAT +0300",
							"description" : "+254700625... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 456689,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 02:47:03 PM EAT +0300",
							"description" : "+254712176... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 456738,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 02:47:03 PM EAT +0300",
							"description" : "+254700223... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 456721,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 02:47:02 PM EAT +0300",
							"description" : "+254700654... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 456719,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 02:47:02 PM EAT +0300",
							"description" : "+254700772... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 456699,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 02:47:02 PM EAT +0300",
							"description" : "+254712073... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 456694,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 02:47:02 PM EAT +0300",
							"description" : "+254712138... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 456730,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 02:47:02 PM EAT +0300",
							"description" : "+254700480... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 456732,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 02:47:02 PM EAT +0300",
							"description" : "+254700451... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 456675,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 02:47:01 PM EAT +0300",
							"description" : "+254712319... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 456712,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 02:47:01 PM EAT +0300",
							"description" : "+254701106... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 456665,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 02:47:01 PM EAT +0300",
							"description" : "+254712463... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 456718,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 02:47:00 PM EAT +0300",
							"description" : "+254700810... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 456682,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 02:47:00 PM EAT +0300",
							"description" : "+254712220... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 456702,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 02:47:00 PM EAT +0300",
							"description" : "+254712056... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 456687,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 02:47:00 PM EAT +0300",
							"description" : "+254712178... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 456673,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 02:47:00 PM EAT +0300",
							"description" : "+254712342... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 456686,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 02:47:00 PM EAT +0300",
							"description" : "+254712187... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 456713,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 02:47:00 PM EAT +0300",
							"description" : "+254701020... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 456683,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 02:47:00 PM EAT +0300",
							"description" : "+254712213... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 456678,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 02:47:00 PM EAT +0300",
							"description" : "+254712292... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 456731,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 02:47:00 PM EAT +0300",
							"description" : "+254700470... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 456740,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 02:47:00 PM EAT +0300",
							"description" : "+254700180... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 456664,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 02:46:59 PM EAT +0300",
							"description" : "+254712465... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 456743,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 02:46:59 PM EAT +0300",
							"description" : "+254700112... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 456688,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 02:46:59 PM EAT +0300",
							"description" : "+254712177... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 456727,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 02:46:59 PM EAT +0300",
							"description" : "+254700493... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 456656,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 02:46:59 PM EAT +0300",
							"description" : "+254712537... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 456709,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 02:46:58 PM EAT +0300",
							"description" : "+254701265... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 456647,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 02:46:58 PM EAT +0300",
							"description" : "+254712714... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 456708,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 02:46:58 PM EAT +0300",
							"description" : "+254701283... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 456696,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 02:46:58 PM EAT +0300",
							"description" : "+254712102... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 456670,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 02:46:58 PM EAT +0300",
							"description" : "+254712408... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 456680,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 02:46:58 PM EAT +0300",
							"description" : "+254712248... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 456677,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 02:46:58 PM EAT +0300",
							"description" : "+254712295... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 456693,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 02:46:58 PM EAT +0300",
							"description" : "+254712138... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 456619,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 02:46:58 PM EAT +0300",
							"description" : "+254713255... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 456690,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 02:46:58 PM EAT +0300",
							"description" : "+254712174... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 456717,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 02:46:58 PM EAT +0300",
							"description" : "+254700818... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 456701,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 02:46:57 PM EAT +0300",
							"description" : "+254712056... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 456695,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 02:46:57 PM EAT +0300",
							"description" : "+254712116... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 456416,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 02:46:57 PM EAT +0300",
							"description" : "+254704314... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 456681,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 02:46:57 PM EAT +0300",
							"description" : "+254712233... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 456697,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 02:46:57 PM EAT +0300",
							"description" : "+254712096... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 456676,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 02:46:56 PM EAT +0300",
							"description" : "+254712312... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 456691,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 02:46:56 PM EAT +0300",
							"description" : "+254712174... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 456672,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 02:46:56 PM EAT +0300",
							"description" : "+254712351... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 456649,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 02:46:56 PM EAT +0300",
							"description" : "+254712652... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 456666,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 02:46:56 PM EAT +0300",
							"description" : "+254712462... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 456703,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 02:46:56 PM EAT +0300",
							"description" : "+254712048... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 456684,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 02:46:56 PM EAT +0300",
							"description" : "+254712203... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 456692,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 02:46:56 PM EAT +0300",
							"description" : "+254712152... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 456663,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 02:46:55 PM EAT +0300",
							"description" : "+254712475... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 456645,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 02:46:54 PM EAT +0300",
							"description" : "+254712726... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 456624,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 02:46:54 PM EAT +0300",
							"description" : "+254713064... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 456660,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 02:46:54 PM EAT +0300",
							"description" : "+254712503... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 456659,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 02:46:54 PM EAT +0300",
							"description" : "+254712509... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 456674,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 02:46:54 PM EAT +0300",
							"description" : "+254712332... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 456679,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 02:46:54 PM EAT +0300",
							"description" : "+254712276... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 456646,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 02:46:53 PM EAT +0300",
							"description" : "+254712716... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 456648,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 02:46:53 PM EAT +0300",
							"description" : "+254712703... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 456630,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 02:46:53 PM EAT +0300",
							"description" : "+254712971... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 456585,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 02:46:53 PM EAT +0300",
							"description" : "+254714037... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 456641,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 02:46:52 PM EAT +0300",
							"description" : "+254712734... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 456650,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 02:46:52 PM EAT +0300",
							"description" : "+254712649... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 456658,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 02:46:51 PM EAT +0300",
							"description" : "+254712513... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 456640,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 02:46:51 PM EAT +0300",
							"description" : "+254712761... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 456635,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 02:46:51 PM EAT +0300",
							"description" : "+254712806... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 456617,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 02:46:50 PM EAT +0300",
							"description" : "+254713301... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 456626,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 02:46:50 PM EAT +0300",
							"description" : "+254713027... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 456627,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 02:46:50 PM EAT +0300",
							"description" : "+254713007... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 456651,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 02:46:50 PM EAT +0300",
							"description" : "+254712607... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 456634,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 02:46:50 PM EAT +0300",
							"description" : "+254712831... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 456623,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 02:46:50 PM EAT +0300",
							"description" : "+254713133... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 456632,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 02:46:49 PM EAT +0300",
							"description" : "+254712887... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 456601,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 02:46:49 PM EAT +0300",
							"description" : "+254713555... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 456612,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 02:46:49 PM EAT +0300",
							"description" : "+254713377... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 456609,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 02:46:49 PM EAT +0300",
							"description" : "+254713404... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 456642,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 02:46:48 PM EAT +0300",
							"description" : "+254712732... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 456631,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 02:46:48 PM EAT +0300",
							"description" : "+254712912... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 456654,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 02:46:48 PM EAT +0300",
							"description" : "+254712557... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 456636,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 02:46:47 PM EAT +0300",
							"description" : "+254712801... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 456643,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 02:46:46 PM EAT +0300",
							"description" : "+254712732... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 456607,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 02:46:45 PM EAT +0300",
							"description" : "+254713517... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 456620,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 02:46:45 PM EAT +0300",
							"description" : "+254713215... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 456579,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 02:46:45 PM EAT +0300",
							"description" : "+254714219... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 454502,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 02:46:44 PM EAT +0300",
							"description" : "+254721324... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 456597,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 02:46:44 PM EAT +0300",
							"description" : "+254713594... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 456633,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 02:46:43 PM EAT +0300",
							"description" : "+254712842... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 456644,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 02:46:43 PM EAT +0300",
							"description" : "+254712730... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 456625,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 02:46:42 PM EAT +0300",
							"description" : "+254713059... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 456610,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 02:46:41 PM EAT +0300",
							"description" : "+254713400... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 456613,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 02:46:41 PM EAT +0300",
							"description" : "+254713373... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 456611,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 02:46:41 PM EAT +0300",
							"description" : "+254713398... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 456604,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 02:46:40 PM EAT +0300",
							"description" : "+254713540... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 456596,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 02:46:40 PM EAT +0300",
							"description" : "+254713640... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 456569,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 02:46:40 PM EAT +0300",
							"description" : "+254714381... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 456614,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 02:46:40 PM EAT +0300",
							"description" : "+254713358... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 456598,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 02:46:40 PM EAT +0300",
							"description" : "+254713591... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 456566,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 02:46:40 PM EAT +0300",
							"description" : "+254714397... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 456621,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 02:46:39 PM EAT +0300",
							"description" : "+254713188... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 456600,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 02:46:39 PM EAT +0300",
							"description" : "+254713555... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 456561,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 02:46:39 PM EAT +0300",
							"description" : "+254714435... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 456606,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 02:46:39 PM EAT +0300",
							"description" : "+254713521... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 456594,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 02:46:38 PM EAT +0300",
							"description" : "+254713685... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 456575,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 02:46:38 PM EAT +0300",
							"description" : "+254714329... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 456589,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 02:46:38 PM EAT +0300",
							"description" : "+254713984... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 456554,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 02:46:38 PM EAT +0300",
							"description" : "+254714576... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 456574,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 02:46:38 PM EAT +0300",
							"description" : "+254714338... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 456603,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 02:46:38 PM EAT +0300",
							"description" : "+254713540... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 456481,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 02:46:37 PM EAT +0300",
							"description" : "+254701388... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 456576,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 02:46:36 PM EAT +0300",
							"description" : "+254714298... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 456588,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 02:46:36 PM EAT +0300",
							"description" : "+254713992... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 456622,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 02:46:36 PM EAT +0300",
							"description" : "+254713173... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 456605,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 02:46:35 PM EAT +0300",
							"description" : "+254713535... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 456551,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 02:46:35 PM EAT +0300",
							"description" : "+254714586... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 456557,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 02:46:35 PM EAT +0300",
							"description" : "+254714499... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 456573,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 02:46:35 PM EAT +0300",
							"description" : "+254714347... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 456608,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 02:46:35 PM EAT +0300",
							"description" : "+254713456... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 456568,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 02:46:35 PM EAT +0300",
							"description" : "+254714385... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 456558,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 02:46:35 PM EAT +0300",
							"description" : "+254714465... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 456602,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 02:46:34 PM EAT +0300",
							"description" : "+254713544... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 456615,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 02:46:34 PM EAT +0300",
							"description" : "+254713339... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 456592,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 02:46:34 PM EAT +0300",
							"description" : "+254713730... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 456590,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 02:46:34 PM EAT +0300",
							"description" : "+254713884... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 456710,
							"state" : "SENT",
							"date_time" : "14 Sep 2016 02:46:34 PM EAT +0300",
							"description" : "+254701257... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 456586,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 02:46:33 PM EAT +0300",
							"description" : "+254714033... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 456536,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 02:46:33 PM EAT +0300",
							"description" : "+254725651... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 456581,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 02:46:33 PM EAT +0300",
							"description" : "+254714143... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 456580,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 02:46:33 PM EAT +0300",
							"description" : "+254714193... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 456562,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 02:46:32 PM EAT +0300",
							"description" : "+254714430... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 456714,
							"state" : "SENT",
							"date_time" : "14 Sep 2016 02:46:32 PM EAT +0300",
							"description" : "+254701011... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 456704,
							"state" : "SENT",
							"date_time" : "14 Sep 2016 02:46:32 PM EAT +0300",
							"description" : "+254712003... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 456534,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 02:46:32 PM EAT +0300",
							"description" : "+254725662... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 456582,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 02:46:32 PM EAT +0300",
							"description" : "+254714140... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 456523,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 02:46:32 PM EAT +0300",
							"description" : "+254725756... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 456537,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 02:46:32 PM EAT +0300",
							"description" : "+254725647... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 456720,
							"state" : "SENT",
							"date_time" : "14 Sep 2016 02:46:31 PM EAT +0300",
							"description" : "+254700712... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 456563,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 02:46:31 PM EAT +0300",
							"description" : "+254714430... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 456567,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 02:46:31 PM EAT +0300",
							"description" : "+254714392... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 456591,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 02:46:31 PM EAT +0300",
							"description" : "+254713795... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 456463,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 02:46:30 PM EAT +0300",
							"description" : "+254702344... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}, {
							"index" : 456542,
							"state" : "DELIVERED",
							"date_time" : "14 Sep 2016 02:46:30 PM EAT +0300",
							"description" : "+254725613... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
							"name" : "40808_Bulk_AMKAGROUP"
						}
					],
					"data" : [[{
								"index" : 454522,
								"state" : "PROCESSING",
								"date_time" : "15 Sep 2016 02:38:03 PM EAT +0300",
								"description" : "N... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 454077,
								"state" : "PROCESSING",
								"date_time" : "15 Sep 2016 02:37:01 PM EAT +0300",
								"description" : "N... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 454076,
								"state" : "PROCESSING",
								"date_time" : "15 Sep 2016 02:36:59 PM EAT +0300",
								"description" : "N... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 450611,
								"state" : "PROCESSING",
								"date_time" : "15 Sep 2016 12:00:50 PM EAT +0300",
								"description" : "N... : Now Invest,Borrow or Lend 24Hrs/7dys on M-Chaama & grow via High Dividends! Lenders Pie now@13,500 upto 10.10.16.Sacco Mradi-Loans Available",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 450166,
								"state" : "PROCESSING",
								"date_time" : "15 Sep 2016 12:00:02 PM EAT +0300",
								"description" : "N... : Now Invest,Borrow or Lend 24Hrs/7dys on M-Chaama & grow via High Dividends! Lenders Pie now@13,500 upto 10.10.16.Sacco Mradi-Loans Available",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 450165,
								"state" : "PROCESSING",
								"date_time" : "15 Sep 2016 12:00:00 PM EAT +0300",
								"description" : "N... : Now Invest,Borrow or Lend 24Hrs/7dys on M-Chaama & grow via High Dividends! Lenders Pie now@13,500 upto 10.10.16.Sacco Mradi-Loans Available",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 455879,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 11:34:11 PM EAT +0300",
								"description" : "+254717203... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 454236,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 11:12:22 PM EAT +0300",
								"description" : "+254725274... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 454953,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 10:07:37 PM EAT +0300",
								"description" : "+254723535... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 455997,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 10:02:48 PM EAT +0300",
								"description" : "+254711909... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 455033,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 09:55:37 PM EAT +0300",
								"description" : "+254728776... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 454880,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 09:41:17 PM EAT +0300",
								"description" : "+254726634... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 454829,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 09:28:01 PM EAT +0300",
								"description" : "+254727062... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 456350,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 08:48:31 PM EAT +0300",
								"description" : "+254707309... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 455734,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 08:44:22 PM EAT +0300",
								"description" : "+254722269... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 456595,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 08:43:26 PM EAT +0300",
								"description" : "+254713675... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 454340,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 08:42:41 PM EAT +0300",
								"description" : "+254727478... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 454087,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 08:36:15 PM EAT +0300",
								"description" : "+254720349... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 454205,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 08:25:48 PM EAT +0300",
								"description" : "+254717311... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 454870,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 08:17:38 PM EAT +0300",
								"description" : "+254726704... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 456495,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 08:06:22 PM EAT +0300",
								"description" : "+254726073... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 454237,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 07:57:52 PM EAT +0300",
								"description" : "+254720408... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 454603,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 07:57:29 PM EAT +0300",
								"description" : "+254790115... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 456685,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 07:56:51 PM EAT +0300",
								"description" : "+254712195... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 455939,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 07:52:18 PM EAT +0300",
								"description" : "+254715715... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 455924,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 07:51:36 PM EAT +0300",
								"description" : "+254716118... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 456011,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 07:47:14 PM EAT +0300",
								"description" : "+254711642... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 454924,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 07:44:22 PM EAT +0300",
								"description" : "+254726210... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 455909,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 07:44:02 PM EAT +0300",
								"description" : "+254716467... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 455822,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 07:43:36 PM EAT +0300",
								"description" : "+254721735... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 453862,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 07:43:19 PM EAT +0300",
								"description" : "+254716269... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 455148,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 07:39:10 PM EAT +0300",
								"description" : "+254722754... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 456531,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 07:38:11 PM EAT +0300",
								"description" : "+254725673... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 455604,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 07:26:52 PM EAT +0300",
								"description" : "+254720122... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 454220,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 07:20:18 PM EAT +0300",
								"description" : "+254720432... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 453893,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 07:16:38 PM EAT +0300",
								"description" : "+254727244... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 454547,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 07:16:27 PM EAT +0300",
								"description" : "+254713683... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 456538,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 07:10:30 PM EAT +0300",
								"description" : "+254725640... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 456210,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 07:07:46 PM EAT +0300",
								"description" : "+254722448... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 455067,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 07:06:04 PM EAT +0300",
								"description" : "+254728338... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 454110,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 06:55:29 PM EAT +0300",
								"description" : "+254715308... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 456705,
								"state" : "FAILED",
								"date_time" : "14 Sep 2016 06:47:11 PM EAT +0300",
								"description" : "+254711989... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 456571,
								"state" : "FAILED",
								"date_time" : "14 Sep 2016 06:46:37 PM EAT +0300",
								"description" : "+254714356... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 456510,
								"state" : "FAILED",
								"date_time" : "14 Sep 2016 06:46:35 PM EAT +0300",
								"description" : "+254725855... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 456323,
								"state" : "FAILED",
								"date_time" : "14 Sep 2016 06:45:54 PM EAT +0300",
								"description" : "+254708208... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 456122,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 06:45:44 PM EAT +0300",
								"description" : "+254725301... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 455628,
								"state" : "FAILED",
								"date_time" : "14 Sep 2016 06:43:46 PM EAT +0300",
								"description" : "+254719391... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 455481,
								"state" : "FAILED",
								"date_time" : "14 Sep 2016 06:43:46 PM EAT +0300",
								"description" : "+254720949... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 455654,
								"state" : "FAILED",
								"date_time" : "14 Sep 2016 06:43:42 PM EAT +0300",
								"description" : "+254718561... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 456043,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 06:40:54 PM EAT +0300",
								"description" : "+254711108... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 454681,
								"state" : "FAILED",
								"date_time" : "14 Sep 2016 06:40:01 PM EAT +0300",
								"description" : "+254729522... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 454605,
								"state" : "SENT",
								"date_time" : "14 Sep 2016 06:39:57 PM EAT +0300",
								"description" : "+254775818... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 454638,
								"state" : "FAILED",
								"date_time" : "14 Sep 2016 06:39:56 PM EAT +0300",
								"description" : "+254736871... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 454300,
								"state" : "FAILED",
								"date_time" : "14 Sep 2016 06:39:53 PM EAT +0300",
								"description" : "+254734735... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 453908,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 06:39:40 PM EAT +0300",
								"description" : "+254722992... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 454371,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 06:39:33 PM EAT +0300",
								"description" : "+254724815... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 454517,
								"state" : "FAILED",
								"date_time" : "14 Sep 2016 06:39:26 PM EAT +0300",
								"description" : "+254719754... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 454114,
								"state" : "FAILED",
								"date_time" : "14 Sep 2016 06:38:22 PM EAT +0300",
								"description" : "+254725095... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 454160,
								"state" : "FAILED",
								"date_time" : "14 Sep 2016 06:37:51 PM EAT +0300",
								"description" : "+254733770... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 454036,
								"state" : "FAILED",
								"date_time" : "14 Sep 2016 06:37:24 PM EAT +0300",
								"description" : "+2547362634... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 453980,
								"state" : "FAILED",
								"date_time" : "14 Sep 2016 06:37:11 PM EAT +0300",
								"description" : "+254729988... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 456321,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 06:36:08 PM EAT +0300",
								"description" : "+254708252... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 454204,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 06:35:13 PM EAT +0300",
								"description" : "+254717432... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 454226,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 06:34:58 PM EAT +0300",
								"description" : "+254713215... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 455638,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 06:33:46 PM EAT +0300",
								"description" : "+254719118... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 456026,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 06:33:42 PM EAT +0300",
								"description" : "+254711427... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 454628,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 06:28:57 PM EAT +0300",
								"description" : "+254728943... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 454577,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 06:25:17 PM EAT +0300",
								"description" : "+254706365... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 456628,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 06:23:13 PM EAT +0300",
								"description" : "+254712990... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 455201,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 06:11:25 PM EAT +0300",
								"description" : "+254721510... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 456046,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 06:11:06 PM EAT +0300",
								"description" : "+254710981... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 456394,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 06:10:02 PM EAT +0300",
								"description" : "+254705516... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 455125,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 06:09:55 PM EAT +0300",
								"description" : "+254722843... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 456618,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 06:09:46 PM EAT +0300",
								"description" : "+254713267... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 454343,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 06:09:22 PM EAT +0300",
								"description" : "+254727236... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 455060,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 06:03:06 PM EAT +0300",
								"description" : "+254728494... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 454303,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 05:52:36 PM EAT +0300",
								"description" : "+254724655... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 454109,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 05:46:40 PM EAT +0300",
								"description" : "+254714684... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 455331,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 05:45:46 PM EAT +0300",
								"description" : "+254724526... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 456723,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 05:44:17 PM EAT +0300",
								"description" : "+254700556... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 454472,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 05:41:21 PM EAT +0300",
								"description" : "+254723468... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 456356,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 05:37:18 PM EAT +0300",
								"description" : "+254707024... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 455673,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 05:35:27 PM EAT +0300",
								"description" : "+254717756... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 456711,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 05:34:43 PM EAT +0300",
								"description" : "+254701232... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 454570,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 05:24:30 PM EAT +0300",
								"description" : "+254708819... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 456479,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 05:22:18 PM EAT +0300",
								"description" : "+254701512... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 455232,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 05:18:11 PM EAT +0300",
								"description" : "+254721340... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 456500,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 05:14:13 PM EAT +0300",
								"description" : "+254725982... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 456473,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 05:11:10 PM EAT +0300",
								"description" : "+254701700... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 455557,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 05:09:26 PM EAT +0300",
								"description" : "+254720528... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 456584,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 05:06:32 PM EAT +0300",
								"description" : "+254714073... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 456045,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 05:03:45 PM EAT +0300",
								"description" : "+254711107... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 455190,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 04:57:42 PM EAT +0300",
								"description" : "+254722587... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 456254,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 04:57:41 PM EAT +0300",
								"description" : "+254720259... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 456461,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 04:52:32 PM EAT +0300",
								"description" : "+254702412... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 453940,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 04:52:26 PM EAT +0300",
								"description" : "+254713904... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 454660,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 04:49:42 PM EAT +0300",
								"description" : "+254729747... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 455062,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 04:48:47 PM EAT +0300",
								"description" : "+254728471... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 454481,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 04:48:44 PM EAT +0300",
								"description" : "+254722726... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 454339,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 04:48:44 PM EAT +0300",
								"description" : "+254727599... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 456638,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 04:48:29 PM EAT +0300",
								"description" : "+254712796... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 454239,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 04:47:46 PM EAT +0300",
								"description" : "+254720376... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 455304,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 04:45:31 PM EAT +0300",
								"description" : "+254724680... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 455413,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 04:44:35 PM EAT +0300",
								"description" : "+254723885... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 453934,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 04:44:15 PM EAT +0300",
								"description" : "+254737486... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 454567,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 04:41:48 PM EAT +0300",
								"description" : "+254711101... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 454630,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 04:40:38 PM EAT +0300",
								"description" : "+254728392... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 455783,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 04:40:29 PM EAT +0300",
								"description" : "+254721947... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 454271,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 04:40:16 PM EAT +0300",
								"description" : "+254720243... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 455922,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 04:39:50 PM EAT +0300",
								"description" : "+254716128... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 454424,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 04:39:03 PM EAT +0300",
								"description" : "+254787776... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 454421,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 04:38:46 PM EAT +0300",
								"description" : "+254733404... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 456001,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 04:38:30 PM EAT +0300",
								"description" : "+254711855... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 454143,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 04:37:47 PM EAT +0300",
								"description" : "+254733925... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 455597,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 04:37:12 PM EAT +0300",
								"description" : "+254720145... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 456342,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 04:36:47 PM EAT +0300",
								"description" : "+254707680... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 454291,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 04:34:15 PM EAT +0300",
								"description" : "+254724793... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 455100,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 04:31:37 PM EAT +0300",
								"description" : "+254722938... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 455129,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 04:29:43 PM EAT +0300",
								"description" : "+254722839... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 454345,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 04:29:35 PM EAT +0300",
								"description" : "+254727135... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 454589,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 04:29:14 PM EAT +0300",
								"description" : "+254702468... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 454378,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 04:24:11 PM EAT +0300",
								"description" : "+254724093... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 454217,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 04:18:16 PM EAT +0300",
								"description" : "+254707214... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 455524,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 04:18:11 PM EAT +0300",
								"description" : "+254720754... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 456587,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 04:16:43 PM EAT +0300",
								"description" : "+254714009... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 456003,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 04:16:27 PM EAT +0300",
								"description" : "+254711842... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 456403,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 04:16:19 PM EAT +0300",
								"description" : "+254705122... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 454833,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 04:14:21 PM EAT +0300",
								"description" : "+254726988... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 456195,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 04:09:39 PM EAT +0300",
								"description" : "+254722501... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 456022,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 04:02:08 PM EAT +0300",
								"description" : "+254711456... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 456444,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 04:01:10 PM EAT +0300",
								"description" : "+254702861... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 455899,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 04:00:11 PM EAT +0300",
								"description" : "+254716718... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 454069,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 03:53:37 PM EAT +0300",
								"description" : "+254721946... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 455643,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 03:52:36 PM EAT +0300",
								"description" : "+254718887... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 454683,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 03:49:46 PM EAT +0300",
								"description" : "+254729498... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 455917,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 03:49:42 PM EAT +0300",
								"description" : "+254716285... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 454346,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 03:48:41 PM EAT +0300",
								"description" : "+254727051... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 454046,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 03:47:27 PM EAT +0300",
								"description" : "+254726143... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 455187,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 03:45:48 PM EAT +0300",
								"description" : "+254722598... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 456065,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 03:44:58 PM EAT +0300",
								"description" : "+254710747... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 454990,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 03:43:46 PM EAT +0300",
								"description" : "+254723336... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 455231,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 03:42:10 PM EAT +0300",
								"description" : "+254721345... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 456051,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 03:41:34 PM EAT +0300",
								"description" : "+254710914... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 454387,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 03:40:57 PM EAT +0300",
								"description" : "+254723173... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 455412,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 03:40:07 PM EAT +0300",
								"description" : "+254723896... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 454151,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 03:39:52 PM EAT +0300",
								"description" : "+254728752... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 454473,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 03:39:33 PM EAT +0300",
								"description" : "+254723367... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 454449,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 03:38:03 PM EAT +0300",
								"description" : "+254724619... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 453853,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 03:37:58 PM EAT +0300",
								"description" : "+254721596... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 453927,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 03:37:27 PM EAT +0300",
								"description" : "+254707362... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 456629,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 03:36:42 PM EAT +0300",
								"description" : "+254712987... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 455234,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 03:36:22 PM EAT +0300",
								"description" : "+254721333... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 456423,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 03:36:07 PM EAT +0300",
								"description" : "+254704061... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 456100,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 03:35:33 PM EAT +0300",
								"description" : "+254725433... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 453870,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 03:35:29 PM EAT +0300",
								"description" : "+254713904... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 455230,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 03:35:09 PM EAT +0300",
								"description" : "+254721352... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 455729,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 03:34:51 PM EAT +0300",
								"description" : "+254722284... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 455972,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 03:34:42 PM EAT +0300",
								"description" : "+254714818... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 455951,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 03:34:37 PM EAT +0300",
								"description" : "+254715378... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 455890,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 03:34:35 PM EAT +0300",
								"description" : "+254716955... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 455740,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 03:33:57 PM EAT +0300",
								"description" : "+254722241... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 455499,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 03:32:56 PM EAT +0300",
								"description" : "+254720844... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 456198,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 03:32:32 PM EAT +0300",
								"description" : "+254722497... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 454027,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 03:32:12 PM EAT +0300",
								"description" : "+254720378... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 455001,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 03:32:04 PM EAT +0300",
								"description" : "+254723250... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 455158,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 03:31:56 PM EAT +0300",
								"description" : "+254722691... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 455028,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 03:31:27 PM EAT +0300",
								"description" : "+254728851... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 454869,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 03:30:48 PM EAT +0300",
								"description" : "+254726705... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 456126,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 03:29:37 PM EAT +0300",
								"description" : "+254725286... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 455163,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 03:29:21 PM EAT +0300",
								"description" : "+254722678... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 454430,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 03:29:18 PM EAT +0300",
								"description" : "+254706704... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 454562,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 03:29:17 PM EAT +0300",
								"description" : "+254711431... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 454428,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 03:29:17 PM EAT +0300",
								"description" : "+254729674... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 454139,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 03:28:52 PM EAT +0300",
								"description" : "+254724204... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 454581,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 03:28:31 PM EAT +0300",
								"description" : "+254704520... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 454195,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 03:28:01 PM EAT +0300",
								"description" : "+254727491... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 455896,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 03:27:39 PM EAT +0300",
								"description" : "+254716800... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 454056,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 03:27:17 PM EAT +0300",
								"description" : "+254721765... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 453867,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 03:26:53 PM EAT +0300",
								"description" : "+254722896... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 454796,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 03:26:47 PM EAT +0300",
								"description" : "+254727434... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 456153,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 03:26:09 PM EAT +0300",
								"description" : "+254725069... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 456107,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 03:25:14 PM EAT +0300",
								"description" : "+254725384... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 454126,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 03:24:30 PM EAT +0300",
								"description" : "+254724236... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 456583,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 03:24:13 PM EAT +0300",
								"description" : "+254714133... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 456389,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 03:23:40 PM EAT +0300",
								"description" : "+254705833... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 455573,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 03:23:11 PM EAT +0300",
								"description" : "+254720473... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 456662,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 03:22:15 PM EAT +0300",
								"description" : "+254712475... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 456724,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 03:21:37 PM EAT +0300",
								"description" : "+254700546... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 454624,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 03:21:21 PM EAT +0300",
								"description" : "+254729010... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 454519,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 03:21:15 PM EAT +0300",
								"description" : "+254718502... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 456404,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 03:21:05 PM EAT +0300",
								"description" : "+254704728... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 455925,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 03:20:46 PM EAT +0300",
								"description" : "+254716111... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 455865,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 03:20:32 PM EAT +0300",
								"description" : "+254721575... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 455944,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 03:19:56 PM EAT +0300",
								"description" : "+254715601... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 455544,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 03:19:45 PM EAT +0300",
								"description" : "+254720615... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 456415,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 03:19:42 PM EAT +0300",
								"description" : "+254704355... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 456247,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 03:19:12 PM EAT +0300",
								"description" : "+254720290... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 455895,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 03:18:49 PM EAT +0300",
								"description" : "+254716812... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 454801,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 03:17:17 PM EAT +0300",
								"description" : "+254727326... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 455203,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 03:17:05 PM EAT +0300",
								"description" : "+254721491... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 453869,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 03:16:55 PM EAT +0300",
								"description" : "+254725739... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 455030,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 03:16:36 PM EAT +0300",
								"description" : "+254728807... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 454691,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 03:15:44 PM EAT +0300",
								"description" : "+254729370... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 456657,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 03:15:23 PM EAT +0300",
								"description" : "+254712519... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 454921,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 03:15:01 PM EAT +0300",
								"description" : "+254726261... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 455099,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 03:14:56 PM EAT +0300",
								"description" : "+254722941... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 454432,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 03:14:50 PM EAT +0300",
								"description" : "+254720972... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 454725,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 03:14:46 PM EAT +0300",
								"description" : "+254728221... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 455325,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 03:14:38 PM EAT +0300",
								"description" : "+254724547... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 455678,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 03:14:13 PM EAT +0300",
								"description" : "+254717641... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 455034,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 03:13:29 PM EAT +0300",
								"description" : "+254728759... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 456340,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 03:13:14 PM EAT +0300",
								"description" : "+254707740... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 455571,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 03:13:08 PM EAT +0300",
								"description" : "+254720477... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 454429,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 03:13:00 PM EAT +0300",
								"description" : "+254711184... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 454178,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 03:12:51 PM EAT +0300",
								"description" : "+254719464... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 455134,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 03:12:51 PM EAT +0300",
								"description" : "+254722817... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 454741,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 03:12:42 PM EAT +0300",
								"description" : "+254727987... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 455566,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 03:12:41 PM EAT +0300",
								"description" : "+254720488... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 455235,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 03:12:26 PM EAT +0300",
								"description" : "+254721332... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 455684,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 03:12:11 PM EAT +0300",
								"description" : "+254722400... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 456509,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 03:12:02 PM EAT +0300",
								"description" : "+254725855... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 454427,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 03:12:01 PM EAT +0300",
								"description" : "+254710941... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 453998,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 03:11:56 PM EAT +0300",
								"description" : "+254722779... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 456133,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 03:11:51 PM EAT +0300",
								"description" : "+254725239... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 455258,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 03:11:49 PM EAT +0300",
								"description" : "+254721232... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 456671,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 03:11:48 PM EAT +0300",
								"description" : "+254712360... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 455900,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 03:11:31 PM EAT +0300",
								"description" : "+254716691... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 454815,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 03:11:16 PM EAT +0300",
								"description" : "+254727212... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 455247,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 03:10:21 PM EAT +0300",
								"description" : "+254721291... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 454104,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 03:10:17 PM EAT +0300",
								"description" : "+254718477... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 456211,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 03:10:10 PM EAT +0300",
								"description" : "+254722447... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 456729,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 03:10:06 PM EAT +0300",
								"description" : "+254700481... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 456716,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 03:09:50 PM EAT +0300",
								"description" : "+254700868... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 454426,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 03:09:49 PM EAT +0300",
								"description" : "+254724071... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 455619,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 03:09:40 PM EAT +0300",
								"description" : "+254719792... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 455799,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 03:09:39 PM EAT +0300",
								"description" : "+254721862... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 455589,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 03:09:31 PM EAT +0300",
								"description" : "+254720386... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 454166,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 03:09:21 PM EAT +0300",
								"description" : "+254705575... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 455255,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 03:09:13 PM EAT +0300",
								"description" : "+254721252... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 455694,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 03:09:09 PM EAT +0300",
								"description" : "+254722376... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 455093,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 03:09:08 PM EAT +0300",
								"description" : "+254722975... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 455251,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 03:09:07 PM EAT +0300",
								"description" : "+254721275... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 456382,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 03:09:06 PM EAT +0300",
								"description" : "+254706118... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 455026,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 03:09:01 PM EAT +0300",
								"description" : "+254728872... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 456092,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 03:09:01 PM EAT +0300",
								"description" : "+254725478... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 456593,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 03:08:56 PM EAT +0300",
								"description" : "+254713711... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 455323,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 03:08:50 PM EAT +0300",
								"description" : "+254724558... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 454233,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 03:08:45 PM EAT +0300",
								"description" : "+254722233... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 454052,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 03:08:37 PM EAT +0300",
								"description" : "+254726826... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 456062,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 03:08:34 PM EAT +0300",
								"description" : "+254710767... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 456735,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 03:08:17 PM EAT +0300",
								"description" : "+254700303... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 454262,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 03:08:10 PM EAT +0300",
								"description" : "+254724766... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 455477,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 03:08:09 PM EAT +0300",
								"description" : "+254720970... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 454070,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 03:08:06 PM EAT +0300",
								"description" : "+254722946... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 455963,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 03:08:06 PM EAT +0300",
								"description" : "+254715056... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 456096,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 03:08:04 PM EAT +0300",
								"description" : "+254725460... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 454433,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 03:08:00 PM EAT +0300",
								"description" : "+254700002... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 453960,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 03:07:59 PM EAT +0300",
								"description" : "+254720831... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 454083,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 03:07:57 PM EAT +0300",
								"description" : "+254726943... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 454120,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 03:07:48 PM EAT +0300",
								"description" : "+254718243... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 455574,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 03:07:48 PM EAT +0300",
								"description" : "+254720473... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 456414,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 03:07:48 PM EAT +0300",
								"description" : "+254704368... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 455386,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 03:07:46 PM EAT +0300",
								"description" : "+254724092... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 455893,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 03:07:16 PM EAT +0300",
								"description" : "+254716834... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 456661,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 03:07:04 PM EAT +0300",
								"description" : "+254712481... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 454534,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 03:06:28 PM EAT +0300",
								"description" : "+254715677... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 454882,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 03:05:17 PM EAT +0300",
								"description" : "+254726610... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 454168,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 03:05:13 PM EAT +0300",
								"description" : "+254702810... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 456470,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 03:05:10 PM EAT +0300",
								"description" : "+254702014... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 455559,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 03:04:38 PM EAT +0300",
								"description" : "+254720520... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 454520,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 03:04:26 PM EAT +0300",
								"description" : "+254718372... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 455327,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 03:03:49 PM EAT +0300",
								"description" : "+254724541... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 454853,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 03:02:52 PM EAT +0300",
								"description" : "+254726860... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 456121,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 03:02:36 PM EAT +0300",
								"description" : "+254725301... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 456734,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 03:01:59 PM EAT +0300",
								"description" : "+254700390... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 454986,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 03:01:40 PM EAT +0300",
								"description" : "+254723370... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 456560,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 03:01:18 PM EAT +0300",
								"description" : "+254714439... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 455914,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 03:01:02 PM EAT +0300",
								"description" : "+254716330... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 456438,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 03:01:01 PM EAT +0300",
								"description" : "+254703168... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 456372,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 03:00:50 PM EAT +0300",
								"description" : "+254706331... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 456706,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 03:00:04 PM EAT +0300",
								"description" : "+254711989... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 456060,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 02:59:56 PM EAT +0300",
								"description" : "+254710778... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 456042,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 02:59:56 PM EAT +0300",
								"description" : "+254711126... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 455959,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 02:59:47 PM EAT +0300",
								"description" : "+254715156... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 455949,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 02:59:45 PM EAT +0300",
								"description" : "+254715426... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 455744,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 02:59:38 PM EAT +0300",
								"description" : "+254722223... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 455803,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 02:59:21 PM EAT +0300",
								"description" : "+254721828... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 455835,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 02:59:11 PM EAT +0300",
								"description" : "+254721683... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 455737,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 02:58:58 PM EAT +0300",
								"description" : "+254722255... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 456419,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 02:58:57 PM EAT +0300",
								"description" : "+254704278... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 456141,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 02:58:44 PM EAT +0300",
								"description" : "+254725206... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 454187,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 02:58:37 PM EAT +0300",
								"description" : "+254726832... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 455377,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 02:57:36 PM EAT +0300",
								"description" : "+254724144... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 455289,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 02:57:28 PM EAT +0300",
								"description" : "+254724786... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 454705,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 02:57:26 PM EAT +0300",
								"description" : "+254729079... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 455415,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 02:57:25 PM EAT +0300",
								"description" : "+254723879... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 455345,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 02:57:21 PM EAT +0300",
								"description" : "+254724441... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 455249,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 02:57:02 PM EAT +0300",
								"description" : "+254721283... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 454402,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 02:57:00 PM EAT +0300",
								"description" : "+254710956... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 455020,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 02:56:43 PM EAT +0300",
								"description" : "+254723165... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 455164,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 02:56:35 PM EAT +0300",
								"description" : "+254722676... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 454837,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 02:56:26 PM EAT +0300",
								"description" : "+254726943... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 454938,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 02:56:20 PM EAT +0300",
								"description" : "+254723591... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 454099,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 02:56:15 PM EAT +0300",
								"description" : "+254713310... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 455610,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 02:55:34 PM EAT +0300",
								"description" : "+254720060... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 454816,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 02:55:33 PM EAT +0300",
								"description" : "+254727177... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 454373,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 02:55:32 PM EAT +0300",
								"description" : "+254724628... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 454885,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 02:55:32 PM EAT +0300",
								"description" : "+254726559... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 454854,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 02:55:31 PM EAT +0300",
								"description" : "+254726841... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 454840,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 02:55:30 PM EAT +0300",
								"description" : "+254726934... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 456564,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 02:55:24 PM EAT +0300",
								"description" : "+254714419... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 454664,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 02:54:59 PM EAT +0300",
								"description" : "+254729722... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 454650,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 02:54:55 PM EAT +0300",
								"description" : "+254729898... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 456652,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 02:54:30 PM EAT +0300",
								"description" : "+254712592... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 454505,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 02:54:22 PM EAT +0300",
								"description" : "+254721209... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 454068,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 02:54:18 PM EAT +0300",
								"description" : "+254704735... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 454487,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 02:54:12 PM EAT +0300",
								"description" : "+254722276... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 456255,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 02:53:24 PM EAT +0300",
								"description" : "+254720258... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 454199,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 02:53:21 PM EAT +0300",
								"description" : "+254716414... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 455383,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 02:53:04 PM EAT +0300",
								"description" : "+254724102... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 454038,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 02:52:30 PM EAT +0300",
								"description" : "+254722828... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 453971,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 02:52:18 PM EAT +0300",
								"description" : "+254726037... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 453954,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 02:52:13 PM EAT +0300",
								"description" : "+254722349... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 454518,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 02:52:03 PM EAT +0300",
								"description" : "+254719389... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 454476,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 02:50:51 PM EAT +0300",
								"description" : "+254723094... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 454231,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 02:50:13 PM EAT +0300",
								"description" : "+254722630... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 454943,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 02:49:53 PM EAT +0300",
								"description" : "+254723574... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 456388,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 02:49:42 PM EAT +0300",
								"description" : "+254705855... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 455155,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 02:48:55 PM EAT +0300",
								"description" : "+254722694... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 455563,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 02:48:50 PM EAT +0300",
								"description" : "+254720495... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 456196,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 02:48:15 PM EAT +0300",
								"description" : "+254722498... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 454366,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 02:48:14 PM EAT +0300",
								"description" : "+254725050... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 456401,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 02:48:01 PM EAT +0300",
								"description" : "+254705199... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 456653,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 02:47:59 PM EAT +0300",
								"description" : "+254712559... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 454913,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 02:47:46 PM EAT +0300",
								"description" : "+254726316... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 454615,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 02:47:18 PM EAT +0300",
								"description" : "+254729980... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 455958,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 02:47:14 PM EAT +0300",
								"description" : "+254715181... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 454587,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 02:47:07 PM EAT +0300",
								"description" : "+254703816... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 456559,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 02:47:07 PM EAT +0300",
								"description" : "+254714447... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 456741,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 02:47:07 PM EAT +0300",
								"description" : "+254700166... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 456726,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 02:47:06 PM EAT +0300",
								"description" : "+254700498... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 456742,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 02:47:06 PM EAT +0300",
								"description" : "+254700148... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 456667,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 02:47:06 PM EAT +0300",
								"description" : "+254712461... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 456733,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 02:47:05 PM EAT +0300",
								"description" : "+254700432... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 456745,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 02:47:05 PM EAT +0300",
								"description" : "+254700082... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 455644,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 02:47:05 PM EAT +0300",
								"description" : "+254718879... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 456715,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 02:47:04 PM EAT +0300",
								"description" : "+254700903... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 456737,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 02:47:04 PM EAT +0300",
								"description" : "+254700227... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 456748,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 02:47:04 PM EAT +0300",
								"description" : "+254700018... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 456698,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 02:47:04 PM EAT +0300",
								"description" : "+254712085... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 456739,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 02:47:04 PM EAT +0300",
								"description" : "+254700207... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 456747,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 02:47:04 PM EAT +0300",
								"description" : "+254700020... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 456728,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 02:47:03 PM EAT +0300",
								"description" : "+254700491... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 456744,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 02:47:03 PM EAT +0300",
								"description" : "+254700083... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 456736,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 02:47:03 PM EAT +0300",
								"description" : "+254700278... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 456725,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 02:47:03 PM EAT +0300",
								"description" : "+254700534... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 456746,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 02:47:03 PM EAT +0300",
								"description" : "+254700032... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 456722,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 02:47:03 PM EAT +0300",
								"description" : "+254700625... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 456689,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 02:47:03 PM EAT +0300",
								"description" : "+254712176... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 456738,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 02:47:03 PM EAT +0300",
								"description" : "+254700223... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 456721,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 02:47:02 PM EAT +0300",
								"description" : "+254700654... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 456719,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 02:47:02 PM EAT +0300",
								"description" : "+254700772... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 456699,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 02:47:02 PM EAT +0300",
								"description" : "+254712073... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 456694,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 02:47:02 PM EAT +0300",
								"description" : "+254712138... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 456730,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 02:47:02 PM EAT +0300",
								"description" : "+254700480... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 456732,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 02:47:02 PM EAT +0300",
								"description" : "+254700451... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 456675,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 02:47:01 PM EAT +0300",
								"description" : "+254712319... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 456712,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 02:47:01 PM EAT +0300",
								"description" : "+254701106... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 456665,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 02:47:01 PM EAT +0300",
								"description" : "+254712463... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 456718,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 02:47:00 PM EAT +0300",
								"description" : "+254700810... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 456682,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 02:47:00 PM EAT +0300",
								"description" : "+254712220... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 456702,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 02:47:00 PM EAT +0300",
								"description" : "+254712056... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 456687,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 02:47:00 PM EAT +0300",
								"description" : "+254712178... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 456673,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 02:47:00 PM EAT +0300",
								"description" : "+254712342... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 456686,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 02:47:00 PM EAT +0300",
								"description" : "+254712187... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 456713,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 02:47:00 PM EAT +0300",
								"description" : "+254701020... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 456683,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 02:47:00 PM EAT +0300",
								"description" : "+254712213... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 456678,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 02:47:00 PM EAT +0300",
								"description" : "+254712292... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 456731,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 02:47:00 PM EAT +0300",
								"description" : "+254700470... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 456740,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 02:47:00 PM EAT +0300",
								"description" : "+254700180... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 456664,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 02:46:59 PM EAT +0300",
								"description" : "+254712465... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 456743,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 02:46:59 PM EAT +0300",
								"description" : "+254700112... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 456688,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 02:46:59 PM EAT +0300",
								"description" : "+254712177... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 456727,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 02:46:59 PM EAT +0300",
								"description" : "+254700493... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 456656,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 02:46:59 PM EAT +0300",
								"description" : "+254712537... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 456709,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 02:46:58 PM EAT +0300",
								"description" : "+254701265... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 456647,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 02:46:58 PM EAT +0300",
								"description" : "+254712714... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 456708,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 02:46:58 PM EAT +0300",
								"description" : "+254701283... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 456696,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 02:46:58 PM EAT +0300",
								"description" : "+254712102... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 456670,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 02:46:58 PM EAT +0300",
								"description" : "+254712408... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 456680,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 02:46:58 PM EAT +0300",
								"description" : "+254712248... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 456677,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 02:46:58 PM EAT +0300",
								"description" : "+254712295... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 456693,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 02:46:58 PM EAT +0300",
								"description" : "+254712138... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 456619,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 02:46:58 PM EAT +0300",
								"description" : "+254713255... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 456690,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 02:46:58 PM EAT +0300",
								"description" : "+254712174... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 456717,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 02:46:58 PM EAT +0300",
								"description" : "+254700818... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 456701,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 02:46:57 PM EAT +0300",
								"description" : "+254712056... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 456695,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 02:46:57 PM EAT +0300",
								"description" : "+254712116... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 456416,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 02:46:57 PM EAT +0300",
								"description" : "+254704314... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 456681,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 02:46:57 PM EAT +0300",
								"description" : "+254712233... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 456697,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 02:46:57 PM EAT +0300",
								"description" : "+254712096... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 456676,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 02:46:56 PM EAT +0300",
								"description" : "+254712312... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 456691,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 02:46:56 PM EAT +0300",
								"description" : "+254712174... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 456672,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 02:46:56 PM EAT +0300",
								"description" : "+254712351... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 456649,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 02:46:56 PM EAT +0300",
								"description" : "+254712652... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 456666,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 02:46:56 PM EAT +0300",
								"description" : "+254712462... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 456703,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 02:46:56 PM EAT +0300",
								"description" : "+254712048... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 456684,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 02:46:56 PM EAT +0300",
								"description" : "+254712203... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 456692,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 02:46:56 PM EAT +0300",
								"description" : "+254712152... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 456663,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 02:46:55 PM EAT +0300",
								"description" : "+254712475... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 456645,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 02:46:54 PM EAT +0300",
								"description" : "+254712726... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 456624,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 02:46:54 PM EAT +0300",
								"description" : "+254713064... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 456660,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 02:46:54 PM EAT +0300",
								"description" : "+254712503... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 456659,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 02:46:54 PM EAT +0300",
								"description" : "+254712509... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 456674,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 02:46:54 PM EAT +0300",
								"description" : "+254712332... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 456679,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 02:46:54 PM EAT +0300",
								"description" : "+254712276... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 456646,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 02:46:53 PM EAT +0300",
								"description" : "+254712716... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 456648,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 02:46:53 PM EAT +0300",
								"description" : "+254712703... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 456630,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 02:46:53 PM EAT +0300",
								"description" : "+254712971... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 456585,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 02:46:53 PM EAT +0300",
								"description" : "+254714037... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 456641,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 02:46:52 PM EAT +0300",
								"description" : "+254712734... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 456650,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 02:46:52 PM EAT +0300",
								"description" : "+254712649... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 456658,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 02:46:51 PM EAT +0300",
								"description" : "+254712513... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 456640,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 02:46:51 PM EAT +0300",
								"description" : "+254712761... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 456635,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 02:46:51 PM EAT +0300",
								"description" : "+254712806... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 456617,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 02:46:50 PM EAT +0300",
								"description" : "+254713301... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 456626,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 02:46:50 PM EAT +0300",
								"description" : "+254713027... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 456627,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 02:46:50 PM EAT +0300",
								"description" : "+254713007... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 456651,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 02:46:50 PM EAT +0300",
								"description" : "+254712607... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 456634,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 02:46:50 PM EAT +0300",
								"description" : "+254712831... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 456623,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 02:46:50 PM EAT +0300",
								"description" : "+254713133... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 456632,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 02:46:49 PM EAT +0300",
								"description" : "+254712887... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 456601,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 02:46:49 PM EAT +0300",
								"description" : "+254713555... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 456612,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 02:46:49 PM EAT +0300",
								"description" : "+254713377... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 456609,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 02:46:49 PM EAT +0300",
								"description" : "+254713404... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 456642,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 02:46:48 PM EAT +0300",
								"description" : "+254712732... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 456631,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 02:46:48 PM EAT +0300",
								"description" : "+254712912... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 456654,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 02:46:48 PM EAT +0300",
								"description" : "+254712557... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 456636,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 02:46:47 PM EAT +0300",
								"description" : "+254712801... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 456643,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 02:46:46 PM EAT +0300",
								"description" : "+254712732... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 456607,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 02:46:45 PM EAT +0300",
								"description" : "+254713517... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 456620,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 02:46:45 PM EAT +0300",
								"description" : "+254713215... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 456579,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 02:46:45 PM EAT +0300",
								"description" : "+254714219... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 454502,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 02:46:44 PM EAT +0300",
								"description" : "+254721324... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 456597,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 02:46:44 PM EAT +0300",
								"description" : "+254713594... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 456633,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 02:46:43 PM EAT +0300",
								"description" : "+254712842... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 456644,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 02:46:43 PM EAT +0300",
								"description" : "+254712730... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 456625,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 02:46:42 PM EAT +0300",
								"description" : "+254713059... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 456610,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 02:46:41 PM EAT +0300",
								"description" : "+254713400... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 456613,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 02:46:41 PM EAT +0300",
								"description" : "+254713373... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 456611,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 02:46:41 PM EAT +0300",
								"description" : "+254713398... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 456604,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 02:46:40 PM EAT +0300",
								"description" : "+254713540... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 456596,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 02:46:40 PM EAT +0300",
								"description" : "+254713640... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 456569,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 02:46:40 PM EAT +0300",
								"description" : "+254714381... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 456614,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 02:46:40 PM EAT +0300",
								"description" : "+254713358... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 456598,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 02:46:40 PM EAT +0300",
								"description" : "+254713591... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 456566,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 02:46:40 PM EAT +0300",
								"description" : "+254714397... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 456621,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 02:46:39 PM EAT +0300",
								"description" : "+254713188... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 456600,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 02:46:39 PM EAT +0300",
								"description" : "+254713555... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 456561,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 02:46:39 PM EAT +0300",
								"description" : "+254714435... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 456606,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 02:46:39 PM EAT +0300",
								"description" : "+254713521... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 456594,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 02:46:38 PM EAT +0300",
								"description" : "+254713685... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 456575,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 02:46:38 PM EAT +0300",
								"description" : "+254714329... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 456589,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 02:46:38 PM EAT +0300",
								"description" : "+254713984... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 456554,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 02:46:38 PM EAT +0300",
								"description" : "+254714576... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 456574,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 02:46:38 PM EAT +0300",
								"description" : "+254714338... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 456603,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 02:46:38 PM EAT +0300",
								"description" : "+254713540... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 456481,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 02:46:37 PM EAT +0300",
								"description" : "+254701388... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 456576,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 02:46:36 PM EAT +0300",
								"description" : "+254714298... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 456588,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 02:46:36 PM EAT +0300",
								"description" : "+254713992... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 456622,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 02:46:36 PM EAT +0300",
								"description" : "+254713173... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 456605,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 02:46:35 PM EAT +0300",
								"description" : "+254713535... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 456551,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 02:46:35 PM EAT +0300",
								"description" : "+254714586... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 456557,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 02:46:35 PM EAT +0300",
								"description" : "+254714499... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 456573,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 02:46:35 PM EAT +0300",
								"description" : "+254714347... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 456608,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 02:46:35 PM EAT +0300",
								"description" : "+254713456... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 456568,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 02:46:35 PM EAT +0300",
								"description" : "+254714385... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 456558,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 02:46:35 PM EAT +0300",
								"description" : "+254714465... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 456602,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 02:46:34 PM EAT +0300",
								"description" : "+254713544... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 456615,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 02:46:34 PM EAT +0300",
								"description" : "+254713339... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 456592,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 02:46:34 PM EAT +0300",
								"description" : "+254713730... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 456590,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 02:46:34 PM EAT +0300",
								"description" : "+254713884... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 456710,
								"state" : "SENT",
								"date_time" : "14 Sep 2016 02:46:34 PM EAT +0300",
								"description" : "+254701257... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 456586,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 02:46:33 PM EAT +0300",
								"description" : "+254714033... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 456536,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 02:46:33 PM EAT +0300",
								"description" : "+254725651... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 456581,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 02:46:33 PM EAT +0300",
								"description" : "+254714143... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 456580,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 02:46:33 PM EAT +0300",
								"description" : "+254714193... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 456562,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 02:46:32 PM EAT +0300",
								"description" : "+254714430... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 456714,
								"state" : "SENT",
								"date_time" : "14 Sep 2016 02:46:32 PM EAT +0300",
								"description" : "+254701011... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 456704,
								"state" : "SENT",
								"date_time" : "14 Sep 2016 02:46:32 PM EAT +0300",
								"description" : "+254712003... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 456534,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 02:46:32 PM EAT +0300",
								"description" : "+254725662... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 456582,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 02:46:32 PM EAT +0300",
								"description" : "+254714140... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 456523,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 02:46:32 PM EAT +0300",
								"description" : "+254725756... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 456537,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 02:46:32 PM EAT +0300",
								"description" : "+254725647... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 456720,
								"state" : "SENT",
								"date_time" : "14 Sep 2016 02:46:31 PM EAT +0300",
								"description" : "+254700712... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 456563,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 02:46:31 PM EAT +0300",
								"description" : "+254714430... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 456567,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 02:46:31 PM EAT +0300",
								"description" : "+254714392... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 456591,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 02:46:31 PM EAT +0300",
								"description" : "+254713795... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 456463,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 02:46:30 PM EAT +0300",
								"description" : "+254702344... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}, {
								"index" : 456542,
								"state" : "DELIVERED",
								"date_time" : "14 Sep 2016 02:46:30 PM EAT +0300",
								"description" : "+254725613... : Karibuni Nyote kwenye Mkutano Muhimu wa uwekezaji Bora,tarehe 18.09.16 @ 2PM Gatundu Cinema Hall. Hakuna malipo ya kiingilio.Alika marafiki!",
								"name" : "40808_Bulk_AMKAGROUP"
							}
						]],
					"cols" : [],
					"min_id" : 0,
					"groups" : ["40808_Bulk_AMKAGROUP"],
					"max_id" : 0
				}
			},
			"CHID" : "1"
		},
		"data_name" : {
			"gateway_host" : "mchaama.com",
			"response_status" : "00",
			"data_name" : "data_name",
			"session_id" : "Njk2YTdhNmY3MTc0NzA3OTZjMzA3ODYxNjU2NDYyNzY3NDZkNzgzMTc1Njg2NjY0Nzc3OTMwMzM3ODc2MzAzNjc1NzQ2MjYzNmU2NTYyNmI2YzYzMzQzOTZlMzM2ZjY5NjQ2NTZlNmE2MTY4MzA3NTcwN2E2YzMxNmYzMTZhN2E2ZDMwNmU2ODZhNjc2ODZjNzQ2NjZkMmIwYTc2NjQ3MzY4NzY2Njc1Nzk2NjY1NmU2Nzc0NzMzMTczNzI3NzYyNjY3MDc0Mzk2MjZhNjk3YTMwNzI3ODMxNjE2Njc4MzAzNTc1NzQ3NjZmNzUzMjMxNzU2ZjYzMzkzNjcxNzQzNDMwNmQzMjZmNmY2NjZlNzA2NzZkNjk3YTM1NjU2ZDZlMzI3OTZiNzkzNzcyNjk3MTcyMGE3MjY3NjI3NTY1NjQ2NjcwNjU3OTM0Mzg2NDc1Njg2Zjc1NjQ3ODY4NjY2ODcwNmE2ZDMxNjczMzc4NjMzNDM3NzM2NjZjNmY2MTY1MzU3MzZmNzMzNDcyNzk3OTYzNjgwYQ==",
			"chid" : "1",
			"lat" : "1.0",
			"lng" : "38.0",
			"ip_address" : "197.237.248.163",
			"response" : {
				"data_source" : {
					"rows" : [["enrollment", "Enrollment"]],
					"data" : [],
					"cols" : [],
					"min_id" : 0,
					"groups" : [],
					"max_id" : 0
				}
			},
			"CHID" : "1"
		}

	},
	"FORGOT_PASSWORD":{"gateway_host": "mchaama.com", "response_status": "00", "CHID": "1", "lat": "1.0", "lng": "38.0", "ip_address": "105.53.39.37", "response": {"get_section": {"this_page_inputs": {"100": {"section_var": ["Account", "info"], "1": {"Reset Password": {"1": {"Reset Password": {"1": ["username", "TEXT INPUT", "2", "45", "USERNAME", "", "icons:perm-identity", "col-md-11", "", true], "3": ["Home Page", "HYPERLINK TOP", "0", "0", "HOME", "/", "icons:link", "col-md-4", "", true], "2": ["Submit", "SUBMIT", "0", "0", "submit", "Submit", "check", "col-md-4", "", true], "input_var": ["RESET PASSWORD", "FORM", "0", "0", "", "", "reset_password", "col-md-7", "icons:lock", false, false, 900]}}}, "page_var": ["Reset Password", "icons:lock"]}}}}, "get_gateway_details": {"background_image": "radial-gradient(ellipse farthest-corner at 50% 100%,#00a656 10%,#4afcff  60%,#00a656 90%)", "name": "MChaama", "tagline": "Invest, Borrow, Grow", "default_color": {"color": "#00a858", "theme": "mchaama_theme_1", "color_bk": "#E0EEE0"}, "host": "mchaama.com", "logo": "administration_gateway_logo/M-Chaama_icon.png"}}, "chid": "1"},
	"RESET_PASSWORD":{"username": "customercare", "gateway_host": "mchaama.com", "response_status": "00", "response": {"get_section": {"this_page_inputs": {"100": {"section_var": ["Account", "info"], "0": {"Reset Password Response": {"1": {"Reset Password": {"1": ["Please check your email for reset instructions", "LABEL", "0", "0", "instruction", "", "icons:check-circle", "col-md-11", "", true], "2": ["Home Page", "HYPERLINK TOP", "0", "0", "HOME", "/", "icons:link", "col-md-11", "", true], "input_var": ["RESET PASSWORD", "FORM", "0", "0", "", "", "reset_password", "col-md-7", "icons:lock", false, false, 900]}}}, "page_var": ["Reset Password Response", "icons:lock"]}}}}, "get_profile": "Session Profile Captured", "get_notification": {"background_image": "radial-gradient(ellipse farthest-corner at 50% 100%,#00a656 10%,#4afcff  60%,#00a656 90%)", "name": "MChaama", "tagline": "Invest, Borrow, Grow", "default_color": {"color": "#00a858", "theme": "mchaama_theme_1", "color_bk": "#E0EEE0"}, "host": "mchaama.com", "logo": "administration_gateway_logo/M-Chaama_icon.png"}, "reset_password": "Reset Profile Captured", "debit_float": "No float amount to debit", "get_gateway_details": {"background_image": "radial-gradient(ellipse farthest-corner at 50% 100%,#00a656 10%,#4afcff  60%,#00a656 90%)", "name": "MChaama", "tagline": "Invest, Borrow, Grow", "default_color": {"color": "#00a858", "theme": "mchaama_theme_1", "color_bk": "#E0EEE0"}, "host": "mchaama.com", "logo": "administration_gateway_logo/M-Chaama_icon.png"}, "send_notification": "Notification Sent. Please check EMAIL"}, "CHID": "1", "chid": "1", "lat": "1.0", "lng": "38.0", "ip_address": "105.53.39.37", "csrfmiddlewaretoken": "MfsQIIhZPXNmk5CBjk1JRqNLE6HHHysm"}

};