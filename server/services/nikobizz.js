var express = require('express');
var router = express.Router();
var config = require(__dirname +"/../../config");

var db = require("../db/"+config.SERVICE_NAME);

router
.route('/HOME')
.get(function (req, res) {

    })
    .post(function (req, res) {
        res.json(db.home2);
        if (req.session.loggedin) {
            res.json(db.HOME_LOGGED_IN);
        } else {
            res.json(db.HOME);
        }
    });

router.route('/LOGIN')
    .get(function (req, res) {

    })
    .post(function (req, res) {
        if (req.body['USERNAME'] == '' && req.body['PASSWORD'] == '') {

        } else {//wrong login
            //res.json(db['login2']);
        }
        req.session.loggedin = true;
        res.json(db.LOGIN);
    });

router.route('/FORGOT%20PASSWORD')
    .get(function (req, res) {
    })
    .post(function (req, res) {
        res.json(db['FORGOT PASSWORD']);
    });

router.route('/SHOPPING%20CART')
    .get(function (req, res) {
    })
    .post(function (req, res) {
        res.json(db['SHOPPING%20CART']);
    });

router
.route('/DATA%20SOURCE')
.post(function (req, res) {
    var dbkey = req.body['data_name'];
    //dbkey = dbkey.replace("=",":");
    
    console.log(req.body);
    console.log('db key is : ' + dbkey);
    res.json(db["DATA_SOURCE"][dbkey]);
    
});