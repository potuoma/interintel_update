var express = require('express');
var router = express.Router();
var config = require(__dirname +"/../../config");

var db = require("../db/"+config.SERVICE_NAME);

router.use(function timeLog(req, res, next) {
  console.log('Time: ', Date.now());
  next();
});

router
.route('/HOME')
.get(function (req, res) {})
.post(function (req, res) {
    if (req.session.loggedin) {
        res.json(db.HOME_LOGGED_IN);
    } else {
        res.json(db.HOME);
    }
    });

router
.route('/LOGIN')
.get(function (req, res) {})
.post(function (req, res) {

    req.session.loggedin=true;
    res.json(db.LOGIN);

    });
router
.route('/FORGOT%20PASSWORD')
.get(function (req, res) {})
.post(function (req, res) {
    res.json(db.FORGOT_PASSWORD);
});

router
.route('/RESET%20PASSWORD')
.get(function (req, res) {})
.post(function (req, res) {
    res.json(db.RESET_PASSWORD);
});

router
.route('/VIEW%20EDIT%20BID')
.get(function (req, res) {})
.post(function (req, res) {
    res.json(db.VIEW_EDIT_BID);
});

router
.route('/DATA%20SOURCE')
.post(function (req, res) {
    var dbkey = req.body['data_name'];
    //dbkey = dbkey.replace("=",":");

    console.log(req.body);
    console.log('db key is : ' + dbkey);
    res.json(db["DATA_SOURCE"][dbkey]);

});

module.exports = router;