var config = require("./config");
var express = require('express');
var session = require('express-session');


var app = express();
var bodyParser = require('body-parser');
app.use(session({
    secret: 'secret',
    resave: false,
    saveUninitialized: true
}));

app.use('/media/', function (req,res,next) {
    res.redirect('https://'+config.SERVICE_HOST+'/media/'+req.url);
});
app.use(express.static(__dirname + '/polymer'));
app.use('/static/polymer', express.static(__dirname + '/polymer'));
app.use('/static/polymer2', express.static(__dirname + '/polymer'));
app.use(express.static('server/web'));

function defaultContentTypeMiddleware (req, res, next) {
  if (req.headers['content-type'] == 'application/x-www-form-urlencoded')
  req.headers['content-type'] = 'application/json';
  next();
}

app.use(defaultContentTypeMiddleware);
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());



app.post(function (req, res, next) {
    console.log(req.body);
    //console.log('Req Path : ' + req.path);
    next();
});



app.use('/GOTO', require('./server/services/'+config.SERVICE_NAME));

app.get('/logout', function (req, res) {
    req.session.destroy();
    res.redirect('/');
});

app.listen(config.PORT, config.HOST, function () {
    console.log(config.SERVICE_NAME.toUpperCase()+' is running ...');
});